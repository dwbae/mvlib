//----------------------------------------------------------------------------
/** @file ImageUtil.cpp
	@brief 여기에는 이미지 데이터간 변환을 정의합니다.
*/
//----------------------------------------------------------------------------

#pragma once
#include "BaseDefine.h"
#include "Pattern_Pad.h"

class ImageUtil
{
public: 

	/// <summary>
	/// ImageData를 OpenCV Mat으로 변환 합니다.
	/// </summary>
	/// <param name="imgData"></param>
	/// <returns></returns>
	static cv::Mat* ImageDataToMat(ImageData& imgData);

	/// <summary>
	/// OpenCV Mat 데이터를 ImageData로 변환합니다.
	/// </summary>
	/// <param name="matImage"></param>
	/// <returns></returns>
	static ImageData* MatToImageData(cv::Mat& matImage);
};