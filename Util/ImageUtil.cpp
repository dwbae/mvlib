﻿#include "pch.h"
#include "ImageUtil.h"

cv::Mat* ImageUtil::ImageDataToMat(ImageData& imgData)
{
	cv::Mat* retMat = new cv::Mat();;
	if (imgData.channel == 1)
		retMat->create(cv::Size(imgData.width, imgData.height), CV_8U);
	else if (imgData.channel == 3)
		retMat->create(cv::Size(imgData.width, imgData.height), CV_8UC3);
	memcpy(retMat->data, imgData.pData, imgData.szSize);
	return retMat;
}

/// <summary>
/// OpenCV Mat 데이터를 ImageData로 변환합니다.
/// </summary>
/// <param name="matImage"></param>
/// <returns></returns>
ImageData* ImageUtil::MatToImageData(cv::Mat& matImage)
{
	ImageData* retImageData = new ImageData;

	retImageData->width = matImage.cols;
	retImageData->height = matImage.rows;
	retImageData->channel = matImage.channels();
	retImageData->szSize = matImage.total() * matImage.elemSize();

	retImageData->pData = new unsigned char[retImageData->szSize];
	memcpy(retImageData->pData, matImage.data, retImageData->szSize);

	return retImageData;
}