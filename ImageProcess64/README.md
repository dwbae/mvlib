##ImageProcess64 : Semics Image Processing Library Test

### Resource

* HomePage : <http://www.semics.com>
* Docs : <https://semicsrnd.atlassian.net/wiki/spaces/PVIP/pages/94635598/OPERA+Pattern+Pad>
* OpenCV : 3.2.0
* Build : Visual Studio 2019 (vc142) x64


#### Summary

* 이 프로젝트는 OpenCV 3.2.0 64비트를 사용하여 이미지 프로세싱을 하는 라이브러리 입니다. 
* 이 프로젝트는 Patten_Pad 프로젝트와 코드를 공유합니다. 
    * 64비트에서 수행되어야할 항목에 대하여 "_X64" 전처리가가 정의됩니다. 
	* 코드레벨에서 공유되지만 참조되는 라이브러리는 Opencv 64bit 버전이 적용됩니다. 
* 사용되는 OpenCV 헤더파일과 라이브러리는 솔루션 폴더에 포함되어 있습니다. 
	* $(SolutionDir)opencv320\64bit\x64\vc14\bin\opencv_world320d.dll
	* $(SolutionDir)opencv320\64bit\x64\vc14\lib\opencv_world320d.lib