// ImageProcess64Test.cpp : 이 파일에는 'main' 함수가 포함됩니다. 거기서 프로그램 실행이 시작되고 종료됩니다.
//
#include <stdio.h>
#include <Windows.h>

#pragma warning(disable:4996)
#include <iostream>
#include "../Inc/Pattern_Pad.h"

//Test for mbcs 32bit
#include "opencv2/opencv.hpp"
//#include "../opencv320/include/opencv2/imgcodecs.hpp"
//#include "../opencv320/include/opencv2/imgproc.hpp"

#include <atlstr.h>
#ifdef _DEBUG
#pragma comment(lib,"../opencv320/32bit/lib/opencv_world320d.lib")
#elif
#pragma comment(lib,"../opencv320/32bit/lib/opencv_world320.lib")
#endif


int recipeRunTest(int argc, TCHAR* argv[])
{
	//FILE* fpRecipe = fopen("..\\Sample\\ECC_Test.json", "r");
	FILE* fpRecipe = fopen("..\\Sample\\gaussian.json", "r");
	char* recipe = nullptr;

	

	if (fpRecipe != nullptr)
	{

		fseek(fpRecipe, 0, SEEK_END);
		auto size = ftell(fpRecipe);
		char* readbuffer = new char[(size + 1)]{ 0, };
		fseek(fpRecipe, 0, SEEK_SET);
		fread(readbuffer, sizeof(char), size, fpRecipe);

		int len = MultiByteToWideChar(CP_UTF8, 0, readbuffer, size, NULL, NULL);
		char* buffer2 = new char[len * 2 + 2]{ 0, };
		WCHAR* wbuffer = (WCHAR*)buffer2;
		MultiByteToWideChar(CP_UTF8, 0, readbuffer, size, wbuffer, len);

		size = len ;
		len = WideCharToMultiByte(CP_ACP, 0, wbuffer, size, NULL,0,NULL, NULL);
		recipe = new char[len + 1]{ 0, };
		
		WideCharToMultiByte(CP_ACP, 0, wbuffer, size, recipe, len, NULL, NULL);



		delete[] buffer2;
		delete[] readbuffer;
		fclose(fpRecipe);
	}

	ImageParam* src = new ImageParam();
	ImageParam* ref = new ImageParam();
	ImageParam* dst = new ImageParam();

	src->argc = 1;
	src->argv = new ImageData * [1];

	cv::Mat img = cv::imread(argv[0]);
	cv::imwrite("C:\\Repos\\input.jpg", img);
	src->argv[0] = new ImageData;
	src->argv[0]->channel = img.channels();
	src->argv[0]->width = img.size().width;
	src->argv[0]->height = img.size().height;
	src->argv[0]->szSize = img.total() * img.elemSize();
	src->argv[0]->pData = new unsigned char[src->argv[0]->szSize];
	memcpy(src->argv[0]->pData, img.data, src->argv[0]->szSize);


	int nRet = RecipeExecute(recipe, NULL, src, ref, dst);
	delete[] recipe;
	if (dst->argc > 0)
	{
		cv::Mat result;
		result.create(cv::Size(dst->argv[0]->width, dst->argv[0]->height), CV_8UC(dst->argv[0]->channel));
		memcpy(result.data, dst->argv[0]->pData, dst->argv[0]->szSize);
		cv::imwrite("C:\\Repos\\output.jpg", result);
		result.release();
	}	

	delete src;
	delete ref;
	ImageParamRelease(dst);
	delete dst;
	return 0;
}

int main(int argc, char* argv[])
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	_CrtDumpMemoryLeaks();
	printf("OpenCV Test 1\n");
	int c = Add(1, 2);
	
	if (argc < 3)
		return 0;

	double ret = VBcv1032Pattern_MatchingECC(argv[1], argv[2]);
	printf("Result : %lf\n", ret);	
	recipeRunTest( argc,  argv);
	
	
	_CrtDumpMemoryLeaks();
	return 0;
}
