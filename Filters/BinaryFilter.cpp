//----------------------------------------------------------------------------
/** @file BlurFilter.cpp
	@brief 여기에는 필터의 동작을 정의합니다. 
*/
//----------------------------------------------------------------------------
//#ifdef _X64
//#include "../ImageProcess64/pch.h"
//#endif

#include "pch.h"
#include "BinaryFilter.h"
#include "Pin.h"
#include "Param.h"

using namespace cv;

/// <summary>
/// Recipe 파서에서 필터 생성을 위한 목록에 추가를 합니다.
/// </summary>
REGISTER_CLASSNAME(_T("VisionRecipeEditor.ImageFilter.ThresholdFilter"), CBinaryFilter);


#define PARAM_THRESHOLD _T("Threshold")
/// <summary>
/// 필터의 입출력 / 변수를 정의 합니다.
/// </summary>
CBinaryFilter::CBinaryFilter()
{
	Pins.push_back(new Pin(this,_T("In")));
	Pins.push_back(new Pin(this, _T("Out"),true));

	Params.push_back(new imgproc::Param<int>(PARAM_THRESHOLD));
}




void CBinaryFilter::Proc(vector<IPin*> pins, vector<IParam*> params)
{
	Mat ret;

	int threshold = 127;
	
	for (auto itor = params.begin(); itor != params.end(); ++itor)//for (const auto& param : params)
	{
		if (_tcscmp((*itor)->GetName(), PARAM_THRESHOLD)==0)
		{
			auto p = dynamic_cast<imgproc::Param<int>*>((*itor));
			threshold = p->GetValue();
		}
	}

	for (auto itor = pins.begin(); itor != pins.end(); ++itor)//for (const auto& pin : pins)
	{
		if (!(*itor)->IsOutput() )
		{	
			cv::threshold(*(*itor)->GetImage(), ret, threshold, 255, cv::ThresholdTypes::THRESH_BINARY);
		}
	}

	for (auto itor = pins.begin(); itor != pins.end(); ++itor)//for (const auto& pin : pins)
	{
		if ((*itor)->IsOutput())
		{
			(*itor)->SetImage(&ret);
		}
	}
	ret.release();
}


int CBinaryFilter::Init(GenericValue<ENCODE>& value)
{
	Filter::Init(value);

	if (value.HasMember(RECIPE_JSON_FILTER_PARAMS))
	{
		GenericValue<ENCODE>& fs = value[RECIPE_JSON_FILTER_PARAMS];
		for (SizeType i = 0; i < fs.Size(); ++i)
		{	
			tstring key;
			GETJSONSTRING(fs[i], RECIPE_JSON_FILTER_PARAM_KEY, key);

			if (key == PARAM_THRESHOLD)
			{
				int value=127;
				
				if (fs[i].HasMember(RECIPE_JSON_FILTER_PARAM_VALUE))
				{
					tstring val = fs[i][RECIPE_JSON_FILTER_PARAM_VALUE].GetString();
					value = stoi(val);
				}
				

				Filter::SetParam(PARAM_THRESHOLD, value);
			}
		}
	}
	/*if (value.HasMember(RECIPE_JSON_FILTER_PINS))
	{
		GenericValue<ENCODE>& fs = value[RECIPE_JSON_FILTER_PINS];
		for (SizeType i = 0; i < fs.Size(); ++i)
		{
		}
	}*/

	return 0;
}


void CBinaryFilter::Print()
{
	cout << "BinaryFilter" << endl;
}