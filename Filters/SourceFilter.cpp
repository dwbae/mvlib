//----------------------------------------------------------------------------
/** @file SourceFilter.cpp
	@brief 여기에는 필터의 동작을 정의합니다.
*/
//----------------------------------------------------------------------------
#include "pch.h"

#include "SourceFilter.h"
#include "Pin.h"
#include "Param.h"

using namespace cv;
/// <summary>
/// Recipe 파서에서 필터 생성을 위한 목록에 추가를 합니다.
/// </summary>
REGISTER_CLASSNAME(_T("VisionRecipeEditor.ImageFilter.SourceFilter"), CSourceFilter);
#define PARAM_PATH _T("Path")
CSourceFilter::CSourceFilter()
{
	Pins.push_back(new Pin(this, _T("Out"),true));
	Params.push_back(new imgproc::Param<tstring>(PARAM_PATH));
}





void CSourceFilter::Proc(vector<IPin*> pins, vector<IParam*> params)
{
	tstring path = _T("");
	
	for (auto itor = params.begin(); itor!= params.end(); ++itor)
	{
		if (_tcscmp((*itor)->GetName() , PARAM_PATH)==0)
		{
			auto p = dynamic_cast<imgproc::Param<tstring>*>((*itor));
			path = p->GetValue();
		}
	}

#ifdef _UNICODE

	string multiByte;
	multiByte.assign(path.begin(), path.end());
	Mat ret = imread(multiByte);
	for (auto itor = pins.begin(); itor != pins.end(); ++itor)
	{
		if ((*itor)->IsOutput())
		{
			(*itor)->SetImage(&ret);
		}
	}

#else
	 
	 Mat ret = imread(path);
	 for (auto itor = pins.begin(); itor != pins.end(); ++itor)
	 {
		 if ((*itor)->IsOutput())
		 {
			 (*itor)->SetImage(&ret);
		 }
	 }
	 ret.release();
#endif  
}


int CSourceFilter::Init(GenericValue<ENCODE>& value) 
{
	Filter::Init(value);


	if (value.HasMember(RECIPE_JSON_FILTER_PARAMS))
	{
		GenericValue<ENCODE>& fs = value[RECIPE_JSON_FILTER_PARAMS];
		for (SizeType i = 0; i < fs.Size(); ++i)
		{
			tstring key;
			GETJSONSTRING(fs[i], RECIPE_JSON_FILTER_PARAM_KEY, key);

			if (key == PARAM_PATH)
			{
				tstring value = _T("");
				GETJSONSTRING(fs[i], RECIPE_JSON_FILTER_PARAM_VALUE, value);

				Filter::SetParam(PARAM_PATH, value.c_str());
			}
		}
	}

	return 0;
}


void CSourceFilter::Print()
{
	cout << "Source Filter" << endl;
}
