//----------------------------------------------------------------------------
/** @file BlurFilter.cpp
	@brief 여기에는 필터의 동작을 정의합니다. 
*/
//----------------------------------------------------------------------------
#include "pch.h"

#include "GaussianBlurFilter.h"
#include "Pin.h"
#include "Param.h"

using namespace cv;

/// <summary>
/// Recipe 파서에서 필터 생성을 위한 목록에 추가를 합니다.
/// </summary>
REGISTER_CLASSNAME(_T("VisionRecipeEditor.ImageFilter.GaussianBlurFilter"), CGaussianBlurFilter);

#define PARAM_KERNEL _T("Kernel")
#define PARAM_SIGMA _T("Sigma")
/// <summary>
/// 필터의 입출력 / 변수를 정의 합니다.
/// </summary>
CGaussianBlurFilter::CGaussianBlurFilter()
{
	Pins.push_back(new Pin(this, _T("In")));
	Pins.push_back(new Pin(this, _T("Out"),true));

	Params.push_back(new imgproc::Param<int>(_T("Kernel")));
	Params.push_back(new imgproc::Param<int>(_T("Sigma")));
}




void CGaussianBlurFilter::Proc(vector<IPin*> pins, vector<IParam*> params)
{
	Mat ret;

	int kernel = 3;
	int sigma = 3;
	for (auto itor = params.begin(); itor != params.end(); ++itor)//for (const auto& param : params)
	{
		if ((*itor)->GetName() == _T("Kernel"))
		{
			auto p = dynamic_cast<imgproc::Param<int>*>((*itor));
			kernel = p->GetValue();
		}
		if ((*itor)->GetName() == _T("Sigma"))
		{
			auto p = dynamic_cast<imgproc::Param<int>*>((*itor));
			sigma = p->GetValue();
		}
	}

	for (auto itor = pins.begin(); itor != pins.end(); ++itor)//for (const auto& pin : pins)
	{
		if (!(*itor)->IsOutput())
		{	
			GaussianBlur(*(*itor)->GetImage(), ret, cv::Size(kernel, kernel), sigma);
		}
	}

	for (auto itor = pins.begin(); itor != pins.end(); ++itor)//for (const auto& pin : pins)
	{
		if ((*itor)->IsOutput())
		{
			(*itor)->SetImage(&ret);
		}
	}
	ret.release();
}


int CGaussianBlurFilter::Init(GenericValue<ENCODE>& value)
{
	Filter::Init(value);
	if (value.HasMember(RECIPE_JSON_FILTER_PARAMS))
	{
		GenericValue<ENCODE>& fs = value[RECIPE_JSON_FILTER_PARAMS];
		for (SizeType i = 0; i < fs.Size(); ++i)
		{
			tstring key;
			GETJSONSTRING(fs[i], RECIPE_JSON_FILTER_PARAM_KEY, key);

			if (key == PARAM_KERNEL)
			{
				int value = 3;
				if (fs[i].HasMember(RECIPE_JSON_FILTER_PARAM_VALUE))
				{
					tstring val = fs[i][RECIPE_JSON_FILTER_PARAM_VALUE].GetString();
					value = stoi(val);
					//value = fs[i][RECIPE_JSON_FILTER_PARAM_VALUE].GetInt();
				}
				//GETJSONINT(fs[i], RECIPE_JSON_FILTER_PARAM_VALUE, value);

				Filter::SetParam(PARAM_KERNEL, value);
			}
			else if (key == PARAM_SIGMA)
			{
				int value = 3;
				if (fs[i].HasMember(RECIPE_JSON_FILTER_PARAM_VALUE))
				{
					tstring val = fs[i][RECIPE_JSON_FILTER_PARAM_VALUE].GetString();
					value = stoi(val);
				}
				//GETJSONINT(fs[i], RECIPE_JSON_FILTER_PARAM_VALUE, value);

				Filter::SetParam(PARAM_SIGMA, value);
			}
		}
	}
	return 0;
}


void CGaussianBlurFilter::Print()
{
	cout << "BlurFilter" << endl;
}

