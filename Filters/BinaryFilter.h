//----------------------------------------------------------------------------
/** @file Filters.h
	@brief 여기에는 추가되는 필터 클래스를 정의합니다.
*/
//----------------------------------------------------------------------------
#pragma once
#include "Filter.h"

MAKEFILTER(BinaryFilter); // 클래스 매크로 BinaryFilter
