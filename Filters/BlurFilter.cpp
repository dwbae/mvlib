//----------------------------------------------------------------------------
/** @file BlurFilter.cpp
	@brief 여기에는 필터의 동작을 정의합니다. 
*/
//----------------------------------------------------------------------------
//#ifdef _X64
//#include "../ImageProcess64/pch.h"
//#endif
#include "pch.h"
#include "BlurFilter.h"
#include "Pin.h"
#include "Param.h"

using namespace cv;

/// <summary>
/// Recipe 파서에서 필터 생성을 위한 목록에 추가를 합니다.
/// </summary>
REGISTER_CLASSNAME(_T("VisionRecipeEditor.ImageFilter.BlurFilter"), CBlurFilter);

#define PARAM_KERNEL _T("Kernel")

/// <summary>
/// 필터의 입출력 / 변수를 정의 합니다.
/// </summary>
CBlurFilter::CBlurFilter()
{
	Pins.push_back(new Pin(this, _T("In")));
	Pins.push_back(new Pin(this, _T("Out"),true));

	Params.push_back(new imgproc::Param<int>(PARAM_KERNEL));
}




void CBlurFilter::Proc(vector<IPin*> pins, vector<IParam*> params)
{
	Mat ret;

	int kernel = 3;
	for (auto itor = params.begin(); itor != params.end(); ++itor)//for (const auto& param : params)
	{
		if ((*itor)->GetName() == PARAM_KERNEL)
		{
			auto p = dynamic_cast<imgproc::Param<int>*>((*itor));
			kernel = p->GetValue();
		}
	}

	for (auto itor = pins.begin(); itor != pins.end(); ++itor)//for (const auto& pin : pins)
	{
		if (!(*itor)->IsOutput())
		{	
			blur(*(*itor)->GetImage(), ret, cv::Size(kernel, kernel));
		}
	}

	for (auto itor = pins.begin(); itor != pins.end(); ++itor)//for (const auto& pin : pins)
	{
		if ((*itor)->IsOutput())
		{
			(*itor)->SetImage(&ret);
		}
	}
	ret.release();
}


int CBlurFilter::Init(GenericValue<ENCODE>& value)
{
	Filter::Init(value);

	if (value.HasMember(RECIPE_JSON_FILTER_PARAMS))
	{
		GenericValue<ENCODE>& fs = value[RECIPE_JSON_FILTER_PARAMS];
		for (SizeType i = 0; i < fs.Size(); ++i)
		{
			tstring key;
			GETJSONSTRING(fs[i], RECIPE_JSON_FILTER_PARAM_KEY, key);

			if (key == PARAM_KERNEL)
			{
				int value = 3;
				if (fs[i].HasMember(RECIPE_JSON_FILTER_PARAM_VALUE))
				{
					tstring val = fs[i][RECIPE_JSON_FILTER_PARAM_VALUE].GetString();
					value = stoi(val);
				}

				Filter::SetParam(PARAM_KERNEL, value);
			}
		}
	}
	return 0;
}


void CBlurFilter::Print()
{
	cout << "BlurFilter" << endl;
}

