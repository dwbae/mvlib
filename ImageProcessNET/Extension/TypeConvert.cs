﻿using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageProcessNET.Extension
{
    public static class TypeConvert
    {
        public static Mat ToGrayscale(this Mat src)
        {
            if (src == null)
                return null;
            Mat grayscale;
            if (src.Channels() > 1)
            {
                grayscale = new Mat(src.Size(), MatType.CV_8UC1);
                Cv2.CvtColor(src, grayscale, ColorConversionCodes.BGR2GRAY);
            }
            else
            {
                grayscale = src.Clone(); ;
            }

            return grayscale;
        }

        public static Bitmap ToBitmap(this Mat dst)
        {
            try
            {
                return OpenCvSharp.Extensions.BitmapConverter.ToBitmap(dst);
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }

            return null;
        }

        public static Mat ToMat(this Bitmap dst)
        {
            try
            {
                Mat ret = OpenCvSharp.Extensions.BitmapConverter.ToMat(dst);


                return ret;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }

            return null;
        }
    }
}
