﻿#define PARALLEL

using ImageProcessNET.Extension;
using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageProcessNET
{

    public class ImageProcess
    {
        /// <summary>
        /// Phase only transform 
        /// </summary>
        public static Bitmap PHOT(Bitmap image)
        {
            if (image == null)
            {
                return null;
            }
            using (Mat img = image?.ToMat())
            {
                if (img == null)
                    return null;
                using (Mat gry = img.ToGrayscale())
                using (Mat ret = PHOT(gry))
                {
                    return ret.ToBitmap();
                }
            }
        }

        private static Mat PHOT(Mat img)
        {

            Mat floatMatImg = new Mat();
            img.ConvertTo(floatMatImg, MatType.CV_32F);

            Mat dft = new Mat();
            Cv2.Dft(floatMatImg, dft);

            var magnitude = Cv2.Abs(dft);            
            Mat phase = new Mat();
            Cv2.Divide(dft, magnitude, phase);

            Mat ivImg = new Mat();
            Cv2.Idft(phase, ivImg);
            
            Cv2.Normalize(ivImg, ivImg, 0, 255, NormTypes.MinMax, MatType.CV_8U);

            return ivImg;
        }

        /// <summary>
        /// 이미지의 특정 영역을 출력 영역에 맞추어 크기 조절
        /// </summary>
        /// <param name="bmp">이미지</param>
        /// <param name="screenLT">출력 영역 왼쪽 상단</param>
        /// <param name="screenRB">출력 영역 오른쪽 하단</param>
        /// <param name="imageLT">Corp 영역 왼쪽 상단</param>
        /// <param name="imageRB">Crop 영역 오른쪽 하단</param>
        /// <returns></returns>
        public static Bitmap Resize(Bitmap image, PointF screenLT, PointF screenRB, PointF imageLT, PointF imageRB)
        {
            try
            {
                if (image == null)
                {
                    return null;
                }
                using (Mat srcMat = image?.ToMat())
                {
                    if (srcMat == null)
                        return null;
                    using (Mat src = srcMat?.SubMat(new OpenCvSharp.Rect(
                                        (int)imageLT.X, (int)imageLT.Y,
                                        (int)(imageRB.X - imageLT.X),
                                        (int)(imageRB.Y - imageLT.Y))))
                    {
                        if (src == null)
                            return null;
                        using (Mat dst = new Mat())
                        {
                            OpenCvSharp.Cv2.Resize(src, dst,
                                new OpenCvSharp.Size(screenRB.X - screenLT.X, screenRB.Y - screenLT.Y),
                                0,
                                0,
                                OpenCvSharp.InterpolationFlags.Cubic);

                            return dst?.ToBitmap();

                        }
                    }
                }

            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            return null;
        }

        public enum ThresholdTypes
        {
            Binary,
            BinaryInv,
            Otsu,
            Triangle
        }
        /// <summary>
        /// 이미지 이진화
        /// </summary>
        /// <param name="image"></param>
        /// <param name="arg1"></param>
        /// <param name="arg2"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static Bitmap Threshold(Bitmap image, int arg1, int arg2, ThresholdTypes type)
        {
            if (image == null)
            {
                return null;
            }
            using (Mat img = image?.ToMat())
            {
                if (img == null)
                    return null;
                using (Mat gry = img.ToGrayscale())
                using (Mat ret = Threshold(gry, arg1, arg2, type))
                {
                    return ret.ToBitmap();
                }
            }
        }



        private static Mat Threshold(Mat img, int arg1, int arg2, ThresholdTypes type)
        {
            switch (type)
            {
                case ThresholdTypes.Binary:
                    return img.Threshold(arg1, arg2, OpenCvSharp.ThresholdTypes.Binary);
                case ThresholdTypes.BinaryInv:
                    return img.Threshold(arg1, arg2, OpenCvSharp.ThresholdTypes.Binary);
                default:
                    return img.Threshold(127, 255, OpenCvSharp.ThresholdTypes.Otsu);
            }
        }

        public enum BlurTypes
        {
            Normal,
            Gaussian,
            Median,
        }

        /// <summary>
        /// 이미지 블러
        /// </summary>
        /// <param name="image"></param>
        /// <param name="arg1">ksize</param>
        /// <param name="arg2">sigma</param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static Bitmap Blur(Bitmap image, int arg1, int arg2, BlurTypes type)
        {
            if (image == null)
            {
                return null;
            }

            using (Mat img = image?.ToMat())
            {
                if (img == null)
                    return null;
                using (Mat ret = Blur(img, arg1, arg2, type))
                {
                    return ret.ToBitmap();
                }
            }
        }

        private static Mat Blur(Mat img, int arg1, int arg2, BlurTypes type)
        {
            if (arg1 % 2 == 0)
                arg1++;

            switch (type)
            {
                case BlurTypes.Normal:
                    return img.Blur(new OpenCvSharp.Size(arg1, arg1));
                case BlurTypes.Gaussian:
                    return img.GaussianBlur(new OpenCvSharp.Size(arg1, arg1), arg2);
                case BlurTypes.Median:
                    return img.MedianBlur(arg1);
                default:
                    return img.GaussianBlur(new OpenCvSharp.Size(3, 3), 2);
            }
        }
        /// <summary>
        /// 이미지 크롭
        /// </summary>
        /// <param name="image"></param>
        /// <param name="rect"></param>
        /// <returns></returns>
        public static Bitmap Crop(Bitmap image, Rectangle rect)
        {
            if (image == null)
            {
                return null;
            }
            using (Mat img = image.ToMat())
            {
                if (img == null)
                    return null;
                using (Mat ret = Crop(img, rect))
                {
                    return ret.ToBitmap();
                }
            }
        }
        private static Mat Crop(Mat img, Rectangle rect)
        {
            if (rect.Width <= 0 || rect.Height <= 0)
            {
                return img.Clone();
            }

            var x = Math.Max(0, rect.X);
            var y = Math.Max(0, rect.Y);

            x = Math.Min(img.Width, x);
            y = Math.Min(img.Height, y);

            var dx = Math.Max(0, rect.Right);
            var dy = Math.Max(0, rect.Bottom);

            dx = Math.Min(img.Width, dx);
            dy = Math.Min(img.Height, dy);


            var targetRect = new OpenCvSharp.Rect(x, y, dx - x, dy - y);
            var pasteRect = new OpenCvSharp.Rect(
                -Math.Min(0, rect.X),
                -Math.Min(0, rect.Y),
                targetRect.Width,
                targetRect.Height);

            using (Mat imgROI = new Mat(img, targetRect)) // new Mat(img, targetRect))
            {
                Mat ret = new Mat(rect.Height, rect.Width, img.Type(), Scalar.Black);
                if (targetRect.Width <= 0 || targetRect.Height <= 0)
                {
                }
                else
                {
                    using (Mat retROI = new Mat(ret, pasteRect))
                    {
                        Cv2.AddWeighted(imgROI, 1, retROI, 0, 1, retROI);
                    }
                }
                //ret.Add(imgROI);
                return ret;
            }
        }

        public  static Bitmap FindTransformECC(Bitmap image, Bitmap refImage)
        {
            if (image == null)
            {
                return null;
            }
            using (Mat img = image.ToMat())
            using (Mat refImg = refImage.ToMat())
            {
                if (img == null)
                    return null;
                if (refImg == null)
                    return null;
                using (Mat grySrc = img.ToGrayscale())
                using (Mat gryRef = refImg.ToGrayscale())
                using (Mat ret = FindTransformECC(grySrc, gryRef))
                {
                    return ret.ToBitmap();
                }
            }

        }

        private static Mat FindTransformECC(Mat mSrc_Image, Mat mRef_Image)
        {
            try
            {


                //3. Calculate
                // Define the motion model
                MotionTypes warp_mode = MotionTypes.Euclidean;

                // Set a 2x3 or 3x3 warp matrix depending on the motion model.
                Mat warp_matrix;

                // Initialize the matrix to identity
                if (warp_mode == MotionTypes.Homography)
                    warp_matrix = Mat.Eye(3, 3, MatType.CV_32F);
                else
                    warp_matrix = Mat.Eye(2, 3, MatType.CV_32F);

                // Specify the number of iterations.
                int number_of_iterations = 5000;

                // Specify the threshold of the increment
                // in the correlation coefficient between two iterations
                double termination_eps = 1e-10;

                // Define termination criteria
                TermCriteria criteria = new TermCriteria(CriteriaTypes.Count | CriteriaTypes.Eps, number_of_iterations, termination_eps);

                // Run the ECC algorithm. The results are stored in warp_matrix.
                Cv2.FindTransformECC(
                    mSrc_Image,
                    mRef_Image,
                    warp_matrix,
                    warp_mode,
                    criteria
                );

                //4. Comput with affine
                // Storage for warped image.
                Mat mRef_Image_Matched = new Mat(); // 결과 이미지
                if (warp_mode != MotionTypes.Homography)
                    // Use warpAffine for Translation, Euclidean and Affine
                    Cv2.WarpAffine(mRef_Image, mRef_Image_Matched, warp_matrix, mSrc_Image.Size(), InterpolationFlags.Linear | InterpolationFlags.WarpInverseMap);

                else
                    // Use warpPerspective for Homography
                    Cv2.WarpPerspective(mRef_Image, mRef_Image_Matched, warp_matrix, mSrc_Image.Size(), InterpolationFlags.Linear | InterpolationFlags.WarpInverseMap);


                //4.1 레퍼런스 영역으로 찾은 이미지영역 마스킹
                Mat mMask_Image = new Mat(mRef_Image.Size(), MatType.CV_8UC1, new Scalar(255)); // 참조 이미지 영역 만큼 마스크 만들기
                Mat mMask_Image_Matched = new Mat(); // 마스크 이미지 위치를 이동한 이미지

                if (warp_mode != MotionTypes.Homography)
                    // Use warpAffine for Translation, Euclidean and Affine		
                    Cv2.WarpAffine(mMask_Image, mMask_Image_Matched, warp_matrix, mSrc_Image.Size(), InterpolationFlags.Linear | InterpolationFlags.WarpInverseMap);
                else
                    // Use warpPerspective for Homography		
                    Cv2.WarpPerspective(mMask_Image, mMask_Image_Matched, warp_matrix, mSrc_Image.Size(), InterpolationFlags.Linear | InterpolationFlags.WarpInverseMap);

                return mRef_Image_Matched;
            }
            catch
            { }
            return null;
            ////4.1 원본 이미지에 마스크 영역 씌우기
            //Mat mSrc_Masked;
            //Cv2.Bitwise_and(mSrc_Image, mMask_Image_Matched, mSrc_Masked);


            ////------------------------------------
            ////Result 
            ////1 mSrc_Masked = 원본에서 찾은 Ref영역 마스크한 이미지 
            ////2 mRef_Image_Matched = 레퍼런스 이미지를 원본 영역 위치에 맞춘 이미지 


            ////Result 1. 원본과 마스크 이미지 단순 빼기 연산	
            //Mat mResult_OrgSub = mSrc_Masked.clone();// = mSrc_Masked - mRef_Image_Matched;	
            //                                         //bitwise_xor(mSrc_Masked, mRef_Image_Matched, mResult_OrgSub);
            //for (int r = mResult_OrgSub.rows - 1; r >= 0; --r)
            //{
            //    int row = r * mResult_OrgSub.cols;
            //    for (int c = mResult_OrgSub.cols - 1; c >= 0; --c)
            //    {
            //        if (abs(mResult_OrgSub.data[row + c] - mRef_Image_Matched.data[row + c]) < 50)
            //        {
            //            mResult_OrgSub.data[row + c] = 255;
            //        }
            //        else
            //            mResult_OrgSub.data[row + c] = 0;
            //    }
            //}

            ////Result 2. 원본과 Ref이미지를 노멀라이즈 처리후 연산
            //Mat mSrc_Masked_Normal;
            //cv::normalize(mSrc_Masked, mSrc_Masked_Normal, 0, 255, NORM_MINMAX);
            //Mat mRef_Image_Matched_Normal;
            //cv::normalize(mRef_Image_Matched, mRef_Image_Matched_Normal, 0, 255, NORM_MINMAX);

            //Mat mResult_NormalizeSub = mSrc_Masked_Normal.clone();
            ////bitwise_xor(mSrc_Masked_Normal, mRef_Image_Matched_Normal, mResult_NormalizeSub);

            //for (int r = mSrc_Masked_Normal.rows - 1; r >= 0; --r)
            //{
            //    int row = r * mSrc_Masked_Normal.cols;
            //    for (int c = mSrc_Masked_Normal.cols - 1; c >= 0; --c)
            //    {
            //        if (abs(mResult_NormalizeSub.data[row + c] - mRef_Image_Matched_Normal.data[row + c]) < 30)
            //        {
            //            mResult_NormalizeSub.data[row + c] = 255;
            //        }
            //        else
            //            mResult_NormalizeSub.data[row + c] = 0;
            //    }
            //}

        }

        public static Bitmap SubStract(Bitmap image, Bitmap refImage)
        {
            if (image == null)
            {
                return null;
            }
            using (Mat img = image.ToMat())
            using (Mat refImg = refImage.ToMat())
            {
                if (img == null)
                    return null;
                if (refImg == null)
                    return null;
                using (Mat grySrc = img.ToGrayscale())
                using (Mat gryRef = refImg.ToGrayscale())
                using (Mat ret = SubStract(grySrc, gryRef))
                {
                    return ret.ToBitmap();
                }
            }

        }

        private static Mat SubStract(Mat mSrc_Image, Mat mRef_Image)
        {
            if (mSrc_Image == null || mRef_Image == null || mSrc_Image.Empty() || mRef_Image.Empty() ||
                mSrc_Image.Type() != MatType.CV_8UC1 || mSrc_Image.Type() != mSrc_Image.Type()
                    )
            {
                return mSrc_Image;
            }

#if PARALLEL
            Parallel.For(0, Math.Min(mSrc_Image.Rows, mRef_Image.Rows), (r) => {
                for (int c = Math.Min(mSrc_Image.Cols, mRef_Image.Cols) - 1; c >= 0; --c)
                {
                    mSrc_Image.At<byte>(r, c) = (byte)Math.Min(255, Math.Abs(mSrc_Image.At<byte>(r, c) - mRef_Image.At<byte>(r, c)));
                }
            });
#else

            for (int r = Math.Min(mSrc_Image.Rows, mRef_Image.Rows) - 1; r >= 0; --r)
            {
                for (int c = Math.Min(mSrc_Image.Cols, mRef_Image.Cols) - 1; c >= 0; --c)
                {
                    mSrc_Image.At<byte>(r, c) = (byte)Math.Min(255, Math.Abs(mSrc_Image.At<byte>(r, c) - mRef_Image.At<byte>(r, c)));
                }
            }
#endif
            return mSrc_Image;

        }
        public static Bitmap Normalize(Bitmap image)
        {
            if (image == null)
            {
                return null;
            }
            using (Mat img = image?.ToMat())
            {
                if (img == null)
                    return null;
                using (Mat gry = img.ToGrayscale())
                using (Mat ret = Normalize(gry))
                {
                    return ret.ToBitmap();
                }
            }
        }

        private static Mat Normalize(Mat img)
        {
            Mat ret = new Mat();
            Cv2.Normalize(img, ret, 0, 255, NormTypes.MinMax);
            return ret;
        }

        public static Bitmap FindBlob(Bitmap image)
        {
            if (image == null)
            {
                return null;
            }
            using (Mat img = image?.ToMat())
            {
                if (img == null)
                    return null;
                using (Mat gry = img.ToGrayscale())
                using (Mat ret = Normalize(gry))
                {
                    return ret.ToBitmap();
                }
            }
        }

        private static Mat FindContours(Mat img)
        {

            Cv2.Normalize(img, img, 0, 1, NormTypes.MinMax);
            return img;
        }
    }


}
