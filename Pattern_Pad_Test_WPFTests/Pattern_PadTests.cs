﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pattern_Pad_Test_WPF;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pattern_Pad_Test_WPF.Tests
{
    [TestClass()]
    public class Pattern_PadTests
    {
        /// <summary>
        /// 함수 호출 테스트 / ECC 알고리즘 실행. 
        /// </summary>
        [TestMethod()]
        public void TestFuncTest()
        {
            Pattern_Pad.TestFunc();
            string fileM = System.IO.Path.Combine(AppDomain.CurrentDomain.SetupInformation.ApplicationBase, "..\\..\\Sample\\target.jpg");
            string fileR = System.IO.Path.Combine(AppDomain.CurrentDomain.SetupInformation.ApplicationBase, "..\\..\\Sample\\ref.bmp");
            string fileN = "TESTAAAAA";
            
            if (System.IO.File.Exists(fileR) && System.IO.File.Exists(fileM))
            {
                Pattern_Pad.Pattern(fileM, fileR, fileN);
            }
        }

        [TestMethod]
        public void TestFunctionMVisionWrapper()
        {
            string file = System.IO.Path.Combine(AppDomain.CurrentDomain.SetupInformation.ApplicationBase, "..\\..\\Sample\\Sample5DotLine.png");

            if (System.IO.File.Exists(file))
            {
                //Pattern_Pad.Pattern(fileM, fileR, fileN);

                Bitmap img = (Bitmap)Bitmap.FromFile(file);

                var ret = Pattern_Pad.EnhancedOTSU(img);

                ret.Save(System.IO.Path.Combine(
                    System.IO.Path.GetDirectoryName(file),
                    "result.bmp"
                    )
                    );

            }
        }

        [TestMethod]
        public void TestFunctionMVisionWrapper_CLR()
        {
            string file = System.IO.Path.Combine(AppDomain.CurrentDomain.SetupInformation.ApplicationBase, "..\\..\\Sample\\Sample5DotLine.png");

            if (System.IO.File.Exists(file))
            {
                //Pattern_Pad.Pattern(fileM, fileR, fileN);

                Bitmap img = (Bitmap)Bitmap.FromFile(file);

                var ret = Pattern_Pad.EnhancedOTSU_CLR(img);

                ret.Save(System.IO.Path.Combine(
                    System.IO.Path.GetDirectoryName(file),
                    "result.bmp"
                    )
                    );

            }
        }
    }
}