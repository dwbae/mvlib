##ImageProcess32Test : Semics Image Processing Library Test

### Resource

* HomePage : <http://www.semics.com>
* Docs : <https://semicsrnd.atlassian.net/wiki/spaces/PVIP/pages/94635598/OPERA+Pattern+Pad>
* OpenCV : 3.2.0
* Build : Visual Studio 2010 (vc100) x32 


#### Summary

* 이 프로젝트는 OpenCV 3.2.0 32비트를 기반으로 준비된 라이브러리의 동작 검증에 사용됩니다. 
* 이 프로젝트는 Pattern_Pad 프로젝트에 종속되어있습니다. 
	* ImageProcess32Test -> Pattern_Pad -> OpenCV