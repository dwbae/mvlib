#include "pch.h"

#include "ImageProcesCLR.h"
#include "../Inc/Pattern_Pad.h"
#include "../MVisionLibrary/MVisionLibrary.h"

#ifdef _DEBUG
#pragma comment(lib,"../x64/Debug/ImageProcess64.lib")
#else
#pragma comment(lib,"../x64/Release/ImageProcess64.lib")
#endif

#include <string.h>

using namespace System;
using namespace System::Runtime::InteropServices;

namespace ImageProcesCLR {

	ImageProcess::ImageProcess()
	{
	}
	ImageProcess::~ImageProcess()
	{

	}

	int ImageProcess::FuncTest(int a, int b)
	{
		int c = Add(a, b);
		return c;
	}
	double ImageProcess::Pattern_Matching(String^ DirectionM, String^ DirectionR, String^ DirectionP)
	{
		char* str1 = (char*)Marshal::StringToHGlobalAnsi(DirectionM).ToPointer();
		char* str2 = (char*)Marshal::StringToHGlobalAnsi(DirectionR).ToPointer();
		char* str3 = (char*)Marshal::StringToHGlobalAnsi(DirectionP).ToPointer();

		auto ret = VBcv1032Pattern_Matching(str1, str2, str3);
		Marshal::FreeHGlobal((IntPtr)str1);
		Marshal::FreeHGlobal((IntPtr)str2);

		return ret;
	}
	double ImageProcess::Pattern_MatchingECC(String^ DirectionM, String^ DirectionR)
	{
		char* str1 = (char*)Marshal::StringToHGlobalAnsi(DirectionM).ToPointer();
		char* str2 = (char*)Marshal::StringToHGlobalAnsi(DirectionR).ToPointer();

		auto ret = VBcv1032Pattern_MatchingECC(str1, str2);
		Marshal::FreeHGlobal((IntPtr)str1);
		Marshal::FreeHGlobal((IntPtr)str2);


		return ret;
	}

	 array<Byte>^ ImageProcess::EnhanceImageFilter(array<Byte>^ src, int nImgWidth, int nImgHeight, int eFilterDirection, float fSigma, float fScaleFactor)
	{
		pin_ptr<Byte> pSrcBuffer = &src[src->GetLowerBound(0)];
		
		array<Byte>^ dst = gcnew array<Byte>(src->Length);
		pin_ptr<Byte> pDstBuffer = &dst[dst->GetLowerBound(0)];

		EnhanceImage(pSrcBuffer,  pDstBuffer,  nImgWidth,  nImgHeight, (EN_FILTEREDIRECTION)eFilterDirection, fSigma,  fScaleFactor);
		return dst;
	}
	 array<Byte>^ ImageProcess::Enhance_Threshold_OtsuFilter(array<Byte>^ src, int nImgWidth, int nImgHeight, bool bWhite, int nAbsoluteThresh)
	{
		pin_ptr<Byte> pSrcBuffer = &src[src->GetLowerBound(0)];

		array<Byte>^ dst = gcnew array<Byte>(src->Length);
		pin_ptr<Byte> pDstBuffer = &dst[dst->GetLowerBound(0)];
		
		Enhance_Threshold_Otsu( pSrcBuffer,  pDstBuffer,  nImgWidth,  nImgHeight,  bWhite,  nAbsoluteThresh);
		return dst;
	}




	 int ImageProcess::RecipeRun(String^ RecipeData, String^ RecipeParam, array< array<unsigned char>^>^, array< array<unsigned char>^>^, int width, int height)
	{
		 return -1;
	}
	 int ImageProcess::RecipeRun(String^ RecipeData, String^ RecipeParam, array< array<unsigned char>^>^ src, array< array<unsigned char>^>^ ref, array< array<unsigned char>^>^ dst, int width, int height)
	{
		return -1;
		ImageParam srcParam;
		{
			int imageCount = src->Length;

			srcParam.argc = src->Length;
			srcParam.argv = new ImageData * [srcParam.argc];

			for (int i = 0; i < imageCount; ++i)
			{
				ImageData* newImage = new ImageData;
				newImage->pData = new unsigned char[src[0]->Length];

				auto s = src[0];

				pin_ptr<Byte> pSrcBuffer = &src[0][src[0]->GetLowerBound(0)];
				memcpy(newImage->pData, pSrcBuffer, src[0]->Length);

				srcParam.argv[i] = newImage;
			}
		}
		ImageParam refParam;
		{
			int imageCount = ref->Length;

			refParam.argc = ref->Length;
			refParam.argv = new ImageData * [srcParam.argc];

			for (int i = 0; i < imageCount; ++i)
			{
				ImageData* newImage = new ImageData;
				newImage->pData = new unsigned char[ref[0]->Length];

				auto s = ref[0];

				pin_ptr<Byte> pSrcBuffer = &ref[0][ref[0]->GetLowerBound(0)];
				memcpy(newImage->pData, pSrcBuffer, ref[0]->Length);

				refParam.argv[i] = newImage;
			}
		}

		
		ImageParam dstParam;
		char* str1 = (char*)Marshal::StringToHGlobalAnsi(RecipeData).ToPointer();
		char* str2 = (char*)Marshal::StringToHGlobalAnsi(RecipeParam).ToPointer();

		auto ret = RecipeExecute(str1, str2, &srcParam, &refParam, &dstParam);

		Marshal::FreeHGlobal((IntPtr)str2);
		Marshal::FreeHGlobal((IntPtr)str1);


		for (int i = 0; i < refParam.argc; ++i)
		{
			delete refParam.argv[i]->pData;
			delete refParam.argv[i];
		}

		for (int i = 0; i < srcParam.argc; ++i)
		{
			delete srcParam.argv[i]->pData;
			delete srcParam.argv[i];
		}

		for (int i = 0; i < dstParam.argc; ++i)
		{
			array<unsigned char>^ imageBuffer = gcnew array<Byte>(dstParam.argv[0]->szSize);
			Marshal::Copy((IntPtr)dstParam.argv[0]->pData, imageBuffer, 0, dstParam.argv[0]->szSize);


			
		}


	}
}
