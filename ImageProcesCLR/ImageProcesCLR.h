﻿#pragma once

using namespace System;

namespace ImageProcesCLR {
	public ref class ImageProcess
	{
	public: 
		ImageProcess();
		~ImageProcess();
	public :
		int  FuncTest(int a, int b);
		static double  Pattern_Matching(String^ DirectionM, String^ DirectionR, String^ DirectionD);
		static double  Pattern_MatchingECC(String^ DirectionM, String^ DirectionR);

		static array<Byte>^ EnhanceImageFilter(array<Byte>^ src, int nImgWidth, int nImgHeight, int eFilterDirection, float fSigma, float fScaleFactor);		
		static array<Byte>^ Enhance_Threshold_OtsuFilter(array<Byte>^ src, int nImgWidth, int nImgHeight, bool bWhite, int nAbsoluteThresh);


		static int RecipeRun(String^ RecipeData, String^ RecipeParam, array< array<Byte>^>^, array< array<Byte>^>^, int width, int height);
		static int RecipeRun(String^ RecipeData, String^ RecipeParam, array< array<Byte>^>^ src, array< array<Byte>^>^ ref, array< array<Byte>^>^ dst, int width, int height);

	};

	
}
