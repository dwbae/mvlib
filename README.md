# README #

장비 시스템에 적용할 이미지 처리 알고리즘을 모듈 개발

2개의 솔루션 파일이 존재함. 
1. VisionLibrary.sln (TBD)
    * 이미지 처리 알고리즘을 스크립트 형태로 적용 할 수 있는 라이브러리 개발.

1. Pattern_In_Pad_Masking_Filter.sln
    * 기존 VB에서 개발된 패드 패턴 매칭 필터 기능을 64bit C# 적용 검증.
    * OpenCV 3.2.0
    * C++, C++/Cli , C# 연동의 레퍼런스
    * 자세한 사항은 해당 솔루션의 README.md 참고. 

# TBD

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact