﻿using OpenCvSharp;
using OpenCvSharp.Extensions;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVisionWrapperNET
{
	public class MVisionWrapper
	{
		public enum EN_FILTEREDIRECTION
		{
			ENHANCE_XY = 0,
			ENHANCE_X = 1,
			ENHANCE_Y = 2,
		}

		public struct MV_FILTER
		{
			public float[] s_pKernel;
			public int s_nSize
			{
				get
				{
					if (s_pKernel != null)
						return s_pKernel.Length;
					else
						return 0;
				}
			}
			public int s_nDivisor;
		};

		public static Bitmap EnhanceImage(Bitmap image, float sigma, float scaleFactor, EN_FILTEREDIRECTION eFilterDirection)
		{
			if (image == null)
			{
				return null;
			}

			using (Mat img = image?.ToMat())
			{
				if (img == null)
					return null;
				using (Mat gry = ToGrayscale(img))
				using (Mat ret = EnhanceImage(gry, sigma, scaleFactor, eFilterDirection))
				{
					return ret.ToBitmap();
				}
			}
		}
		public static Mat ToGrayscale( Mat src)
		{
			if (src == null)
				return null;
			Mat grayscale;
			if (src.Channels() > 1)
			{
				grayscale = new Mat(src.Size(), MatType.CV_8UC1);
				Cv2.CvtColor(src, grayscale, ColorConversionCodes.BGR2GRAY);
			}
			else
			{
				grayscale = src.Clone(); ;
			}

			return grayscale;
		}
		private static Mat EnhanceImage(Mat image, float fSigma, float fScaleFactor, EN_FILTEREDIRECTION eFilterDirection)
		{
			if (image == null)
			{
				return null;
			}

			fSigma = Math.Max(1, fSigma);
			fScaleFactor = Math.Max(0.1F, fScaleFactor);

			Mat matDst = new Mat();
			using (Mat matRaw = image)
			using (Mat matFilterSrc = new Mat())
			using (Mat matFilterX = new Mat())
			using (Mat matFilterY = new Mat())
			{
				var Filters = FilterCreate(fSigma, fScaleFactor);

				MV_FILTER filterGUS = Filters.Item1;
				MV_FILTER filterLOG = Filters.Item2;

				for (int i = 0; i < filterGUS.s_nSize; i++)
				{
					filterGUS.s_pKernel[i] = filterGUS.s_pKernel[i] / filterGUS.s_nDivisor;
				}
				for (int i = 0; i < filterLOG.s_nSize; i++)
				{
					filterLOG.s_pKernel[i] = filterLOG.s_pKernel[i] / filterLOG.s_nDivisor;
				}


				Mat m_KrnlLOG = new Mat(filterLOG.s_nSize, 1, MatType.CV_32FC1, filterLOG.s_pKernel);
				Mat m_KrnlGUS = new Mat(filterGUS.s_nSize, 1, MatType.CV_32FC1, filterGUS.s_pKernel);

				int xn = filterLOG.s_nSize;
				int yn = filterGUS.s_nSize;
				int xAnchor = (xn >> 1); //+ 1;
				int yAnchor = (yn >> 1); //+ 1;


				int COLOR_RANGE_CENTER_VALUE = 128;
				matRaw.ConvertTo(matFilterSrc, MatType.CV_32F, 1.0, -COLOR_RANGE_CENTER_VALUE);

				switch (eFilterDirection)
				{
					case EN_FILTEREDIRECTION.ENHANCE_X:
						Cv2.SepFilter2D(matFilterSrc, matFilterX, MatType.CV_32F, m_KrnlLOG, m_KrnlGUS, new OpenCvSharp.Point(xAnchor - 1, yAnchor - 1), 0, BorderTypes.Replicate);
						break;
					case EN_FILTEREDIRECTION.ENHANCE_Y:
						Cv2.SepFilter2D(matFilterSrc, matFilterX, MatType.CV_32F, m_KrnlGUS, m_KrnlLOG, new OpenCvSharp.Point(yAnchor - 1, xAnchor - 1), 0, BorderTypes.Replicate);
						break;
					default:
						Cv2.SepFilter2D(matFilterSrc, matFilterX, MatType.CV_32F, m_KrnlLOG, m_KrnlGUS, new OpenCvSharp.Point(xAnchor - 1, yAnchor - 1), 0, BorderTypes.Replicate);
						Cv2.SepFilter2D(matFilterSrc, matFilterY, MatType.CV_32F, m_KrnlGUS, m_KrnlLOG, new OpenCvSharp.Point(yAnchor - 1, xAnchor - 1), 0, BorderTypes.Replicate);
						Cv2.Add(matFilterX, matFilterY, matFilterX);
						break;
				}


				using (Mat matShift = matFilterX + COLOR_RANGE_CENTER_VALUE)
				{
					matShift.ConvertTo(matDst, MatType.CV_8U);
				}

			}


			return matDst;
		}

		public static Bitmap Enhance_Threshold_Otsu(Bitmap image, bool white, int nAbsoluteThresh, out int threshold)
		{
			threshold = -1;
			if (image == null)
			{
				return null;
			}

			using (Mat img = image?.ToMat())
			{
				if (img == null)
					return null;
				using (Mat gry = ToGrayscale(img))
				using (Mat ret = Enhance_Threshold_Otsu(gry, white, nAbsoluteThresh, out threshold))
				{
					return ret.ToBitmap();
				}
			}
		}
		static Mat Enhance_Threshold_Otsu(Mat image,  bool bWhite, int nAbsoluteThresh, out int threshold)
		{
			int nReturn = 0;
			

			Mat matRaw = image;
			Mat matThresh = new Mat();


			double dThreshValue;

			if (bWhite)
			{
				Cv2.Threshold(matRaw, matThresh, 127, 255, ThresholdTypes.Binary);
				dThreshValue = otsu_8u_with_mask(matRaw, ref matThresh);

				dThreshValue = Math.Max(dThreshValue, nAbsoluteThresh);
				Cv2.Threshold(matRaw, matThresh, dThreshValue, 255, ThresholdTypes.Binary);
			}
			else
			{
				Cv2.Threshold(matRaw, matThresh, 128, 255, ThresholdTypes.BinaryInv);
				dThreshValue = otsu_8u_with_mask(matRaw,ref  matThresh);

				dThreshValue = Math.Max(dThreshValue, nAbsoluteThresh);
				Cv2.Threshold(matRaw, matThresh, dThreshValue, 255, ThresholdTypes.BinaryInv);
			}

			threshold = (int)dThreshValue;
		

			return matThresh;
		}


		private static (MV_FILTER, MV_FILTER) FilterCreate(double s_lfSigma, double fScaleFactor)
		{
			fScaleFactor = fScaleFactor * 0.2;
			s_lfSigma = Math.Max(s_lfSigma, 1.0F);



			double s_lfGusFltPeak = 71;
			double s_lfLOGFltPeak = 50;

			int nGusWidth = (int)Math.Floor(s_lfSigma * 4);
			int n_LogWidth = (int)Math.Floor(s_lfSigma * 8);

			int size = nGusWidth;
			int anc = size / 2;

			MV_FILTER filterGUS;
			MV_FILTER filterLOG;

			short[] pGUS = new short[size];

			filterGUS.s_pKernel = new float[size];

			double scale = Math.Abs(s_lfGusFltPeak / Gaussian(0.0, s_lfSigma));
			int sum = 0;
			char[] str = new char[80];

			for (int i = 0; i < size; i++)
			{
				int x = i - anc;
				double val = Gaussian(x, s_lfSigma) * scale;
				pGUS[i] = (short)(val + (val >= 0 ? 0.5 : -0.5));
				sum += pGUS[i];
			}

			// flip
			for (int i = 0; i < size; i++)
			{
				filterGUS.s_pKernel[i] = pGUS[size - i - 1];


			}
			System.Diagnostics.Debug.WriteLine($"MVISIONWrapper:CreateFilter GUS Kernel {string.Join(", ", filterGUS.s_pKernel)}");

			filterGUS.s_nDivisor = sum;



			/////////////////////////////////////////////////////////////////////////////////////////////////////
			/// LOG 

			scale = Math.Abs(s_lfLOGFltPeak / LOG(0.0, s_lfSigma));
			size = n_LogWidth;



			anc = size / 2;
			short[] pLOG = new short[size];
			filterLOG.s_pKernel = new float[size];
			sum = 0;
			for (int i = 0; i < size; i++)
			{
				int x = i - anc;
				double val = -1.0 * LOG(x, s_lfSigma) * scale * fScaleFactor;
				pLOG[i] = (short)(val + (val >= 0 ? 0.5 : -0.5));


				sum += pLOG[i];
			}
			// flip
			for (int i = 0; i < size; i++)
			{

				filterLOG.s_pKernel[i] = pLOG[size - i - 1];
			}
			System.Diagnostics.Debug.WriteLine($"MVISIONWrapper:CreateFilter LOG Kernel {string.Join(", ", filterLOG.s_pKernel)}");

			//			m_filterLOG.s_nSize = size;
			filterLOG.s_nDivisor = 32;


			if (sum != 0)
			{
				if ((size % 2) == 0)
					anc--;

				double ancValue = filterLOG.s_pKernel[anc];
				filterLOG.s_pKernel[n_LogWidth - 1] = filterLOG.s_pKernel[n_LogWidth - 1] - sum;

				System.Diagnostics.Debug.WriteLine($"MVISIONWrapper:CreateFilter make zero sum  .... anc{anc}  {ancValue} --> {filterLOG.s_pKernel[anc]}");

			}

			return (filterGUS, filterLOG);
		}


		static double Gaussian(double x, double sigma)
		{
			return (1.0 / (Math.Sqrt(2 * Math.PI) * sigma) * Math.Exp(-x * x / (2.0 * sigma * sigma)));
		}

		static double LOG(double x, double sigma)
		{
			double t;

			t = x * x / (sigma * sigma);
			return (1.0 / (Math.Sqrt(2 * Math.PI) * sigma * sigma * sigma) * (t - 1.0) * Math.Exp(t / -2.0));
		}

		static double otsu_8u_with_mask(Mat src, ref Mat mask)
		{
			const int N = 256;
			int M = 0; // total pixel count
			int i, j;
			int[] h = new int[N];

			for (i = 0; i < src.Rows; i++)
			{	
				for (j = 0; j < src.Cols; j++)
				{
					if (mask.At<byte>(j,i) !=0)
					{
						h[src.At<byte>(j,i)]++;
						++M;
					}
				}
			}

			double mu = 0, scale = 1.0 / (M);
			for (i = 0; i < N; i++)
				mu += i * (double)h[i];

			mu *= scale;
			double mu1 = 0, q1 = 0;
			double max_sigma = 0, max_val = 0;

			for (i = 0; i < N; i++)
			{
				double p_i, q2, mu2, sigma;

				p_i = h[i] * scale;
				mu1 *= q1;
				q1 += p_i;
				q2 = 1.0 - q1;

				if (Math.Min(q1, q2) < double.Epsilon || Math.Max(q1, q2) > 1.0 - double.Epsilon)
					continue;

				mu1 = (mu1 + i * p_i) / q1;
				mu2 = (mu - q1 * mu1) / q2;
				sigma = q1 * q2 * (mu1 - mu2) * (mu1 - mu2);


				if (sigma > max_sigma)
				{
					max_sigma = sigma;
					max_val = i;

				}
			}

			return max_val;
		}

	}
}
