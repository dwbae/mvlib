﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ImageViewer.DrawItem;
using System.Drawing.Drawing2D;
using ImageProcessNET;

namespace ImageViewer
{
    public partial class ViewControl : PictureBox
    {
        #region Property
        private Bitmap _srcImage;
        /// <summary>
        /// 표시할 이미지를 설정하거나 가져옵니다.
        /// </summary>
        public Bitmap SrcImage
        {
            get => _srcImage;
            set
            {
                try
                {
                    if (value != null)
                    {
                        _srcImage = (Bitmap)value.Clone();
                        drawHandler.ImageSize = new Size(SrcImage.Width, SrcImage.Height);
                    }
                    else
                    {
                        _srcImage = null;
                        drawHandler.ImageSize = new Size(1, 1);
                    }

                    drawHandler.CenterPixel = new PointF(drawHandler.ImageSize.Width / 2.0F, drawHandler.ImageSize.Height / 2.0F);
                    drawHandler.ImageZoom(1);

                    InvokeRefresh();
                }
                catch { }
            }
        }


        /// <summary>
        /// 화면에서 영역 설정을 허용할지 여부
        /// </summary>
        public bool SelectAreaEnable
        {
            get; set;
        } = false;


        /// <summary>
        /// 화면에 사각형의 영역을 설정하거나 설정된 영역을 가져옵니다.
        /// </summary>
        public RectangleF SelectArea
        {
            get
            {
                if (guideItemList != null && guideItemList.Count > 0 && guideItemList[0] is ExtendRect r)
                {
                    return r.Rectangle;
                }
                return new RectangleF(0, 0, 0, 0);
            }
            set
            {
                ClearExtendRect();
                if (value == null || value.X == 0 || value.Y == 0)
                {
                }
                else
                {
                    SetNewExtendRect(new PointF(value.X, value.Y), new PointF(value.Right, value.Bottom));
                }
                InvokeRefresh();
            }
        }

        #endregion

        #region Event
        /// <summary>
        /// 화면에 표시되는 사각형 영역이 변경되면 발생 합니다.
        /// </summary>
        public event EventHandler SelectAreaChanged;
        private void Rect_PointChanged(object sender, EventArgs e)
        {
            SelectAreaChanged?.Invoke(this, new EventArgs());
        }

        #endregion

        #region Variable
        private bool IsEditable =>
           (this.SrcImage != null) &&
           (this.SrcImage.Width > 0) &&
           (this.SrcImage.Height > 0);

        private DrawImageHandler drawHandler = new DrawImageHandler();
        List<DrawItemBase> guideItemList = new List<DrawItemBase>();
        private void AddDrawItem(DrawItemBase item)
        {
            if (guideItemList == null)
                guideItemList = new List<DrawItemBase>();
            guideItemList.Add(item);
        }


        #endregion

        #region SelectArea Manage
        private void ClearExtendRect()
        {
            if (guideItemList != null)
            {
                foreach (var item in guideItemList)
                {
                    if (item is ExtendRect r)
                    {
                        r.PointChanged -= Rect_PointChanged;
                    }
                }
                guideItemList.Clear();
            }
        }

        private ExtendRect SetNewExtendRect(PointF start, PointF end)
        {
            ExtendRect rect = new ExtendRect
            {
                Point1 = start,
                Point2 = end
            };
            rect.PointChanged += Rect_PointChanged;

            ClearExtendRect();
            AddDrawItem(rect);
            return rect;
        }

        #endregion


        public ViewControl()
        {
            this.Resize += ViewControl_Resize;

            this.MouseDown += MouseDownOnImage;
            this.MouseUp += MouseUpOnImage;
            this.MouseWheel += MouseWheelOnImage;
            this.MouseMove += MouseMoveOnImage;
        }
        #region UI Update
        delegate void voidFuncBool(bool bFlag);
        public void InvokeRefresh(bool bForce = false)
        {

            if (this.InvokeRequired)
            {
                voidFuncBool d = new voidFuncBool(InvokeRefresh);
                this.Invoke(d, new object[] { bForce });
            }
            else
            {

                cache?.Dispose();
                cache = null;


                this.Refresh();
            }
        }
        private void ViewControl_Resize(object sender, System.EventArgs e)
        {
            drawHandler.ViewArea = this.ClientRectangle;
            drawHandler.ImageZoom(drawHandler.ImageScale);
            Invalidate();
        }

        Brush text_brush = new SolidBrush(Color.Red);
        Bitmap cache;
        protected override void OnPaint(PaintEventArgs pe)
        {
            if (cache == null || cache.Width != pe.ClipRectangle.Width || cache.Height != pe.ClipRectangle.Height)
            {
                cache?.Dispose();
                cache = new Bitmap(pe.ClipRectangle.Width, pe.ClipRectangle.Height);

                using (Graphics g = Graphics.FromImage(cache))
                {
                    g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

                    g.FillRectangle(new SolidBrush(Color.White), pe.ClipRectangle);


                    //이미지가 화면에 보이는 영역 계산. 
                    Matrix matrix =
                        DrawImageHandler.GetImageDrawAreaMatrix(drawHandler.ImageDrawArea, drawHandler.ImageSize);
                    drawHandler.GetDisplayArea(out PointF screenLT, out PointF screenRB, out PointF imageLT,
                        out PointF imageRB);

                    //화면에 보이는 영역을 크롭해서 표시.

                    if (SrcImage != null)
                    {
                        using (Bitmap drawBitmap = ImageProcess.Resize(SrcImage, screenLT, screenRB, imageLT, imageRB))
                        {
                            if (drawBitmap != null)
                            {
                                g.DrawImage(drawBitmap,
                                    new Rectangle((int)screenLT.X, (int)screenLT.Y,
                                        (int)(screenRB.X - screenLT.X),
                                        (int)(screenRB.Y - screenLT.Y)));

                            }
                        }
                    }
                    else
                        g.DrawString("Draw by View Control", new Font("Tahoma", 24), text_brush, new PointF((float)pe.ClipRectangle.Width / 5, 0));

                    foreach (var drawItem in guideItemList)
                    {
                        drawItem.Draw(g, matrix);
                    }
                }

            }

            if (cache != null)
            {
                pe.Graphics.DrawImage(cache, pe.ClipRectangle.Location);
            }

        }
        #endregion

        #region Mouse Hit
        /// <summary>
        /// HitTest DragItem
        /// </summary>
        public enum ItemDrawMode
        {
            None,
            Rect,
        }

        private ItemDrawMode drawMode = ItemDrawMode.None;

        public ItemDrawMode DrawMode
        {
            get => drawMode;
            set
            {
                if (IsEditable)
                {
                    drawMode = value;
                    InvokeRefresh();
                }
            }
        }

        /// <summary>
        /// 현재 선택된 DrawItem
        /// </summary>
        DrawItemBase drawItemLBD;
        private System.Drawing.Point PointLBD = new System.Drawing.Point();
        private bool OnDrag;

        public DrawItemBase GetHitItem(Point pt)
        {
            PointF ptTarget = drawHandler.PointScreenToImage(pt);

            DrawItemBase ret = null;
            Matrix ivMatrix = DrawImageHandler.GetImageDrawAreaMatrix(drawHandler.ImageDrawArea, drawHandler.ImageSize);
            ivMatrix.Invert();

            if (ret == null)
            {
                foreach (var drawItem in guideItemList)
                {
                    if (drawItem.HitTest(ptTarget, ivMatrix))
                    {
                        return drawItem;
                    }
                }
            }

            return ret;
        }

        #endregion

        #region MouseEvent

        private void MouseDownOnImage(object sender, MouseEventArgs e)
        {
            this.Focus();
            if (!IsEditable) return;

            switch (e.Button)
            {
                case MouseButtons.Left:
                    PointLBD = e.Location;
                    OnDrag = true;
                    //DrawMode = ItemDrawMode.Rect;
                    drawItemLBD = GetHitItem(e.Location);

                    break;
                case MouseButtons.Right:
                    PointLBD = e.Location;
                    OnDrag = true;
                    if (SelectAreaEnable)
                        DrawMode = ItemDrawMode.Rect;
                    break;
                default:
                    break;
            }
            InvokeRefresh();
        }
        private void MouseUpOnImage(object sender, MouseEventArgs e)
        {
            if (!IsEditable) return;

            switch (e.Button)
            {
                case MouseButtons.Left:
                    {
                        OnDrag = false;
                        drawItemLBD = null;
                    }
                    break;
                case MouseButtons.Right:
                    {
                        OnDrag = false;
                        drawItemLBD = null;
                        DrawMode = ItemDrawMode.None;
                    }
                    break;
                default:
                    break;
            }
        }
        private void MouseMoveOnImage(object sender, MouseEventArgs e)
        {
            if (!IsEditable) return;

            System.Drawing.Point mouseDownLocation = new System.Drawing.Point(e.X, e.Y);

            if (OnDrag)
            {
                if (((Control.MouseButtons & MouseButtons.Left) != MouseButtons.Left) &&
                   ((Control.MouseButtons & MouseButtons.Right) != MouseButtons.Right))
                {
                    OnDrag = false;
                }

                if (drawItemLBD != null)
                {

                    PointF start = drawHandler.PointScreenToImage(PointLBD);
                    PointF end = drawHandler.PointScreenToImage(mouseDownLocation);

                    if ((Control.ModifierKeys & Keys.Alt) == Keys.Alt)
                    {

                    }
                    else
                    {
                        var item = GetHitItem(mouseDownLocation);

                    }
                    Matrix ivMatrix = DrawImageHandler.GetImageDrawAreaMatrix(drawHandler.ImageDrawArea, drawHandler.ImageSize);
                    ivMatrix.Invert();
                    drawItemLBD.Move(start, end, ivMatrix);
                    InvokeRefresh();
                }
                else if (DrawMode == ItemDrawMode.Rect)
                {
                    PointF start = drawHandler.PointScreenToImage(PointLBD);
                    PointF end = drawHandler.PointScreenToImage(mouseDownLocation);

                    drawItemLBD = SetNewExtendRect(start, end);

                    DrawMode = ItemDrawMode.None;
                }
                else
                {
                    drawHandler.ImageMove(mouseDownLocation.X - PointLBD.X, mouseDownLocation.Y - PointLBD.Y);
                    InvokeRefresh();
                }
                PointLBD = mouseDownLocation;
            }
            else
            {

                var item = GetHitItem(new Point(e.X, e.Y));
                if (item != null)
                {
                    Cursor.Current = item.GetHitCursor();
                }
                else
                {
                    Cursor.Current = Cursors.Default;
                }

            }

            return;
        }

        private void MouseWheelOnImage(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (!IsEditable) return;

            if (e.Delta > 0)
            {
                drawHandler.ImageZoomIn(drawHandler.ZoomScaleSmallStep, e.Location);
            }
            else
            {
                drawHandler.ImageZoomOut(drawHandler.ZoomScaleSmallStep, e.Location);
            }
            InvokeRefresh();
        }
        #endregion
    }
}
