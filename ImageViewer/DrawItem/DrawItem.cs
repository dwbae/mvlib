﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;



namespace ImageViewer.DrawItem
{
    public abstract class DrawItemBase
    {
        protected int hitTestMargin = 5;
        public abstract void Draw(Graphics g, Matrix m);
        public abstract bool HitTest(PointF pt, Matrix m);
        public abstract void Move(PointF start, PointF end, Matrix m);

        public abstract Cursor GetHitCursor();
    }

    /// <summary>
    /// 사각형 그리기 정보 
    /// </summary>
    public class ExtendRect : DrawItemBase
    {
        /// <summary>
        /// 위치가 이동하면 발생하는 이벤트
        /// </summary>
        public event EventHandler PointChanged;


        public static System.Drawing.Color Mango = System.Drawing.Color.FromArgb(249, 149, 33);

        Pen pen = new Pen(Color.Magenta, 1);
        Pen penCenter = new Pen(Mango, 1);
        int crossSize = 5;

        PointF pt1 = new PointF();
        PointF pt2 = new PointF();

        bool editable = true;

        /// <summary>
        /// 사각형의 대각선 좌표1 (위아래 좌우가 뒤 바뀔수 있음) 
        /// </summary>
        public PointF Point1
        {
            get => pt1;
            set
            {
                pt1 = value;
                PointChanged?.Invoke(this, new EventArgs());
            }
        }
        /// <summary>
        /// 사각형의 대각선 좌표2
        /// </summary>
        public PointF Point2
        {
            get => pt2;
            set
            {
                pt2 = value;
                PointChanged?.Invoke(this, new EventArgs());
            }
        }
        
        /// <summary>
        /// 대각선 좌표를 사각형 좌표로 변환
        /// </summary>
        public RectangleF Rectangle
        {
            get => new RectangleF(Math.Min(Point1.X, Point2.X), Math.Min(Point1.Y, Point2.Y), Math.Abs(Point1.X - Point2.X), Math.Abs(Point1.Y - Point2.Y));
        }

        /// <summary>
        /// 크기/ 위치 이동 가능 여부
        /// </summary>
        public bool Editable
        {
            get => editable;
            set => editable = value;
        }

        public ExtendRect()
        {

        }

        private PointF[] GetRectPoints(PointF ptlt, PointF ptrl)
        {
            return new PointF[] { ptlt, new PointF(ptrl.X, ptlt.Y), ptrl, new PointF(ptlt.X, ptrl.Y) };
        }

        public override void Draw(Graphics g, Matrix m)
        {
            var crosspoints = new PointF[] { Point1, Point2 };
            m.TransformPoints(crosspoints);

            var points = GetRectPoints(crosspoints[0], crosspoints[1]);

            g.DrawLine(pen, points[0], points[1]);
            g.DrawLine(pen, points[1], points[2]);
            g.DrawLine(pen, points[2], points[3]);
            g.DrawLine(pen, points[3], points[0]);


            if (!Editable)
                return;


            RectangleF rect1 = new RectangleF(points[0].X - crossSize, points[0].Y - crossSize, crossSize * 2, crossSize * 2);
            RectangleF rect2 = new RectangleF(points[1].X - crossSize, points[1].Y - crossSize, crossSize * 2, crossSize * 2);
            RectangleF rect3 = new RectangleF(points[2].X - crossSize, points[2].Y - crossSize, crossSize * 2, crossSize * 2);
            RectangleF rect4 = new RectangleF(points[3].X - crossSize, points[3].Y - crossSize, crossSize * 2, crossSize * 2);

            RectangleF rectIn = new RectangleF(Math.Min(points[0].X, points[2].X), Math.Min(points[0].Y, points[2].Y),
                Math.Abs(points[0].X - points[2].X), Math.Abs(points[0].Y - points[2].Y)
                );

            g.FillRectangle(new SolidBrush(Color.FromArgb(255, 40, 40, 40)), rect1);
            g.FillRectangle(new SolidBrush(Color.FromArgb(255, 40, 40, 40)), rect2);
            g.FillRectangle(new SolidBrush(Color.FromArgb(255, 40, 40, 40)), rect3);
            g.FillRectangle(new SolidBrush(Color.FromArgb(255, 40, 40, 40)), rect4);

            g.FillRectangle(new SolidBrush(Color.FromArgb(175, 40, 40, 40)), rectIn);
        }

        public override bool HitTest(PointF pt, Matrix m)
        {
            if (!Editable)
                return false;

            int ret = CheckHitTest(pt, m);
            if (ret >= 0)
                return true;

            return false;
        }

        enum HitPoint : int
        {
            None = -1,
            ConerLT = 0,
            ConerRT,
            ConerRB,
            ConerLB,
            EdgeTop,
            EdgeRight,
            EdgeBottom,
            EdgeLeft,
            Inner
        }

        private int CheckHitTest(PointF pt, Matrix m)
        {
            float dist = GetViewDistance(m);
            var points = GetRectPoints(Point1, Point2);
            //겹치는 경우 Point2가 우선. 최초에 겹치게 설정. 

            //모서리점 
            int[] priority = new int[4] { (int)HitPoint.ConerRB, (int)HitPoint.ConerRT, (int)HitPoint.ConerLB, (int)HitPoint.ConerLT };
            for (int i = 0; i < priority.Length; ++i)
            {
                RectangleF rectPoint = new RectangleF(points[priority[i]].X - dist, points[priority[i]].Y - dist, dist * 2, dist * 2);
                if (rectPoint.Contains(pt))
                {
                    return priority[i];
                }
            }

            //테두리

            RectangleF rectTop = new RectangleF(Math.Min(points[0].X, points[1].X), Math.Min(points[0].Y, points[1].Y) - dist, Math.Abs(points[1].X - points[0].X), dist * 2);
            if (rectTop.Contains(pt))
                return (int)HitPoint.EdgeTop;
            RectangleF rectRight = new RectangleF(Math.Min(points[1].X, points[2].X) - dist, Math.Min(points[1].Y, points[2].Y), dist * 2, Math.Abs(points[2].Y - points[1].Y));
            if (rectRight.Contains(pt))
                return (int)HitPoint.EdgeRight;
            RectangleF rectBot = new RectangleF(Math.Min(points[3].X, points[2].X), Math.Min(points[3].Y, points[2].Y) - dist, Math.Abs(points[2].X - points[3].X), dist * 2);
            if (rectBot.Contains(pt))
                return (int)HitPoint.EdgeBottom;
            RectangleF rectLeft = new RectangleF(Math.Min(points[0].X, points[3].X) - dist, Math.Min(points[0].Y, points[3].Y), dist * 2, Math.Abs(points[3].Y - points[0].Y));
            if (rectLeft.Contains(pt))
                return (int)HitPoint.EdgeLeft;

            //내부
            RectangleF rectInner = new RectangleF(Math.Min(points[0].X, points[2].X), Math.Min(points[0].Y, points[2].Y),
              Math.Abs(points[0].X - points[2].X), Math.Abs(points[0].Y - points[2].Y));

            if (rectInner.Contains(pt))
                return (int)HitPoint.Inner;

            return (int)HitPoint.None;
        }

        private float GetViewDistance(Matrix m)
        {
            var points = new PointF[] { new PointF(0, 0), new PointF(0, 1) };
            m.TransformPoints(points);

            float dist = (points[1].Y - points[0].Y) * hitTestMargin;
            return dist;
        }

        public PointF GetNearstPoint(PointF pt)
        {
            return pt;
        }


        public override void Move(PointF start, PointF end, Matrix m)
        {
            var hitResult = (HitPoint)CheckHitTest(start, m);
            if (hitResult > HitPoint.None)
            {
                switch (hitResult)
                {
                    case HitPoint.ConerLT:
                        {//point1을 이동                     

                            pt1.X = end.X;
                            pt1.Y = end.Y;

                        }
                        break;
                    case HitPoint.ConerRT:
                        {
                            float dx = end.X - start.X;
                            float dy = end.Y - start.Y;

                            pt2.X += dx;
                            pt1.Y += dy;

                        }
                        break;
                    case HitPoint.ConerRB:
                        {//point2을 이동.
                            pt2.X = end.X;
                            pt2.Y = end.Y;

                        }
                        break;
                    case HitPoint.ConerLB:
                        {
                            float dx = end.X - start.X;
                            float dy = end.Y - start.Y;

                            pt1.X += dx;
                            pt2.Y += dy;

                        }
                        break;
                    case HitPoint.EdgeTop:
                        {
                            float dy = end.Y - start.Y;
                            pt1.Y += dy;
                        }
                        break;
                    case HitPoint.EdgeRight:
                        {
                            float dx = end.X - start.X;
                            pt2.X += dx;

                        }
                        break;
                    case HitPoint.EdgeBottom:
                        {
                            float dy = end.Y - start.Y;
                            pt2.Y += dy;
                        }
                        break;
                    case HitPoint.EdgeLeft:
                        {
                            float dx = end.X - start.X;
                            pt1.X += dx;

                        }
                        break;
                    case HitPoint.Inner:
                        {
                            float dx = end.X - start.X;
                            float dy = end.Y - start.Y;

                            pt1.X += dx;
                            pt1.Y += dy;

                            pt2.X += dx;
                            pt2.Y += dy;
                        }
                        break;
                    default:
                        break;
                }
                PointChanged?.Invoke(this, new EventArgs());
            }
        }

        public override Cursor GetHitCursor()
        {
            return Cursors.SizeAll;
        }
    }
}
