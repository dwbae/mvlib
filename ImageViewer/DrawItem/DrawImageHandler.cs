﻿using ImageViewer.Extension;
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;

namespace ImageViewer.DrawItem
{

    /// <summary>
    /// 화면에 표시될 이미지 영역을 관리하는 
    /// </summary>
    class DrawImageHandler
    {
        public readonly float ZoomScaleStep = 30F;
        public readonly float ZoomScaleSmallStep = 1.25F;

        /// <summary>
        /// 화면 영역
        /// </summary>
        public Rectangle ViewArea = new Rectangle();

        /// <summary>
        /// 그려질 이미지 영역 , 이미지가 확대가 되면 이 영역이 커짐.
        /// 이미지가 이동하면 이영역이 이동함. 
        /// </summary>
        public RectangleF ImageDrawArea = new RectangleF();

        /// <summary>
        /// 이미지 크기
        /// </summary>
        public Size ImageSize = new Size();

        /// <summary>
        /// 픽셀 좌표 기준 화면 중심점
        /// </summary>
        public PointF CenterPixel = new PointF(0, 0);



        private float imageScale = 1;
        public float ImageScale
        {
            get => imageScale;
            set
            {
                float newScale = value;
                if (newScale < 1)
                    newScale = 1;

                if (ImageSize.Width >= 0 && ImageSize.Height >= 0)
                {
                    var defaultImageSize = GetFullSizeToScreen();

                    //var drawWidth = defaultImageSize.Width * imageScale;
                    // 100 = defaultImageSize.Width * imageScale / srcImage.Width
                    // imageScale = 100*srcImage.Width/defaultImageSize.Width 
                    if (newScale > (ImageSize.Width / defaultImageSize.Width) * 100)
                    {
                        newScale = (ImageSize.Width / defaultImageSize.Width) * 100;
                    }
                }

                if (float.IsInfinity(newScale))
                    newScale = 1;
                else if (float.IsNaN(newScale))
                    newScale = 1;

                if (imageScale != newScale)
                {
                    imageScale = newScale;
                }
            }
        }


        public event EventHandler ScaleChanged;

        
        /// <summary>
        /// 이미지 이동
        /// </summary>
        /// <param name="dx"></param>
        /// <param name="dy"></param>
        public void ImageMove(int dx, int dy)
        {
            ImageDrawArea.X += dx;
            ImageDrawArea.Y += dy;

            ImageDrawArea = AreaBondCheck(ViewArea, ImageDrawArea);
            CenterPixel = GetViewImageCenter(ViewArea, ImageDrawArea, ImageSize);
        }

        public bool ImageZoomAt(float scale, PointF zoomPixel)
        {
            if (zoomPixel.X <= 0 || zoomPixel.Y <= 0)
                return false;
            CenterPixel = zoomPixel;
            ImageZoom(scale);
            return true;
        }
        /// <summary>
        /// 이미지 확대/축소
        /// </summary>
        /// <param name="scale"></param>
        public void ImageZoom(float scale)
        {
            ImageScale = scale;
            ImageDrawArea = GetZoomImageDrawArea(ViewArea, ImageSize, ImageScale);
            ImageMoveToCenterPixel(CenterPixel);
            ImageDrawArea = AreaBondCheck(ViewArea, ImageDrawArea);
            CenterPixel = GetViewImageCenter(ViewArea, ImageDrawArea, ImageSize);
            ScaleChanged?.Invoke(this, new EventArgs());
        }

        public void ImageZoomIn(float zoomStep, Point ptMouse)
        {
            ImageZoom(ImageScale * zoomStep, ptMouse);
        }

        public void ImageZoomOut(float zoomStep, Point ptMouse)
        {
            ImageZoom(ImageScale / zoomStep, ptMouse);
        }
        /// <summary>
        /// 마우스 커서가 위치한 포인트를 유지 하는 확대 /축소
        /// </summary>
        /// <param name="scale"></param>
        /// <param name="ptMouse"></param>
        public void ImageZoom(float scale, Point ptMouse)
        {

            ///변경전 마우스위치의 이미지 포인트
            PointF ptImage = PointScreenToImage(ptMouse);
            ImageScale = scale;
            ImageDrawArea = GetZoomImageDrawArea(ViewArea, ImageSize, ImageScale);
            System.Diagnostics.Debug.WriteLine($"GetDrawArea1 scale: {ImageDrawArea} {CenterPixel} ");
            ImageMoveToCenterPixel(CenterPixel);
            System.Diagnostics.Debug.WriteLine($"GetDrawArea1 move: {ImageDrawArea} ");
            Matrix ivMatrix = GetImageDrawAreaMatrix(ImageDrawArea, ImageSize);
            ivMatrix.Invert();
            ///변경후 마우스 위치의 이미지 포인트
            PointF resizeImagePoint = ivMatrix.GetTransformPoint(ptMouse);

            CenterPixel.X += ptImage.X - resizeImagePoint.X;
            CenterPixel.Y += ptImage.Y - resizeImagePoint.Y;

            //마우스 포인트 위치의 이미지와 확대후 마우스 포인트 이미지 위치를 일치시킴. 
            ImageMoveToCenterPixel(CenterPixel);
            ImageDrawArea = AreaBondCheck(ViewArea, ImageDrawArea);
            CenterPixel = GetViewImageCenter(ViewArea, ImageDrawArea, ImageSize);
            ScaleChanged?.Invoke(this, new EventArgs());
        }


        /// <summary>
        /// 이미지 상의 좌표를 화면 중심으로 이동
        /// </summary>
        /// <param name="ptCenter"></param>
        public void ImageMoveToCenterPixel(PointF ptCenter)
        {
            CenterPixel = ptCenter;
            ImageDrawArea = GetCenterPointImageDrawArea(ViewArea, ImageDrawArea, ImageSize, ptCenter);
        }

        public float GetImageScale()
        {
            if (ImageSize.Width <= 0)
            {
                return 1;
            }

            return ImageDrawArea.Width / ImageSize.Width;
        }
        public SizeF GetPixelSize()
        {
            float divX = ImageDrawArea.Width / (ImageSize.Width == 0 ? ImageDrawArea.Width : ImageSize.Width);
            float divY = ImageDrawArea.Height / (ImageSize.Height == 0 ? ImageDrawArea.Height : ImageSize.Height);
            return new SizeF(divX, divY);
        }
        /// <summary>
        /// 화면좌표를 이미지의 실제 픽셀 좌표로 변환
        /// </summary>
        /// <param name="pt"></param>
        /// <returns></returns>
        public PointF PointScreenToImage(PointF pt)
        {
            var points = new PointF[] { pt };
            Matrix ivMatrix = GetImageDrawAreaMatrix(ImageDrawArea, ImageSize);
            ivMatrix.Invert();
            ivMatrix.TransformPoints(points);
            return points[0];

        }
        /// <summary>
        /// 이미지의 픽셀 좌표를 화면 좌표로 전환
        /// </summary>
        /// <param name="pt"></param>
        /// <returns></returns>
        public PointF PointImageToScreen(PointF pt)
        {
            var points = new PointF[] { pt };
            Matrix matrix = GetImageDrawAreaMatrix(ImageDrawArea, ImageSize);
            matrix.TransformPoints(points);
            return points[0];
        }

        public RectangleF GetFullSizeToScreen()
        {
            return GetFitRect(ViewArea, ImageSize);
        }
        #region Static Value Convert

        /// <summary>
        /// 화면에 표시되는 이미지영역 계산 (마진 추가되어 있음) 
        /// </summary>
        /// <param name="screenLT"></param>
        /// <param name="screenRB"></param>
        /// <param name="imageLT"></param>
        /// <param name="imageRB"></param>
        public void GetDisplayArea(out PointF screenLT, out PointF screenRB, out PointF imageLT, out PointF imageRB)
        {
            int margin = 100;
            screenLT = new PointF(-margin, -margin);
            screenRB = new PointF(ViewArea.Width + margin, ViewArea.Height + margin);
            //화면 표시 영역보다 조금더 크게 계산. 

            if (screenLT.X < ImageDrawArea.X)
                screenLT.X = ImageDrawArea.X;
            if (screenLT.Y < ImageDrawArea.Y)
                screenLT.Y = ImageDrawArea.Y;

            if (screenRB.X > ImageDrawArea.Right)
                screenRB.X = ImageDrawArea.Right;
            if (screenRB.Y > ImageDrawArea.Bottom)
                screenRB.Y = ImageDrawArea.Bottom;

            //이미지 크롭은 픽셀 기준으로 처리되므로 정수값으로 변경하여, 화면 위치 Float로 다시 변환. 
            // 화면 영역을 확대한 이미지의 픽셀로 계산
            imageLT = PointScreenToImage(screenLT);
            imageRB = PointScreenToImage(screenRB);
            //픽셀 영역을 정수로 전환. 
            imageLT.X = (int)Math.Floor(imageLT.X);
            imageLT.Y = (int)Math.Floor(imageLT.Y);
            imageRB.X = (int)Math.Ceiling(imageRB.X);
            imageRB.Y = (int)Math.Ceiling(imageRB.Y);

            if (imageLT.X < 0)
                imageLT.X = 0;
            if (imageLT.Y < 0)
                imageLT.Y = 0;

            if (imageRB.X > ImageSize.Width)
                imageRB.X = ImageSize.Width;
            if (imageRB.Y > ImageSize.Height)
                imageRB.Y = ImageSize.Height;

            //이미지 픽셀 영역을 화면 좌표 영역으로 변환. 
            screenLT = PointImageToScreen(imageLT);
            screenRB = PointImageToScreen(imageRB);
        }

        public static RectangleF GetCenterPointImageDrawArea(Rectangle viewarea, RectangleF drawrect, Size imageSize, PointF ptCenter)
        {
            //이미지 영역의 중심점
            PointF center = new PointF(drawrect.Width / 2, drawrect.Height / 2);

            //화면 중심
            float screenCenterX = viewarea.Width / 2.0F;
            float screenCenterY = viewarea.Height / 2.0F;

            //화면에 중심에 맞춤.                         
            drawrect.X = (screenCenterX - center.X);
            drawrect.Y = (screenCenterY - center.Y);


            ////화면픽셀 비율.
            float divX = drawrect.Width / (imageSize.Width == 0 ? 1 : imageSize.Width);
            float divY = drawrect.Height / (imageSize.Height == 0 ? 1 : imageSize.Height);

            float imageCenterX = imageSize.Width / 2.0F;
            float imageCenterY = imageSize.Height / 2.0F;

            drawrect.X -= (ptCenter.X - imageCenterX) * divX;
            drawrect.Y -= (ptCenter.Y - imageCenterY) * divY;



            return new RectangleF(drawrect.X, drawrect.Y, drawrect.Width, drawrect.Height);

        }

        public static RectangleF GetZoomImageDrawArea(RectangleF area, Size imageSize, float scale)
        {
            var f = GetFitRect(area, imageSize);

            float resizeWidth = f.Width * scale;
            float resizeHeight = f.Height * scale;

            return new RectangleF(
                f.X + (resizeWidth - f.Width) / 2.0F,
                f.Y + (resizeHeight - f.Height) / 2.0F,
                resizeWidth,
                resizeHeight);
        }



        public static Matrix GetImageDrawAreaMatrix(RectangleF drawrect, Size imageSize)
        {
            Matrix matrix = new Matrix();
            matrix.Translate(drawrect.X, drawrect.Y);
            if (imageSize.Width <= 0 || imageSize.Height <= 0 || drawrect.Width <= 0 || drawrect.Height <= 0)
            {
                matrix.Scale(1, 1);
            }
            else
            {
                matrix.Scale(drawrect.Width / imageSize.Width, drawrect.Height / imageSize.Height);
            }
            return matrix;
        }

        public static PointF GetViewImageCenter(Rectangle viewarea, RectangleF drawrect, Size imageSize)
        {
            Matrix matrix = GetImageDrawAreaMatrix(drawrect, imageSize);
            matrix.Invert();
            return matrix.GetTransformPoint(new PointF(viewarea.Width / 2.0F, viewarea.Height / 2.0F));
        }

        public static RectangleF AreaBondCheck(Rectangle area, RectangleF rect)
        {
            RectangleF baseRect = new RectangleF(rect.X, rect.Y, rect.Width, rect.Height);
            //이미지 영역의 중심점
            PointF center = new PointF(rect.Width / 2, rect.Height / 2);

            //화면 중심
            float screenCenterX = area.Width / 2.0F;
            float screenCenterY = area.Height / 2.0F;

            //화면에 중심에 맞춤.                         
            baseRect.X = (screenCenterX - center.X);
            baseRect.Y = (screenCenterY - center.Y);

            float leftmargin = baseRect.X;
            float rightmargin = baseRect.X + baseRect.Width;
            float topmargin = baseRect.Y;
            float bottommargin = baseRect.Y + baseRect.Height;

            if (leftmargin < 0)
                leftmargin = 0;
            if (topmargin < 0)
                topmargin = 0;

            if (rightmargin > area.Width)
                rightmargin = area.Width;
            if (bottommargin > area.Height)
                bottommargin = area.Height;

            if (rect.X > leftmargin)
                rect.X = leftmargin;
            if (rect.X + rect.Width < rightmargin)
                rect.X = rightmargin - rect.Width;

            if (rect.Y > topmargin)
                rect.Y = topmargin;
            if (rect.Y + rect.Height < bottommargin)
                rect.Y = bottommargin - rect.Height;

            return new RectangleF(rect.X, rect.Y, rect.Width, rect.Height);
        }

        /// <summary>
        /// 영역에 꽉차는 비율 유지 사각형 영역 구하기 (가운데 정렬)
        /// </summary>
        /// <param name="area"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        public static RectangleF GetFitRect(RectangleF area, Size imageSize)
        {
            if (imageSize.Width <= 0 || imageSize.Height <= 0)
                return area;
            // 이미지 비율에 비율에 맞추기
            float fitheight = area.Width * imageSize.Height / (float)imageSize.Width;
            float fitwidth = area.Height * imageSize.Width / (float)imageSize.Height;

            if (fitheight <= area.Height)
            {
                float locationY = (area.Height - fitheight) / 2F;
                area = new RectangleF(0, locationY, area.Width, fitheight);
            }
            else
            {
                float locationX = (area.Width - fitwidth) / 2F;
                area = new RectangleF(locationX, 0, fitwidth, area.Height);
            }
            return area;
        }
        #endregion
    }
}
