﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace ImageViewer.Extension
{
    public static class MatrixExtension
    {
        public static PointF GetTransformPoint(this Matrix m, PointF pt)
        {
            if (pt == null)
                return pt;

            if (m == null)
                return pt;

            var points = new PointF[] { pt };
            m.TransformPoints(points);
            return points[0];
        }
    }
}
