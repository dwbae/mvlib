﻿// ImageProcess64Test.cpp : 이 파일에는 'main' 함수가 포함됩니다. 거기서 프로그램 실행이 시작되고 종료됩니다.
//

#include <stdio.h>
#include <Windows.h>
#include <tchar.h>
#include <iostream>

#pragma warning(disable:4996)

#include "../Inc/Pattern_Pad.h"


#include "opencv2/opencv.hpp"
//#include "../opencv320/include/opencv2/imgcodecs.hpp"
//#include "../opencv320/include/opencv2/imgproc.hpp"

#ifdef _DEBUG
#pragma comment(lib,"../opencv320/64bit/x64/vc14/lib/opencv_world320d.lib")
#elif
#pragma comment(lib,"../opencv320/64bit/x64/vc14/lib/opencv_world320.lib")
#endif

#define BUFFER_CONST 8192 

#define _CRTDBG_MAP_ALLOC


#include <cstdlib>
#include <crtdbg.h>

#ifdef _DEBUG
#define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
// Replace _NORMAL_BLOCK with _CLIENT_BLOCK if you want the
// allocations to be of _CLIENT_BLOCK type
#else
#define DBG_NEW new
#endif

void OutputFormattedDebugString(LPCTSTR format, ...) {
	static TCHAR buffer[BUFFER_CONST] = { _T('\0'), }; // TCHAR형 문자 버퍼

	va_list arg; // 가변인수 벡터

	va_start(arg, format);
	_vstprintf(buffer, format, arg);
	va_end(arg);

	OutputDebugString(buffer);
	_tprintf(buffer);
	_tprintf(_T("\n"));
	ZeroMemory(buffer, sizeof(buffer)); // 사용 후 버퍼 내용 지우기
}

int patternMatchingTest(int argc, TCHAR* argv[]);
int recipeRunTest(int argc, TCHAR* argv[]);

int _tmain(int argc, TCHAR* argv[])
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	//_CrtDumpMemoryLeaks();

	patternMatchingTest(argc, argv);

	recipeRunTest(argc, argv);


	_CrtDumpMemoryLeaks();
	return 0;
}

int patternMatchingTest(int argc, TCHAR* argv[])
{
	OutputFormattedDebugString(_T("OpenCV Test 1\n"));
	int c = Add(1, 2);

	if (argc < 3)
		return 0;


	if (sizeof(TCHAR) == 2)
	{
		wchar_t strUnicode[256] = { 0, };

		char strMultibyteArg1[256] = { 0, };
		char strMultibyteArg2[256] = { 0, };
		char strMultibyteArg3[256] = { 0, };
		int len = 0;

		len = WideCharToMultiByte(CP_ACP, 0, argv[1], -1, NULL, 0, NULL, NULL);
		if (len > 255)
		{
			_tprintf(_T("Arg1 Convert Fail\n"));
			return -1;
		}
		WideCharToMultiByte(CP_ACP, 0, argv[1], -1, strMultibyteArg1, len, NULL, NULL);

		len = WideCharToMultiByte(CP_ACP, 0, argv[2], -1, NULL, 0, NULL, NULL);
		if (len > 255)
		{
			_tprintf(_T("Arg2 Convert Fail\n"));
			return -2;
		}
		WideCharToMultiByte(CP_ACP, 0, argv[2], -1, strMultibyteArg2, len, NULL, NULL);

		len = WideCharToMultiByte(CP_ACP, 0, argv[3], -1, NULL, 0, NULL, NULL);
		if (len > 255)
		{
			_tprintf(_T("Arg3 Convert Fail\n"));
			return -3;
		}
		WideCharToMultiByte(CP_ACP, 0, argv[3], -1, strMultibyteArg3, len, NULL, NULL);

		double ret = VBcv1032Pattern_Matching(strMultibyteArg1, strMultibyteArg2, strMultibyteArg3);
		//unsigned char* src[3] = { (unsigned char*)strMultibyteArg1, (unsigned char*)strMultibyteArg2, (unsigned char*)strMultibyteArg3 };


		printf("Result : %lf\n", ret);
	}
	else
	{
		printf("Please Set UNICODE");
	}
}

int recipeRunTest(int argc, TCHAR* argv[])
{
	//FILE* fpRecipe = fopen("..\\Sample\\ECC_Test.json", "r");
	FILE* fpRecipe = fopen("..\\Sample\\gaussian.json", "r");
	char* recipe = nullptr;

	char strMultibyteArg1[256] = { 0, };

	int len = 0;
	len = WideCharToMultiByte(CP_ACP, 0, argv[1], -1, NULL, 0, NULL, NULL);
	if (len > 255)
	{
		_tprintf(_T("Arg1 Convert Fail\n"));
		return -1;
	}

	if (fpRecipe != nullptr)
	{

		fseek(fpRecipe, 0, SEEK_END);
		auto size = ftell(fpRecipe);
		char* readbuffer = new char[(size + 1)]{ 0, };
		fseek(fpRecipe, 0, SEEK_SET);
		fread(readbuffer, sizeof(char), size, fpRecipe);

		len = MultiByteToWideChar(CP_UTF8, 0, readbuffer, size, NULL, NULL);

		recipe = new char[len * 2 + 2]{ 0, };
		WCHAR* wbuffer = (WCHAR*)recipe;
		MultiByteToWideChar(CP_UTF8, 0, readbuffer, size, wbuffer, len);
		delete[] readbuffer;
		fclose(fpRecipe);
	}

	ImageParam* src = new ImageParam();
	ImageParam* ref = new ImageParam();
	ImageParam* dst = new ImageParam();

	src->argc = 1;
	src->argv = new ImageData * [1];

	cv::Mat img = cv::imread(strMultibyteArg1);
	cv::imwrite("C:\\Repos\\input.jpg", img);
	src->argv[0] = new ImageData;
	src->argv[0]->channel = img.channels();
	src->argv[0]->width = img.size().width;
	src->argv[0]->height = img.size().height;
	src->argv[0]->szSize = img.total() * img.elemSize();
	src->argv[0]->pData = new unsigned char[src->argv[0]->szSize];
	memcpy(src->argv[0]->pData, img.data, src->argv[0]->szSize);
	img.release();
	int nRet = RecipeExecute(recipe, NULL, src, ref, dst);
	delete[] recipe;
	if (dst->argc > 0)
	{

		cv::Mat result;
		result.create(cv::Size(dst->argv[0]->width, dst->argv[0]->height), CV_8UC(dst->argv[0]->channel));

		memcpy(result.data, dst->argv[0]->pData, dst->argv[0]->szSize);

		cv::imwrite("C:\\Repos\\output.jpg", result);

		result.release();
	}

	delete src;
	delete ref;
	delete dst;
}