//----------------------------------------------------------------------------
/** @file Filter_Opencv.cpp
	@brief 여기에는 OpenCV를 사용하는 필터의 동작을 정의합니다.
*/
//----------------------------------------------------------------------------

#ifdef _X64
#include "../ImageProcess64/pch.h"
#endif 

#include "Filters.h"
#include "ImageUtil.h"

//using namespace cv;

Mat Filter::Proc(Mat value)
{
	return value;
}

Mat BlurFilter::Proc(Mat value)
{
	Mat ret;
	GaussianBlur(value, ret, cv::Size(3, 3), 3);
	return ret;
}


Mat SourceFilter::Proc(Mat value)
{
	tstring path = _T("C:\\Sample.jpg");
#ifdef _UNICODE
	string multiByte;
	multiByte.assign(path.begin(), path.end());
	return imread(multiByte);
#else
	return imread(path);
#endif  
}
