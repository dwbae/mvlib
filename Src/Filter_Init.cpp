//----------------------------------------------------------------------------
/** @file Filter_Init.cpp
	@brief 여기에는 Recipe.json에 기록된 필터 정보를 사용하여 해당 필터를 초기화 하는 내용을 작성합니다. 
*/
//----------------------------------------------------------------------------
#pragma once
#include "Filter.h"
#include "RecipeJsonDefine.h"

#include <Windows.h>


/// <summary>
/// Recipe 파일을 처리하기 위한 최상위 파싱 함수
/// </summary>
/// <param name="recipe">unicode json stirng </param>
void Recipe::Parse(const char* recipe)
{
	GenericDocument<ENCODE> doc;
	WCHAR* buffer = (WCHAR*)recipe;
	doc.Parse<0, ENCODE>(buffer);
	this->Init(doc);
}

/// <summary>
/// Recipe Json을 분석 하는 함수
/// </summary>
/// <param name="value">rapidjson 으로 변환된 recipe </param>
/// <returns>처리결과 </returns>
int Recipe::Init(GenericValue<ENCODE>& value)
{

	GETJSONSTRING(value, RECIPE_JSON_NAME, strName);
	GETJSONSTRING(value, RECIPE_JSON_COMMENT, strComment);

	if (value.HasMember(RECIPE_JSON_FILTERS))
	{
		GenericValue<ENCODE>& fs = value[RECIPE_JSON_FILTERS];
		for (SizeType i = 0; i < fs.Size(); ++i)
		{
			tstring strType;
			GETJSONSTRING(fs[i], RECIPE_JSON_FILTER_TYPE, strType);
			
			auto obj = CFactory::Instance()->CreateInstance(strType);
			Filter* filter = dynamic_cast<Filter*>(obj);
			if (filter != nullptr)
			{	
				Filters.push_back(filter);
				filter->Print();
			}
		}
	}

	return 0;
		
}

int Filter::Init(GenericValue<ENCODE>& value) {

	GETJSONSTRING(value, RECIPE_JSON_FILTER_NAME, strName);
	/*if (value.HasMember(RECIPE_JSON_FILTER_NAME))
	{
		strName = value[RECIPE_JSON_FILTER_NAME].GetString();
	}*/

	GETJSONSTRING(value, RECIPE_JSON_FILTER_KEY, strKey);

	GETJSONSTRING(value, RECIPE_JSON_FILTER_ENABLE, Enable);
	GETJSONSTRING(value, RECIPE_JSON_FILTER_IS_END_POINT, IsEndPoint);

	/*if (value.HasMember(RECIPE_JSON_FILTER_KEY))
	{
		strKey = value[RECIPE_JSON_FILTER_KEY].GetString();
	}
	if (value.HasMember(RECIPE_JSON_FILTER_ENABLE))
	{
		Enable = value[RECIPE_JSON_FILTER_ENABLE].GetBool();
	}
	if (value.HasMember(RECIPE_JSON_FILTER_IS_END_POINT))
	{
		IsEndPoint = value[RECIPE_JSON_FILTER_IS_END_POINT].GetBool();
	}*/

	return 0;

}
