﻿#include "pch.h"

#include "Filter.h"
#include "Pin.h"
#include "Param.h"
using namespace cv;
using namespace std;

Filter::Filter()
{
	strName = _T("##Filter");
	strKey = _T("##Key");
	bEnable = false;
	bIsEndPoint = false;
}

Filter::~Filter()
{
	
	Dispose();
}
void Filter::Print()
{
	cout << "Filter" << endl;
}

void Filter::Process()
{
	Proc(Pins, Params);
}

void Filter::Dispose()
{
	vector<IPin*>::iterator itorEnd = Pins.end();
	for (vector<IPin*>::iterator itor =
		Pins.begin();
		itor != itorEnd;
		++itor
		)
	{
		Pin* p = dynamic_cast<Pin*>(*itor);
		if (p != nullptr)
			delete p;
		else
			delete (*itor);
		 
	}
	Pins.clear();


	for (vector<IParam*>::iterator itor =
		Params.begin();
		itor != Params.end();
		++itor
		)
	{
		imgproc::CParam* p = dynamic_cast<imgproc::CParam*>(*itor);
		if (p != nullptr)
			delete p;
		else
			delete (*itor);
	}
	Params.clear();
}

void Filter::Proc(vector<IPin*> pins, vector<IParam*> params)
{
	Mat* img=nullptr;
	for (auto itor = pins.begin(); itor != pins.end(); ++itor)//for (const auto& pin : pins)
	{
		if (!(*itor)->IsOutput())
		{
			img = (*itor)->GetImage();
		}
	}

	for (auto itor = pins.begin(); itor != pins.end(); ++itor)//for (const auto& pin : pins)
	{
		if ((*itor)->IsOutput())
		{
			(*itor)->SetImage(img);
		}
	}
	//img.release();
}

vector<IPin*> Filter::GetOutPin()
{
	vector<IPin*> ret;

	for (auto itor = Pins.begin(); itor != Pins.end(); ++itor)//for (const auto& pin : Pins)
	{
		if ((*itor)->IsOutput())
		{
			ret.push_back((*itor));
		}
	}

	return ret;
}

IPin* Filter::GetPin(LPCTSTR pinKey)
{
	for (auto itor = Pins.begin(); itor != Pins.end(); ++itor)//for (const auto& pin : Pins)
	{
		if (_tcscmp((*itor)->GetKey(), pinKey) == 0)
		{
			return (*itor);
		}
	}
	return nullptr;
}


int Filter::Init(GenericValue<ENCODE>& value)
{

	if (value.HasMember(RECIPE_JSON_FILTER_NAME))
	{
		strName = value[RECIPE_JSON_FILTER_NAME].GetString();
	}

	//GETJSONSTRING(value, RECIPE_JSON_FILTER_NAME, strName);
	GETJSONSTRING(value, RECIPE_JSON_FILTER_KEY, strKey);
	GETJSONBOOL(value, RECIPE_JSON_FILTER_ENABLE, bEnable);
	GETJSONBOOL(value, RECIPE_JSON_FILTER_IS_END_POINT, bIsEndPoint);

	return 1;
}

bool Filter::SetParam(LPCTSTR key, int value)
{
	for (int i = 0; i < Params.size(); ++i)
	{
		if (_tcscmp(key, Params[i]->GetName()) == 0)
		{
			imgproc::Param<int>* param = dynamic_cast<imgproc::Param<int>*>(Params[i]);
			param->SetValue(value);
			return true;
		}
	}
	return false;
}


bool Filter::SetParam(LPCTSTR key, LPCTSTR value)
{
	for (int i = 0; i < Params.size(); ++i)
	{
		if (_tcscmp(key, Params[i]->GetName()) == 0)
		{
			imgproc::Param<tstring>* param = dynamic_cast<imgproc::Param<tstring>*>(Params[i]);
			param->SetValue(value);
			return true;
		}
	}
	return false;
}

bool Filter::SetParam(LPCTSTR key, bool value)
{
	for (int i = 0; i < Params.size(); ++i)
	{
		if (_tcscmp(key, Params[i]->GetName()) == 0)
		{
			imgproc::Param<bool>* param = dynamic_cast<imgproc::Param<bool>*>(Params[i]);
			param->SetValue(value);
			return true;
		}
	}
	return false;
}

bool Filter::SetParam(LPCTSTR key, float value)
{
	for (int i = 0; i < Params.size(); ++i)
	{
		if (_tcscmp(key, Params[i]->GetName()) == 0)
		{
			imgproc::Param<float>* param = dynamic_cast<imgproc::Param<float>*>(Params[i]);
			param->SetValue(value);
			return true;
		}
	}
	return false;
}