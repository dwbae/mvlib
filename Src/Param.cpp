﻿#include "pch.h"

#include "Param.h"

using namespace imgproc;

CParam::CParam(LPCTSTR name)
{
	_name = name;
}

imgproc::CParam::~CParam()
{
	
}

LPCTSTR CParam::GetName()
{
	return _name.c_str();
}

void CParam::SetName(LPCTSTR name)
{
	_name = name;
}

