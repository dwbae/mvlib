﻿#pragma once

#include "pch.h"
#include "Pattern_Pad.h"

#include "opencv2/opencv.hpp"
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>

#define TRACE DebugOutput


using namespace cv;
using namespace std;

//vector<int> Marked_Axisx;
//vector<int> Ref_Axisy;
//vector<int> Marked_Axisy;
//vector<int> Ref_Axisx;
float Light_Resolution_Val;

//Debug String
char* GetLocalTimeStr();
void DebugOutput(const char* fmt, ...);

void DebugOutput(const char* fmt, ...)
{
	char buf[1024], obuf[1024];
	va_list argptr;
	int ret;
	va_start(argptr, fmt);
	ret = vsprintf(buf, fmt, argptr);
	va_end(argptr);
	sprintf(obuf, "%s [CVDLL] %s", GetLocalTimeStr(), buf);
	OutputDebugStringA(obuf);
}





char* GetLocalTimeStr()
{
	static SYSTEMTIME systime;
	static char buf[80];
	GetLocalTime(&systime);
	sprintf(buf, "%02d/%02d %02d:%02d:%02d:%03d", systime.wDay, systime.wMonth, systime.wHour, systime.wMinute, systime.wSecond, systime.wMilliseconds); // 130618
	return (char*)&buf;
}
//-Debug String
int Add(int a, int b)
{
	return a + b;
}

int Shift_Img(Mat& image1, Mat& image2, int dx, int dy)
{
	try {
		for (int y = dy; y < image2.rows; y++) {
			for (int x = dx; x < image2.cols; x++) {
				if (image1.channels() == 1) {
					uchar data = image1.at<uchar>(y - dy, x - dx);
					image2.at<uchar>(y, x) = data;
				}
				else if (image1.channels() == 3) {
					Vec3b colVal = image1.at<Vec3b>(y - dy, x - dx);
					image2.at<Vec3b>(y, x) = colVal;
				}
				else {
					TRACE("----------Image Channel(s) error - please check Mat type----------\n");
					break;
				}
			}
		}
	}
	catch (...) {
		TRACE("----------Shifting Image error - please check dx or dy values----------\n");
		return 1;
	}
}

float Light_Resolve(float Mark_Mean_Val_Local)
{
	Light_Resolution_Val = (256 - Mark_Mean_Val_Local) / (256 - ((float)0.96 * Mark_Mean_Val_Local));
	//마크 이미지의 밝기값이 220보다 크면 급격하게 밝기를 줄인다. 

	return Light_Resolution_Val;
}

int GetThresholdVal(Mat& IMG) {
	float Brightness_Mean = (mean(IMG)[0] * 0.95); //패턴 컨투어링을 위한 임계값 설정
	return Brightness_Mean;
}

double VBcv1032Pattern_MatchingECC(const char* DirectionM, const char* DirectionR)
{
	//1. CheckFile exist
	//2. File Read
	//3. Calculate
	//4. Comput with affine
	//5. memory release



	//2. File Read
	Mat mSrc_Image = imread(DirectionM, CV_LOAD_IMAGE_GRAYSCALE);
	Mat mRef_Image = imread(DirectionR, CV_LOAD_IMAGE_GRAYSCALE);

	//1. CheckFile exist	
	if (mSrc_Image.empty() || mRef_Image.empty())
	{
		return -1;
	}
	//3. Calculate
	// Define the motion model
	const int warp_mode = MOTION_EUCLIDEAN;

	// Set a 2x3 or 3x3 warp matrix depending on the motion model.
	Mat warp_matrix;

	// Initialize the matrix to identity
	if (warp_mode == MOTION_HOMOGRAPHY)
		warp_matrix = Mat::eye(3, 3, CV_32F);
	else
		warp_matrix = Mat::eye(2, 3, CV_32F);

	// Specify the number of iterations.
	int number_of_iterations = 5000;

	// Specify the threshold of the increment
	// in the correlation coefficient between two iterations
	double termination_eps = 1e-10;

	// Define termination criteria
	TermCriteria criteria(TermCriteria::COUNT + TermCriteria::EPS, number_of_iterations, termination_eps);

	// Run the ECC algorithm. The results are stored in warp_matrix.
	findTransformECC(
		mSrc_Image,
		mRef_Image,
		warp_matrix,
		warp_mode,
		criteria
	);

	//4. Comput with affine
	// Storage for warped image.
	Mat mRef_Image_Matched; // 결과 이미지
	if (warp_mode != MOTION_HOMOGRAPHY)
		// Use warpAffine for Translation, Euclidean and Affine
		warpAffine(mRef_Image, mRef_Image_Matched, warp_matrix, mSrc_Image.size(), INTER_LINEAR + WARP_INVERSE_MAP);

	else
		// Use warpPerspective for Homography
		warpPerspective(mRef_Image, mRef_Image_Matched, warp_matrix, mSrc_Image.size(), INTER_LINEAR + WARP_INVERSE_MAP);


	//4.1 레퍼런스 영역으로 찾은 이미지영역 마스킹
	Mat mMask_Image(mRef_Image.size(), CV_8UC1, Scalar(255)); // 참조 이미지 영역 만큼 마스크 만들기
	Mat mMask_Image_Matched; // 마스크 이미지 위치를 이동한 이미지

	if (warp_mode != MOTION_HOMOGRAPHY)
		// Use warpAffine for Translation, Euclidean and Affine		
		warpAffine(mMask_Image, mMask_Image_Matched, warp_matrix, mSrc_Image.size(), INTER_LINEAR + WARP_INVERSE_MAP);
	else
		// Use warpPerspective for Homography		
		warpPerspective(mMask_Image, mMask_Image_Matched, warp_matrix, mSrc_Image.size(), INTER_LINEAR + WARP_INVERSE_MAP);


	//4.1 원본 이미지에 마스크 영역 씌우기
	Mat mSrc_Masked;
	bitwise_and(mSrc_Image, mMask_Image_Matched, mSrc_Masked);


	//------------------------------------
	//Result 
	//1 mSrc_Masked = 원본에서 찾은 Ref영역 마스크한 이미지 
	//2 mRef_Image_Matched = 레퍼런스 이미지를 원본 영역 위치에 맞춘 이미지 


	//Result 1. 원본과 마스크 이미지 단순 빼기 연산	
	Mat mResult_OrgSub = mSrc_Masked - mRef_Image_Matched;

	//Result 2. 원본과 Ref이미지를 노멀라이즈 처리후 연산
	Mat mSrc_Masked_Normal;
	cv::normalize(mSrc_Masked, mSrc_Masked_Normal, 0, 255, NORM_MINMAX);
	Mat mRef_Image_Matched_Normal;
	cv::normalize(mRef_Image_Matched, mRef_Image_Matched_Normal, 0, 255, NORM_MINMAX);

	Mat mResult_NormalizeSub = mSrc_Masked_Normal - mRef_Image_Matched_Normal;


	//------------------------------------
	//Show Reuslt
	imshow("OrgMask", mSrc_Masked);
	imshow("Ref", mRef_Image_Matched);

	imshow("OrgSub", mResult_OrgSub);
	imshow("NormalizeSub", mResult_NormalizeSub);

	Mat mResultOrgNormal;
	cv::normalize(mResult_OrgSub, mResultOrgNormal, 0, 255, NORM_MINMAX);
	imshow("Normal-OrgSub", mResultOrgNormal);

	Mat mResultNomNormal;
	cv::normalize(mResult_NormalizeSub, mResultNomNormal, 0, 255, NORM_MINMAX);
	imshow("Normal-NormalizeSub", mResultNomNormal);

	waitKey(0);

	//5. memory release

	return 0;

}

double VBcv1032Pattern_Matching(const char* DirectionM, const char* DirectionR, const char* DirectionF)
{
	CvMemStorage* storage = cvCreateMemStorage(0);
	CvSeq* contours = 0;
	CvSeq* result = 0;
	Rect rc_M;
	Rect rc_R;
	Rect rc;
	Rect rc_C;

	Mat Mark_Pattern_Area, Ref_Pattern_Area;
	Mat bin;
	int Move_AxisX = 0;
	int Move_AxisY = 0;
	int Diff_X_Size = 0;
	int Diff_Y_Size = 0;

	bool EndFlag = false;

	vector<float> Cal_Mark_Img;
	vector<float> Cal_Ref_Img;
	vector<float> Total_Cal_Img;
	vector<int> Ref_Pat_X, Ref_Pat_Y, Ref_Pat_Area;
	vector<int> Mark_Pat_X, Mark_Pat_Y, Mark_Pat_Area;
	vector<int> Diff_Pat_X, Diff_Pat_Y;


	float Mark_Mean_Val;
	float Ref_Mean_Val;
	float diff_Mean_Val;

	int Mark_Pattern_Sum = 0, Ref_Pattern_Sum = 0;
	int Mark_Area_Sum = 0, Ref_Area_Sum = 0;
	int MarkPatBrightMean = 0, RefPatBrightMean = 0;
	int Pattern_Count = 0;
	int ccount = 0;
	try
	{
		cv::Mat Mark_Image = cv::imread(DirectionM, CV_8UC1);					   //marked Image
		if (Mark_Image.cols == 0 || Mark_Image.rows == 0) {
			TRACE("----------failed to Load Image - Mark Image is not exist----------\n");
			return 1;
		}

		cv::Mat Ref_Image = cv::imread(DirectionR, CV_8UC1);				   //reference Image
		if (Ref_Image.cols == 0 || Ref_Image.rows == 0) {
			TRACE("----------failed to Load Image - Reference Image not exist----------\n");
			return 1;
		}

		resize(Ref_Image, Ref_Image, Size(Mark_Image.cols, Mark_Image.rows));				   //레퍼런스 이미지를 현재PMI이미지 크기와 같게 변경, 레퍼런스 이미지를
		if (Mark_Image.cols != Ref_Image.cols || Mark_Image.rows != Ref_Image.rows) {		   //변경하는 이유는 마크 크기를 최대한 보존하기 위함임.
			TRACE("----------failed to resize Image - Image size not coinside----------\n");
			return 1;
		}
		Mat Edge_Ref_Image = Ref_Image.clone();
		Mat Edge_Mark_Image = Mark_Image.clone();
		Mat Mark_ToShift_Image = Mark_Image;
		Mat Ref_ToShift_Image = Ref_Image;
		Mat TH_Total_Img = Mat(Mark_Image.rows, Mark_Image.cols, CV_32FC1);		   //pre-processed Image Matrix

		Mark_Mean_Val = (float)mean(Mark_Image)[0];
		Ref_Mean_Val = (float)mean(Ref_Image)[0];
		diff_Mean_Val = abs(Mark_Mean_Val - Ref_Mean_Val);
		// '<-Add by Iron 210802
		//if (Mark_Mean_Val > Ref_Mean_Val) {
		//	//Ref_Image = ((Mark_Mean_Val - Ref_Mean_Val) + Ref_Image) * 0.8;
		//	Ref_Image = (diff_Mean_Val + Ref_Image);
		//}
		//else {
		//	Ref_Image = (Ref_Image - diff_Mean_Val);
		//}
		//Ref_Image = Ref_Image - (255 * (1 - (Mark_Mean_Val / Ref_Mean_Val)));
		// '>-Add by Iron 210802
		TRACE("PMI Mean : %f, REF Mean : %f\n", Mark_Mean_Val, Ref_Mean_Val);
		Light_Resolve(Mark_Mean_Val);

		//---------------------------------------------------------------------------------------------------------------------------

		double Mark_Brightness_Mean = (double)GetThresholdVal(Mark_Image);
		double Ref_Brightness_Mean = (double)GetThresholdVal(Ref_Image);

		threshold(Mark_Image, bin, Mark_Brightness_Mean, 255, THRESH_BINARY_INV);//Marked Image Contouring
		IplImage Mark_bin_Image(bin);
		//imwrite("C:\\PROBERPARAM\\ParaImg\\Proc1.bmp", bin);
		//IplImage* Mark_bin_Image = cvLoadImage("C:\\PROBERPARAM\\ParaImg\\Proc1.bmp", 0);
		cvFindContours(&Mark_bin_Image, storage, &contours, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_NONE, Point(0, 0));

		for (; contours != 0; contours = contours->h_next)
		{
			if (cvContourArea(contours, CV_WHOLE_SEQ) < (Mark_Image.cols * Mark_Image.rows) * 0.007 ||
				cvContourArea(contours, CV_WHOLE_SEQ) > (Mark_Image.cols * Mark_Image.rows) * 0.5 ||
				cvArcLength(contours, CV_WHOLE_SEQ) > (Mark_Image.rows + Mark_Image.cols) / 2)
			{
				continue;
			}
			result = cvApproxPoly(contours, sizeof(CvContour), storage, CV_POLY_APPROX_DP, cvContourPerimeter(contours) * 0.02, 0);
			rc_M = cvBoundingRect(result, 0);
			//Marked_Axisx.push_back(rc_M.x);
			//Marked_Axisy.push_back(rc_M.y);
			Mark_Pat_X.push_back(rc_M.x);
			Mark_Pat_Y.push_back(rc_M.y);
			Mark_Pat_Area.push_back(rc_M.area());
			TRACE("MARK : x : %d y : %d a : %d\n", rc_M.x, rc_M.y, rc_M.area());
			//패턴의 평균 밝기를 구하는 프로세스
			//---------------------------------------------------------------------------------------------------------------------
			Mark_Pattern_Area = Mark_Image(Range(rc_M.y + 1, rc_M.y + rc_M.height - 1), Range(rc_M.x + 1, rc_M.x + rc_M.width - 1));
			for (int y = 0; y < Mark_Pattern_Area.size().height; y++)
			{
				for (int x = 0; x < Mark_Pattern_Area.size().width; x++)
					Mark_Pattern_Sum += Mark_Pattern_Area.at<uchar>(y, x);
			}
			Mark_Area_Sum += (Mark_Pattern_Area.rows * Mark_Pattern_Area.cols);
		}

		if (Mark_Area_Sum != 0) {
			MarkPatBrightMean = Mark_Pattern_Sum / Mark_Area_Sum;
		}

		threshold(Ref_Image, bin, Ref_Brightness_Mean, 255, THRESH_BINARY_INV);//Reference Image Contouring
		IplImage Ref_bin_Image(bin);
		//imwrite("C:\\PROBERPARAM\\ParaImg\\Proc2.bmp", bin);
		//IplImage* Ref_bin_Image = cvLoadImage("C:\\PROBERPARAM\\ParaImg\\Proc2.bmp", 0);
		cvFindContours(&Ref_bin_Image, storage, &contours, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_NONE, Point(0, 0));
		for (; contours != 0; contours = contours->h_next)
		{
			if (cvContourArea(contours, CV_WHOLE_SEQ) < (Mark_Image.cols * Mark_Image.rows) * 0.007 ||
				cvContourArea(contours, CV_WHOLE_SEQ) > (Mark_Image.cols * Mark_Image.rows) * 0.5 ||
				cvArcLength(contours, CV_WHOLE_SEQ) > (Mark_Image.rows + Mark_Image.cols) / 2)
			{
				continue;
			}
			result = cvApproxPoly(contours, sizeof(CvContour), storage, CV_POLY_APPROX_DP, cvContourPerimeter(contours) * 0.02, 0);
			rc_R = cvBoundingRect(result, 0);

			//Ref_Axisx.push_back(rc_R.x);
			//Ref_Axisy.push_back(rc_R.y);
			Ref_Pat_X.push_back(rc_R.x);
			Ref_Pat_Y.push_back(rc_R.y);
			Ref_Pat_Area.push_back(rc_R.area());
			TRACE("REF : x : %d y : %d a : %d\n", rc_R.x, rc_R.y, rc_R.area());

			//패턴의 평균 밝기를 구하는 프로세스
			//-----------------------------------------------------------------------------------------------------------------------
			Ref_Pattern_Area = Ref_Image(Range(rc_R.y + 1, rc_R.y + rc_R.height - 1), Range(rc_R.x + 1, rc_R.x + rc_R.width - 1));
			for (int y = 0; y < Ref_Pattern_Area.size().height; y++)
			{
				for (int x = 0; x < Ref_Pattern_Area.size().width; x++)
					Ref_Pattern_Sum += Ref_Pattern_Area.at<uchar>(y, x);
			}

			Ref_Area_Sum += (Ref_Pattern_Area.rows * Ref_Pattern_Area.cols);
		}
		if (Ref_Area_Sum != 0) {
			RefPatBrightMean = Ref_Pattern_Sum / Ref_Area_Sum;
		}


		TRACE("Mark Pat Avrg : %d ,Ref Pat Avrg : %d\n", MarkPatBrightMean, RefPatBrightMean);
		//--------------------------------------------------------------------------------------------------------------------------
		if (!Ref_Pat_X.empty() && !Ref_Pat_Y.empty() && !Mark_Pat_X.empty() && !Mark_Pat_Y.empty()) {
			if (Ref_Area_Sum > Mark_Area_Sum) {
				Ref_Image = (Ref_Image - (RefPatBrightMean - MarkPatBrightMean));
			}
			else {
				Ref_Image = Ref_Image + (MarkPatBrightMean - RefPatBrightMean);
			}
		}
		else {
			if (Mark_Mean_Val > Ref_Mean_Val) {
				Ref_Image = (diff_Mean_Val + Ref_Image);
			}
			else {
				Ref_Image = (Ref_Image - diff_Mean_Val);
			}
		}

		//pattern image matching process - 1
		//---------------------------------------------------------------------------------------------------------------------------
		int imgcstate = (Mark_Image.channels() == 1) ? CV_8UC1 : CV_8UC3;
		Mat Mark_Channeling(Mark_Image.rows, Mark_Image.cols, imgcstate, Scalar());
		Mat Ref_Channeling(Ref_Image.rows, Ref_Image.cols, imgcstate, Scalar());

		//sort(Marked_Axisx.begin(), Marked_Axisx.end());
		//sort(Marked_Axisy.begin(), Marked_Axisy.end());
		//sort(Ref_Axisx.begin(), Ref_Axisx.end());
		//sort(Ref_Axisy.begin(), Ref_Axisy.end());
		//<-Add by Iron 210805
		Ref_Pat_X.push_back(rc_R.x);
		Ref_Pat_Y.push_back(rc_R.y);
		Ref_Pat_Area.push_back(rc_R.area());
		//float Avrg_Diff_X, Avrg_Diff_Y;

		for (int i = 0; i < Ref_Pat_X.size(); i++) {
			for (int j = 0; j < Mark_Pat_X.size(); j++) {//패턴 좌표가 4픽셀을 초과해서 차이나거나, 넓이가 20%이상 차이나면 같은 패턴이라고 안봄
				if (4 > abs(Ref_Pat_X[i] - Mark_Pat_X[j]) && 4 > abs(Ref_Pat_Y[i] - Mark_Pat_Y[j]) &&
					(Ref_Pat_Area[i] * 1.2) > Mark_Pat_Area[j] && (Mark_Pat_Area[j] * 1.2) > Ref_Pat_Area[i]) {
					Diff_Pat_X.push_back(Mark_Pat_X[j] - Ref_Pat_X[i]); //플러스면 마크패턴이 더 오른쪽으로 시프팅됐다는 것.
					Diff_Pat_Y.push_back(Mark_Pat_Y[j] - Ref_Pat_Y[i]); //플러스면 마크패턴이 더 아래에 내려와 있다는 것.
				}
				else {
				}
			}
		}
		if (Diff_Pat_X.empty() || Diff_Pat_Y.empty()) {
			////패턴찾기 실패, 밝기 변경하고 다시 DLL 수행하기
			////재시도 x회 이상 했는데도 불구하고 실패시엔 Semics-12에서 현재패드 완전실패로 넘기기
			//TRACE("Pattern Detection FAIL\n");
			//vector<float>().swap(Cal_Mark_Img);
			//vector<float>().swap(Cal_Ref_Img);
			//vector<float>().swap(Total_Cal_Img);
			//vector<int>().swap(Ref_Pat_X);
			//vector<int>().swap(Ref_Pat_Y);
			//vector<int>().swap(Ref_Pat_Area);
			//vector<int>().swap(Mark_Pat_X);
			//vector<int>().swap(Mark_Pat_Y);
			//vector<int>().swap(Mark_Pat_Area);
			//vector<int>().swap(Diff_Pat_X);
			//vector<int>().swap(Diff_Pat_Y);
			//if (storage != NULL) {
			//	cvClearMemStorage(storage);
			//}
			//return 2;
		}
		else {
			for (int i = 0; i < Diff_Pat_X.size(); i++) {
				TRACE("X diff : %d ,Y diff : %d\n", Diff_Pat_X[i], Diff_Pat_Y[i]);
			}
		}
		for (int i = 0; i < Diff_Pat_X.size(); i++) {
			Move_AxisX += Diff_Pat_X[i];
			Move_AxisY += Diff_Pat_Y[i];
		}

		Move_AxisX = (double)Move_AxisX / (Diff_Pat_X.size());
		Move_AxisY = (double)Move_AxisY / (Diff_Pat_Y.size());

		if (Diff_Pat_X.empty() || Diff_Pat_Y.empty()) {
			Mark_Channeling = Mark_ToShift_Image;
			Ref_Channeling = Ref_ToShift_Image;
			TRACE("Pattern Detecting FAIL\n");
		}
		else {
			//>-Add by Iron 210805
				//아래코드 : 좌측상단 패턴 정보만 매칭하는 코드
			/*if (Marked_Axisx.empty() || Ref_Axisx.empty()) {
				Mark_Channeling = Mark_ToShift_Image;
				Ref_Channeling = Ref_ToShift_Image;
				TRACE("----------Not shift pattern. average brightness : %f ---------- \n", mean(Mark_Image)[0]);
			}
			else {
				int Sum_Dot = Marked_Axisx[0];
				int DividBy = 1;
				//TRACE("size of Marked_Axisx : %d \n", Marked_Axisx.size());

				for (int i = 1; i < Marked_Axisx.size(); i++) {
					if (abs(Marked_Axisx[i - 1] - Marked_Axisx[i]) < 5)
						break;
					Sum_Dot += Marked_Axisx[i];
					DividBy = i + 1;
				}
				int Mark_Avrg_Axisx = (Sum_Dot / DividBy);
				//TRACE("Average_M_Axisx : %d \n", Mark_Avrg_Axisx);

				Sum_Dot = Marked_Axisy[0];
				for (int i = 1; i < Marked_Axisy.size(); i++) {
					if (abs(Marked_Axisy[i - 1] - Marked_Axisy[i]) < 5)
						break;
					Sum_Dot += Marked_Axisy[i];
					DividBy = i + 1;
				}
				int Mark_Avrg_Axisy = (Sum_Dot / DividBy);
				//TRACE("Average_M_Axisy : %d \n", Mark_Avrg_Axisy);

				Sum_Dot = Ref_Axisx[0];
				for (int i = 1; i < Ref_Axisx.size(); i++) {
					if (abs(Ref_Axisx[i - 1] - Ref_Axisx[i]) < 5)
						break;
					Sum_Dot += Ref_Axisx[i];
					DividBy = i + 1;
				}
				int Ref_Avrg_Axisx = (Sum_Dot / DividBy);
				//TRACE("Average_P_Axisx : %d \n", Ref_Avrg_Axisx);

				Sum_Dot = Ref_Axisy[0];
				for (int i = 1; i < Ref_Axisy.size(); i++) {
					if (abs(Ref_Axisy[i - 1] - Ref_Axisy[i]) < 5)
						break;
					Sum_Dot += Ref_Axisy[i];
					DividBy = i + 1;
				}
				int Ref_Avrg_Axisy = (Sum_Dot / DividBy);
				//TRACE("Average_p_Axisy : %d \n", Ref_Avrg_Axisy);
				*/


				//pattern image matching process - 2
			if (Move_AxisX > -5 && Move_AxisY > -5 && Move_AxisX <= 0 && Move_AxisY <= 0) {
				int distx = abs(Move_AxisX);// -distx만큼 옮겨줘야 마크의 원위치 좌표(중심)를 알수있음
				int disty = abs(Move_AxisY);// -disty만큼 옮겨줘야 마크의 원위치 좌표(중심)를 알수있음
				Shift_Img(Mark_ToShift_Image, Mark_Channeling, distx, disty);
				Ref_Channeling = Ref_ToShift_Image;
				TRACE("mark image shifting success// x = %d y = %d \n", distx, disty);
			}

			else if (Move_AxisX < 5 && Move_AxisY < 5 && Move_AxisX >= 0 && Move_AxisY >= 0) {
				int distx = Move_AxisX;
				int disty = Move_AxisY;
				Shift_Img(Ref_ToShift_Image, Ref_Channeling, distx, disty);
				Mark_Channeling = Mark_ToShift_Image;
				TRACE("reference image shifting success// x = %d y = %d \n", distx, disty);
			}

			else if (Move_AxisX > -5 && Move_AxisY < 5 && Move_AxisX <= 0 && Move_AxisY >= 0) {
				int distx = abs(Move_AxisX);
				int disty = abs(Move_AxisY);
				Shift_Img(Mark_ToShift_Image, Mark_Channeling, distx, 0);// -distx만큼 옮겨줘야 마크의 원위치 좌표(중심)를 알수있음
				Shift_Img(Ref_ToShift_Image, Ref_Channeling, 0, disty);
				TRACE("mark and reference image shifting success// x = %d y = %d \n", Move_AxisX, Move_AxisY);
			}

			else if (Move_AxisX < 5 && Move_AxisY > -5 && Move_AxisX >= 0 && Move_AxisY <= 0) {
				int distx = abs(Move_AxisX);
				int disty = abs(Move_AxisY);
				Shift_Img(Ref_ToShift_Image, Ref_Channeling, distx, 0);
				Shift_Img(Mark_ToShift_Image, Mark_Channeling, 0, disty);// -disty만큼 옮겨줘야 마크의 원위치 좌표(중심)를 알수있음
				TRACE("mark and reference image shifting success// x = %d y = %d \n", Move_AxisX, Move_AxisY);
			}

			else {
				Mark_Channeling = Mark_ToShift_Image;
				Ref_Channeling = Ref_ToShift_Image;
				TRACE("Not shift pattern// x = %d y = %d \n", Move_AxisX, Move_AxisY);
			}
		}

		for (int r = 0; r < Mark_Image.rows; r++) {											   //calcurating pre-processing in Vector (marked Image)
			uchar* ptr = Mark_Channeling.ptr<uchar>(r);
			for (int c = 0; c < Mark_Image.cols; c++) {
				Cal_Mark_Img.push_back((float)(uchar(ptr[c])));
			}
		}

		for (int r = 0; r < Mark_Image.rows; r++) {											   //calcurating pre-processing in Vector (Reference Image)
			uchar* ptr = Ref_Channeling.ptr<uchar>(r);
			for (int c = 0; c < Mark_Image.cols; c++) {
				Cal_Ref_Img.push_back((float)(ptr[c] + 0.01));
			}
		}
		for (int r = 0; r < Cal_Mark_Img.size(); r++) {							   //calcurating pre-processing in Vector (marked and Reference Image)
			Total_Cal_Img.push_back((float)(Cal_Mark_Img[r] / Cal_Ref_Img[r]));
		}

		memcpy(TH_Total_Img.data, Total_Cal_Img.data(), Total_Cal_Img.size() * sizeof(float)); // save Vector data at Matrix
		//---------------------------------------------------------------------------------------------------------------------------------
		//float ss,ss2;
		//float avg = 0, avg2 = 0;
		//for (int i = 0; i < Cal_Mark_Img.size(); i++) {
		//	ss += Cal_Mark_Img[i];
		//}
		//avg = ss / Cal_Mark_Img.size();														//디버그용 코드

		//for (int i = 0; i < Cal_Ref_Img.size(); i++) {
		//	ss2 += Cal_Ref_Img[i];
		//}
		//avg2 = ss2 / Cal_Ref_Img.size();
		//TRACE(" %f, %f, %f\n", avg, avg2, avg/avg2);
		//--------------------------------------------------------------------------------------------------------------------------------
		TH_Total_Img.convertTo(TH_Total_Img, CV_32FC1, 1, 0);
		Mat Result_Img(Mark_Image.rows, Mark_Image.cols, CV_32FC1, Scalar());
		for (int y = 0; y < Mark_Channeling.rows; y++) {
			for (int x = 0; x < Mark_Channeling.cols; x++) {
				if (TH_Total_Img.at<float>(y, x) == 0) {
					uchar data = 255;
					Result_Img.at<float>(y, x) = data;
				}//                                                                           Erasing Black Belt
				else {
					float data = TH_Total_Img.at<float>(y, x);
					Result_Img.at<float>(y, x) = data;
				}
			}
		}
		char Temporary_Image[256]={ 0, };
		char ResultPad_Image[256]={ 0, };
		
		strcpy(Temporary_Image, DirectionF);
		strcat(Temporary_Image, "\\TempImg.bmp");
		strcpy(ResultPad_Image, DirectionF);
		strcat(ResultPad_Image, "\\ResultPadImg.bmp");

		Result_Img = Result_Img * 200;
		imwrite(Temporary_Image, Result_Img);
		Result_Img = imread(Temporary_Image, CV_8UC1);
		//노이즈제거
		//-------------------------------------------------------------------------------------------------------------------------------

		threshold(Result_Img, bin, (float)mean(Result_Img)[0] * 0.9, 255, THRESH_BINARY_INV);
		bin.convertTo(bin, CV_8UC1, 1, 0);
		IplImage copy_mat3(bin);
		//imwrite("C:\\PROBERPARAM\\ParaImg\\Proc3.bmp", bin);
		//IplImage* copy_mat3 = cvLoadImage("C:\\PROBERPARAM\\ParaImg\\Proc3.bmp", 0);

		cvFindContours(&copy_mat3, storage, &contours, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_NONE, Point(0, 0));
		for (; contours != 0; contours = contours->h_next)
		{
			if (cvContourArea(contours, CV_WHOLE_SEQ) > (Result_Img.cols * Result_Img.rows) * 0.01)
			{
				continue;
			}
			result = cvApproxPoly(contours, sizeof(CvContour), storage, CV_POLY_APPROX_DP, cvContourPerimeter(contours) * 0.02, 0);
			rc = cvBoundingRect(result, 0);
		}
		if (rc.area() != 0) {
			Result_Img = Result_Img * 1.27;
		}
		else {
		}

		Mat Gfiltering_M;
		GaussianBlur(Result_Img, Result_Img, Size(5, 5), 0);
		bilateralFilter(Result_Img, Gfiltering_M, -1, 10, 5);
		Result_Img = Gfiltering_M;
		imwrite(Temporary_Image, Result_Img);

		threshold(Result_Img, Result_Img, mean(Result_Img)[0] * 0.9, 255, THRESH_BINARY);
		imwrite(ResultPad_Image, Result_Img);

		EndFlag = true;
		//------------------------------------
		//imshow("img3", Mark_Image);
		//imshow("img2", Ref_Image);
		//imshow("img5", Result_Img);
		//imshow("img6", Ref_Pattern_Area);
		//imshow("img7", Mark_Pattern_Area);
		//------------------------------------
		vector<float>().swap(Cal_Mark_Img);
		vector<float>().swap(Cal_Ref_Img);
		vector<float>().swap(Total_Cal_Img);
		//vector<int>().swap(Marked_Axisx);
		//vector<int>().swap(Ref_Axisy);
		//vector<int>().swap(Marked_Axisy);
		//vector<int>().swap(Ref_Axisx);
		vector<int>().swap(Ref_Pat_X);
		vector<int>().swap(Ref_Pat_Y);
		vector<int>().swap(Ref_Pat_Area);
		vector<int>().swap(Mark_Pat_X);
		vector<int>().swap(Mark_Pat_Y);
		vector<int>().swap(Mark_Pat_Area);
		vector<int>().swap(Diff_Pat_X);
		vector<int>().swap(Diff_Pat_Y);

		Mark_Area_Sum = 0, Ref_Area_Sum = 0;
		Mark_Pattern_Sum = 0, Ref_Pattern_Sum = 0;
		MarkPatBrightMean = 0, RefPatBrightMean = 0;

		//cvReleaseImage(&Mark_bin_Image);
		//cvReleaseImage(&Ref_bin_Image);
		//cvReleaseImage(&copy_mat3);
		//cvClearSeq(contours);
		//cvClearMemStorage(storage);
		//if (Ref_bin_Image) {
		//	cvReleaseImage(&Ref_bin_Image);
		//}
		//if (Mark_bin_Image) {
		//	cvReleaseImage(&Mark_bin_Image);
		//}
		if (storage != NULL) {
			cvClearMemStorage(storage);
			cvReleaseMemStorage(&storage);
			//TRACE("--------------------\n");
		}
		TRACE("----------Pattern Image Process Done.----------\n");
		return 0;
	}
	catch (const std::exception& errorName)
	{
		TRACE(errorName.what());
		if (EndFlag == 0) {
			TRACE("PMI DLL exception error\n");
		}
		return 1;
	}


}

