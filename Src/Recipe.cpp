﻿#include "pch.h"

#include "Recipe.h"
#include "Factory.h"

#include "Filter.h"
using namespace rapidjson;
using namespace cv;

Recipe::~Recipe()
{
	std::vector<IFilter*>::iterator itor = Filters.begin();
	for (std::vector<IFilter*>::iterator itor = Filters.begin(); itor != Filters.end(); ++itor)
	{
		/*Filter* p = dynamic_cast<Filter*>(*itor);
		if (p != nullptr)
			delete p;
		else*/
			delete (*itor);
	}
	Filters.clear();
}

/// <summary>
/// Recipe 파일을 처리하기 위한 최상위 파싱 함수
/// </summary>
/// <param name="recipe">unicode json stirng </param>
void Recipe::Parse(const char* recipe)
{
	GenericDocument<ENCODE> doc;
	doc.Parse<0, ENCODE>((TCHAR*)recipe);
	this->Init(doc);
}

/// <summary>
/// Recipe Json을 분석 하는 함수
/// </summary>
/// <param name="value">rapidjson 으로 변환된 recipe </param>
/// <returns>처리결과 </returns>
int Recipe::Init(GenericValue<ENCODE>& value)
{

	GETJSONSTRING(value, RECIPE_JSON_NAME, strName);
	GETJSONSTRING(value, RECIPE_JSON_COMMENT, strComment);

	if (value.HasMember(RECIPE_JSON_FILTERS))
	{
		GenericValue<ENCODE>& fs = value[RECIPE_JSON_FILTERS];
		
		for (SizeType i = 0; i < fs.Size(); ++i)
		{
			tstring strType;
			GETJSONSTRING(fs[i], RECIPE_JSON_FILTER_TYPE, strType);

			auto obj = CFactory::Instance()->CreateInstance(strType);
			IFilter* filter = dynamic_cast<IFilter*>(obj);
			if (filter != nullptr)
			{
				filter->Init(fs[i]);
				Filters.push_back(filter);
			}
		}

		//Link
		for (SizeType i = 0; i < fs.Size(); ++i)
		{
			tstring strFilterKey;
			GETJSONSTRING(fs[i], RECIPE_JSON_FILTER_KEY, strFilterKey);

			IFilter* filter = nullptr;
			if (fs[i].HasMember(RECIPE_JSON_FILTER_PINS))
			{
				GenericValue<ENCODE>& fp = fs[i][RECIPE_JSON_FILTER_PINS];

				for (SizeType j = 0; j < fp.Size(); ++j)
				{
					tstring strPinKey;
					GETJSONSTRING(fp[j], RECIPE_JSON_FILTER_PIN_KEY, strPinKey);

					if (fp[j].HasMember(RECIPE_JSON_FILTER_PIN_LINK))
					{
						GenericValue<ENCODE>& pLink = fp[j][RECIPE_JSON_FILTER_PIN_LINK];

						tstring linkFilter;
						GETJSONSTRING(pLink, RECIPE_JSON_FILTER_PIN_LINK_FILTER, linkFilter);

						tstring linkPin;
						GETJSONSTRING(pLink, RECIPE_JSON_FILTER_PIN_LINK_PIN, linkPin);

						if (filter == nullptr)
							filter = GetFilterByKey(strFilterKey.c_str());

						IPin* dst = nullptr;
						if (filter != nullptr)
						{
							dst = filter->GetPin(strPinKey.c_str());
						}

						IFilter* srcFilter = GetFilterByKey(linkFilter.c_str());
						IPin* src = nullptr;
						if (srcFilter != nullptr)
						{
							src = srcFilter->GetPin(linkPin.c_str());
						}

						if (src != nullptr)
						{
							dst->SetLink(src);
						}
					}
				}
			}
		}
	}

	return 1;

}




void Recipe::Process(std::vector<Mat*> &src, std::vector<Mat*> &ref, std::vector<Mat*> &ret)
{
	std::vector<IFilter*>::iterator itor = Filters.begin();
	for (std::vector<IFilter*>::iterator itor = Filters.begin(); itor != Filters.end(); ++itor)
	{
		
			if ((*itor)->IsEndPoint())
			{
				std::vector<IPin*> pins = (*itor)->GetOutPin();

				for (auto fitor = pins.begin(); fitor != pins.end(); ++fitor)//for (const auto& pin : pins)
				{
					ret.push_back((*fitor)->GetImage());
				}
			}

	}
	//for (const auto& filter: Filters)
	//{
	//	if (filter->IsEndPoint())
	//	{
	//		std::vector<IPin*> pins = filter->GetOutPin();

	//		for (const auto& pin : pins)
	//		{
	//			ret.push_back(pin->GetImage());
	//		}
	//	}
	//	/*
	//	if (param->GetName() == _T("Kernel"))
	//	{
	//		auto p = dynamic_cast<imgproc::Param<int>*>(param);
	//		kernel = p->GetValue();
	//	}
	//	if (param->GetName() == _T("Sigma"))
	//	{
	//		auto p = dynamic_cast<imgproc::Param<int>*>(param);
	//		sigma = p->GetValue();
	//	}*/
	//}
}


IFilter* Recipe::GetFilterByKey(LPCTSTR key)
{
	std::vector<IFilter*>::iterator itor = Filters.begin();
	for (std::vector<IFilter*>::iterator itor = Filters.begin(); itor != Filters.end(); ++itor)
	{

		if (_tcscmp(key, (*itor)->GetKey()) == 0)
		{
			return (*itor);
		}

	}
	return nullptr;
}