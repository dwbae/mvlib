﻿#include "pch.h"

#include "Pattern_Pad.h"

#include "Recipe.h"
#include "ImageUtil.h"

using namespace cv;

int RecipeExecute(const char* recipe, const char* param, ImageParam* src, ImageParam* ref, ImageParam* dst)
{
	Recipe* recipeExe = new Recipe();
	recipeExe->Parse(recipe);

	std::vector<Mat*> vecSrc;
	for (int i = 0; i < src->argc; ++i)
	{
		ImageData* img = src->argv[i];
		Mat* mat = ImageUtil::ImageDataToMat(*img);
		vecSrc.push_back(mat);
	}


	std::vector<Mat*> vecRef;
	for (int i = 0; i < ref->argc; ++i)
	{
		ImageData* img = ref->argv[i];
		Mat* mat = ImageUtil::ImageDataToMat(*img);
		vecRef.push_back(mat);
	}

	std::vector<Mat*> vecDst;

	recipeExe->Process(vecSrc, vecRef, vecDst);

	
	dst->argc = vecDst.size();;
	dst->argv = new ImageData * [dst->argc];

	for (int i=0; i< dst->argc; ++i)
	{
		
		ImageData* dstImage = ImageUtil::MatToImageData(*vecDst[i]);

		dst->argv[i] = dstImage;
	}

	delete recipeExe;
	//

	//for (int i = 0; i < src->argc; ++i)
	//{	 
	//	 ImageData* srcImage = src->argv[i];

	//	 Mat matSrc = ImageUtil::ImageDataToMat(*srcImage);

	//	 Mat matDst;
	//	 Mat matGray;

	//	 cv::cvtColor(matSrc, matGray, CV_RGB2GRAY);
	//	 cv::threshold(matGray, matDst, 127, 255, cv::THRESH_BINARY);

	//	 ImageData* dstImage = ImageUtil::MatToImageData(matDst);

	//	 dst->argv[i] = dstImage;
	//}
	return 0;
}

int ImageParamRelease(ImageParam* dst)
{
	if (dst == nullptr)
		return 0;
	for (int i = 0; i < dst->argc; ++i)
	{
		delete dst->argv[i];
		dst->argv[i] = nullptr;
	}

	delete[] dst->argv;

	dst->argv = nullptr;
	dst->argc = 0;
}