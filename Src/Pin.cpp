﻿#include "pch.h"
#include "Pin.h"

using namespace cv;

Pin::Pin(IFilter *filter, const TCHAR* name, bool bOutput/* = false*/)
{
	strName = name;
	Link = nullptr;
	Parent = filter;
	Token = 0;
	isOutput = bOutput;
	Image = nullptr;
	strKey = (isOutput ? tstring(_T("OUT_")) : tstring(_T("IN_"))) + tstring(name);
}
Pin::~Pin()
{
	Dispose();
}

bool Pin::IsOutput()
{
	return isOutput;
}

LPCTSTR Pin::GetKey()
{
	return strKey.c_str();
}

Mat* Pin::GetImage()
{
	if (IsOutput())
	{
		Parent->Process();
		return Image;
	}
	else if (Link != nullptr)
	{
		return Link->GetImage();
	}
	return new Mat();
}

void Pin::SetImage(Mat* img)
{
	if (Image != nullptr) {
		delete Image;
		Image = nullptr;
	}
	if(img!=nullptr)
		Image = new Mat(*img);
}

void Pin::SetLink(IPin* link)
{
	Link = link;
}

IPin* Pin::GetLink()
{
	return Link;
}

void Pin::Dispose()
{
	Link = nullptr;
	if (Image != nullptr) {
		delete Image;
		Image = nullptr;
	}
}
