﻿#include "pch.h"
#include "Factory.h"

/// <summary>
/// 싱글턴/  팩토리 클래스의 등록과 생성을 관리
/// </summary>
CFactory* CFactory::Instance()
{
	static CFactory factory;
	return &factory;
}

/// <summary>
/// 등록된 이름으로 해당하는 클래스를 생성하여 반환
/// </summary>
/// <param name="name">등록된 이름</param>
/// <returns>생성된 클래스 포인터 (등록되지 않은 이름의 경우 nullptr)</returns>
IBaseObject* CFactory::CreateInstance(tstring name) 
{
	auto it = mFactoryRegistry.find(name);
	if (it != mFactoryRegistry.end())
	{
		return it->second();
	}
	return nullptr;
}

/// <summary>
/// 클래스 등록
/// </summary>
/// <param name="name">검색 키워드</param>
/// <param name="classFactoryFunction">클래스</param>
void CFactory::RegisterFactoryFuncton(tstring name, std::function<IBaseObject* (void)> classFactoryFunction)
{
	mFactoryRegistry[name] = classFactoryFunction;
}



