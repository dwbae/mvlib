﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using VisionRecipeEditor.ImageFilter;

namespace VisionRecipeEditor.View
{
    /// <summary>
    /// ImageEditView.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ImageEditView : UserControl
    {
        public object FilterItem
        {
            get => GetValue(FilterItemProperty);
            set => SetValue(FilterItemProperty, value);
        }

        public static readonly DependencyProperty FilterItemProperty =
            DependencyProperty.Register(
                "FilterItem",
                typeof(object),
                typeof(ImageEditView), new PropertyMetadata(null, OnFilterItemPropertyChangedCallback));

        private static void OnFilterItemPropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            //UpdateFilter
            if (d != null && d is ImageEditView view)
            {
                if (e.NewValue is ImageFilterBase fNew)
                {
                    fNew.PropertyChanged += view.DependencyPropertyChanged;
                }
                if (e.OldValue is ImageFilterBase fOld)
                {
                    fOld.PropertyChanged -= view.DependencyPropertyChanged;
                }

                view.SourceViewUpdate(e.NewValue);
                view.ResultViewUpdate(e.NewValue);
            }
        }

        private void DependencyPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (sender is ImageFilterBase f)
            {
                if (e.PropertyName == nameof(f.ImageSource))
                {
                    SourceViewUpdate(sender);
                }
                else if (e.PropertyName == nameof(f.ImageDest))

                {
                    ResultViewUpdate(sender);

                }
            }
        }

        /// <summary>
        /// FilterItem 정보 갱신 (화면에 표시)
        /// </summary>
        /// <param name="o"></param>
        private void ResultViewUpdate(object o)
        {
            if (ResultViewArea.Child != null && ResultViewArea.Child is ImageViewer.ViewControl c)

            {
                if (o == null)
                {
                    System.Diagnostics.Debug.WriteLine("get [NULL] filtered image data");

                    c.SrcImage = null;
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("get [new] filtered image data");
                    if (o is ImageFilterBase f)
                    {

                        c.SrcImage = f.ImageDest?.FirstOrDefault();

                    }
                }

            }
        }

        private void SourceViewUpdate(object o)
        {
            if (SourceViewArea.Child != null && SourceViewArea.Child is ImageViewer.ViewControl c)
            {
                if (o == null)
                {
                    System.Diagnostics.Debug.WriteLine("get [NULL] filtered image data");

                    c.SrcImage = null;
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("get [new] filtered image data");
                    if (o is ImageFilterBase f)
                    {
                        c.SrcImage = f.ImageSource?.FirstOrDefault();
                        c.SelectArea = new RectangleF();
                        c.SelectAreaEnable = false;

                        foreach (var param in f.Params)
                        {
                            if (param is ParamRect rect)
                            {
                                c.SelectArea = new RectangleF(
                                rect.X,
                                rect.Y,
                                rect.Width,
                                rect.Height);
                                c.SelectAreaEnable = true;
                                break;
                            }
                        }
                    }
                }
            }
        }

        public ImageEditView()
        {
            InitializeComponent();

            this.Loaded += EditView_Loaded;
        }
        private void EditView_Loaded(object sender, RoutedEventArgs e)
        {
            {
                ImageViewer.ViewControl c = new ImageViewer.ViewControl();
                c.SelectAreaChanged += (sx, ex) =>
                {
                    if (FilterItem is CropFilter f)
                    {
                        if (sx is ImageViewer.ViewControl cx && cx == SourceViewArea.Child)
                        {
                            var area = cx.SelectArea;

                            foreach (var param in f.Params)
                            {
                                if (param is ParamRect rect)
                                {
                                    rect.X = (int)Math.Round(area.X);
                                    rect.Y = (int)Math.Round(area.Y);
                                    rect.Width = (int)Math.Round(area.Width);
                                    rect.Height = (int)Math.Round(area.Height);
                                }
                            }

                        }
                    }
                };
                SourceViewArea.Child = c;
            }
            {
                ImageViewer.ViewControl c = new ImageViewer.ViewControl();
                ResultViewArea.Child = c;
            }
        }
    }

}
