﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using VisionRecipeEditor.BaseModel;
using VisionRecipeEditor.Extension;
using VisionRecipeEditor.ImageFilter;

namespace VisionRecipeEditor.View
{
    /// <summary>
    /// ImageFilterItem.xaml에 대한 상호 작용 논리
    /// </summary>
    /// 

    //Pin은 UIElement로 Link의 속성과 연결. 
    //Graph에서도  Pin의 UIElement와 연결. 
    public partial class ImageFilterItem : UserControl
    {

        #region PIN
        public Link PinIn { get; set; }

        List<Link> _pinin = new List<Link>();
        Dictionary<string, Link> _pinDic = new Dictionary<string, Link>();


        public Link SetLink(ImageFilterPin pinName, ImageFilterItem target, ImageFilterPin targetPinName)
        {
            return SetLink(pinName.Name, target, targetPinName);
        }

        public Link SetLink(string pinName, ImageFilterItem target, ImageFilterPin targetPinName)
        {
            if (this.DataContext is ImageFilterBase f)
            {
                var pin = f.GetPin(pinName);
                if (pin != null && _pinDic.ContainsKey(pinName))
                {
                    if (target != null && target.DataContext is ImageFilterBase tf)
                    {
                        pin.SetLink(tf.GetPin(targetPinName.Name));
                    }
                    else
                        pin.SetLink(null);


                    return _pinDic[pinName];
                }
            }
            return null;
        }

        internal IEnumerable<string> GetLinks()
        {
            foreach (var pair in _pinDic)
            {
                yield return pair.Key;
            }
        }

        #endregion

        /// <summary>
        /// 화면에서 필터 닫기 버튼 선택시 발생
        /// </summary>
        public event EventHandler CloseRequest;

        internal void Close()
        {
            CloseRequest?.Invoke(this, null);
        }

        /// <summary>
        /// Pin간 연결상태가 변경되었을때 발생.  
        /// </summary>
        public event ObjectEventHandler<bool> LinkUpdated;


        public delegate UIElement FindUIFilterPin(string filterKey, string pinKey);

        public FindUIFilterPin RequestPinElement;

        public Type PinType
        {
            get { return typeof(ImageFilterPin); }
        }

        private FrameworkElement GetPinObject()
        {
            return new ImageFilterPin();
        }

        public ImageFilterItem()
        {
            InitializeComponent();
            PinIn = new Link();
            PinIn.Target = this.In;

            DataContextChanged += ImageFilterItem_DataContextChanged;
        }

        private void ImageFilterItem_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            this.PinInPanel.Children.Clear();
            this.PinOutPanel.Children.Clear();
            this._pinDic.Clear();
            if (e.NewValue is ImageFilterBase f)
            {
                foreach (FilterPin pin in f.GetPins().DefaultIfEmpty())
                {
                    if (pin == null)
                        continue;
                    if (pin.PinType == FilterPin.IOType.IN)
                    {
                        var pinIn = GetBindingPin(pin);
                        var link = new Link()
                        {
                            Target = pinIn
                        };

                        link.LinkChanged += (ls, le) =>
                        {
                            if (ls is Link l)
                            {
                                LinkUpdated?.Invoke(
                                    l.DrawLine,
                                    new ObjectEventArgs<bool>(l.Source != null & l.Target != null)
                                    );
                            }
                        };

                        _pinDic[pin.KeyName] = link;
                        this.PinInPanel.Children.Add(pinIn);
                        this.PinInPanel.Children.Add(new Rectangle() { Height = 30 });
                    }
                    else if (pin.PinType == FilterPin.IOType.OUT)
                    {
                        this.PinOutPanel.Children.Add(GetBindingPin(pin));
                        this.PinOutPanel.Children.Add(new Rectangle() { Height = 30 });
                    }
                }

                f.PinPropertyChanged += F_PinPropertyChanged;
            }
        }

        internal UIElement GetLink(string pinName)
        {
            foreach (var pin in this.PinOutPanel.Children)
            {
                if (pin is ImageFilterPin p && string.Compare(pinName, p.Name) == 0)
                {
                    return p;
                }
            }

            foreach (var pin in this.PinInPanel.Children)
            {
                if (pin is ImageFilterPin p && string.Compare(pinName, p.Name) == 0)
                {
                    return p;
                }
            }
            return null;
        }

        private void F_PinPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (sender is FilterPin p)
            {
                if (e.PropertyName == nameof(FilterPin.Link))
                {
                    if (p.Link == null && _pinDic.ContainsKey(p.KeyName))
                    {
                        _pinDic[p.KeyName].Source = null;
                    }

                    if (p.Link != null && RequestPinElement != null)
                    {
                        var element = RequestPinElement(p.Link.Parent.GetKey(), p.Link.KeyName);

                        _pinDic[p.KeyName].Source = element;

                    }
                }
            }
        }

        private UIElement GetBindingPin(FilterPin source)
        {
            var newPin = GetPinObject();
            newPin.Name = source.KeyName;
            Binding pinBind = new Binding("Name")
            {
                Source = source
            };

            Binding pinTypeBind = new Binding("PinType")
            {
                Source = source
            };

            newPin.Height = 30;
            newPin.SetBinding(ImageFilterPin.PinNameProperty, pinBind);
            newPin.SetBinding(ImageFilterPin.PinTypeProperty, pinTypeBind);
            return newPin;
        }

        private void Button_Close_Click(object sender, RoutedEventArgs e)
        {
            CloseRequest?.Invoke(this, null);
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Display1Menu_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Display2Menu_Click(object sender, RoutedEventArgs e)
        {

        }
    }

    /// <summary>
    /// 핀 연결을 표시하는 선을 관리하는 클래스
    /// </summary>
    public class Link
    {
        /// <summary>
        /// Link 대상이 변경되면 발생
        /// </summary>
        public event EventHandler LinkChanged;

        public Line DrawLine { get; set; } = new Line()
        {
            StrokeThickness = 2,
            Stroke = new SolidColorBrush(Color.FromArgb(255, 9, 9, 9)),
            IsHitTestVisible = false
        };

        private UIElement _target;
        public UIElement Target
        {
            get => _target;
            set
            {
                if (_target != value)
                {
                    if (_target != null)
                    {
                        _target.LayoutUpdated -= _target_LayoutUpdated;
                    }

                    _target = value;

                    if (_target != null)
                    {
                        _target.LayoutUpdated += _target_LayoutUpdated;
                    }

                    LinkChanged?.Invoke(this, null);
                }
            }
        }

        private void _target_LayoutUpdated(object sender, EventArgs e)
        {
            LayoutUpdated(Target, LayoutUpdateTarget.TARGET);
        }


        private UIElement _source;
        public UIElement Source
        {
            get => _source;
            set
            {
                if (_source != value)
                {
                    if (_source != null)
                    {
                        _source.LayoutUpdated -= _source_LayoutUpdated;
                    }

                    _source = value;

                    if (_source != null)
                    {
                        _source.LayoutUpdated += _source_LayoutUpdated;
                    }

                    LinkChanged?.Invoke(this, null);
                }
            }
        }

        private void _source_LayoutUpdated(object sender, EventArgs e)
        {
            LayoutUpdated(Source, LayoutUpdateTarget.SOURCE);
        }

        private enum LayoutUpdateTarget
        {
            SOURCE,
            TARGET
        }

        private void LayoutUpdated(object sender, LayoutUpdateTarget target)
        {
            if (sender is UIElement uI)
            {
                if (DrawLine == null)
                {
                    DrawLine = new Line();
                }

                if (sender is FrameworkElement f)
                {
                    DependencyObject dObject = VisualTreeHelper.GetParent(f);
                    while (dObject != null)
                    {
                        if (dObject is Canvas p)
                        {
                            GeneralTransform generalTransform = f.TransformToVisual(p);
                            Rect rectangle = generalTransform.TransformBounds(new Rect(new Point(f.Margin.Left, f.Margin.Top), f.RenderSize));
                            Point pt = new Point();
                            pt.X = rectangle.Left + rectangle.Width / 2;
                            pt.Y = rectangle.Top + rectangle.Height / 2;

                            if (target == LayoutUpdateTarget.SOURCE)
                            {
                                DrawLine.X1 = pt.X;
                                DrawLine.Y1 = pt.Y;
                            }
                            else if (target == LayoutUpdateTarget.TARGET)
                            {
                                DrawLine.X2 = pt.X;
                                DrawLine.Y2 = pt.Y;
                            }
                            break;
                        }
                        dObject = VisualTreeHelper.GetParent(dObject);
                    }
                }
            }
        }


        public void LineRefresh()
        {
            LayoutUpdated(Source, LayoutUpdateTarget.SOURCE);
            LayoutUpdated(Target, LayoutUpdateTarget.TARGET);
        }

    }

}