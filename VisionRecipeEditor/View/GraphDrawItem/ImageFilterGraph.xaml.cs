﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using VisionRecipeEditor.ImageFilter;

namespace VisionRecipeEditor.View
{
    /// <summary>
    /// ImageFilterGraph.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ImageFilterGraph : UserControl
    {


        public object ItemsSource
        {
            get => GetValue(ItemsSourceProperty);
            set => SetValue(ItemsSourceProperty, value);
        }

        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register(
                "ItemsSource",
                typeof(object),
                typeof(ImageFilterGraph),                new PropertyMetadata(null, OnItemListPropertyChangedCallback));


        public object SelectedItem
        {
            get => GetValue(SelectedItemProperty);
            set => SetValue(SelectedItemProperty, value);
        }

        public static readonly DependencyProperty SelectedItemProperty =
           DependencyProperty.Register(
               "SelectedItem",
               typeof(object),
               typeof(ImageFilterGraph));


        private static void OnItemListPropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d != null && d is ImageFilterGraph graph)
            {
                if(e.OldValue is ObservableCollection<ImageFilterBase> oldCollection)
                {
                    oldCollection.CollectionChanged +=graph.Collection_CollectionChanged;
                }

                if(e.NewValue is ObservableCollection<ImageFilterBase> newCollection)
                {
                    newCollection.CollectionChanged += graph.Collection_CollectionChanged;
                }
                graph.FilterListUpdate(e.NewValue);
            }
        }

        private  void Collection_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (sender is ObservableCollection<ImageFilterBase> collection)
            {
                if(e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
                {
                    foreach (var item in e.NewItems)
                    {
                        if (item is ImageFilterBase f)
                        {
                            AddFilterUI(f);
                        }
                    }
                }
                else if(e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove)
                {
                    List<ImageFilterItem> removeItem = new List<ImageFilterItem>();
                    foreach (var s in canvas.Children)
                    {
                        if (s is ImageFilterItem f)
                        {
                            if (e.OldItems.Contains(f.DataContext))
                                removeItem.Add(f);
                                   
                        }
                    }

                    foreach(var remove in removeItem)
                    {
                        remove.Close();
                    }
                }
            }
        }

        private void FilterListUpdate(object newValue)
        {
            if (newValue is ObservableCollection<ImageFilterBase> collection)
            {
                List<ImageFilterItem> removeItem = new List<ImageFilterItem>();
                foreach (var s in canvas.Children)
                {
                    if (s is ImageFilterItem f)
                    {
                        removeItem.Add(f);

                    }
                }

                foreach (var remove in removeItem)
                {
                    remove.Close();
                }

                foreach (var item in collection)
                {
                    if (item is ImageFilterBase f)
                    {
                        AddFilterUI(f);
                    }
                }

                foreach (var item in collection)
                {
                    if (item is ImageFilterBase f)
                    {
                        foreach(var pin in f.GetPins())
                        {
                            pin.RaiseLinkUpdate();
                        }
                    }
                }
            }

            
        }



        bool drag = false;
        object dragObject;
        Point startPoint;

        private Random rnd = new Random(DateTime.Now.Millisecond);

        public ImageFilterGraph()
        {
            InitializeComponent();
        }

        // this creates and adds rectangles dynamically
        private void addRectangleButton_Click(object sender, RoutedEventArgs e)
        {
            Dictionary<int, List<ImageFilterItem>> depthLevelItemCountDic = new Dictionary<int, List<ImageFilterItem>>();

            Dictionary<object, float> layerLevelMin = new Dictionary<object, float>();
            Dictionary<object, float> layerLevelMax = new Dictionary<object, float>();

            List<object> layerList = new List<object>();

            foreach ( var element in canvas .Children)
            {   
                if(element is ImageFilterItem f)
                {
                    if(f.DataContext is IFilterBase filter)
                    {

                        var layer = filter.GetLayer();

                        if (!layerList.Contains(layer))
                            layerList.Add(layer);
                        if (layerLevelMin.ContainsKey(layer))
                            layerLevelMin[layer] = Math.Min(layerLevelMin[layer], filter.GetLevel());
                        else
                            layerLevelMin[layer] =  filter.GetLevel();

                        if (layerLevelMax.ContainsKey(layer))
                            layerLevelMax[layer] = Math.Max(layerLevelMax[layer], filter.GetLevel());
                        else
                            layerLevelMax[layer] = filter.GetLevel();


                    }
                }
            }



            foreach (var element in canvas.Children)
            {
                if (element is ImageFilterItem f)
                {
                    if (f.DataContext is IFilterBase filter)
                    {
                        var layer = filter.GetLayer();

                        float offset = 0;

                        for(int i=0; i<layerList.Count; ++i)
                        {
                            if(layer!= layerList[i])
                            {
                                offset += layerLevelMax[layerList[i]] - layerLevelMin[layerList[i]];
                                offset += 1;
                            }
                            else
                            {
                                break;
                            }
                        }


                        int depth = filter.GetDepth();
                        float level = filter.GetLevel() - layerLevelMin[layer];
                        level += offset;
                        //if (!depthLevelItemCountDic.ContainsKey(depth))
                        //{
                        //    depthLevelItemCountDic[depth] = new List<ImageFilterItem>();
                        //}
                        //depthLevelItemCountDic[depth].Add(f);

                        Canvas.SetLeft(f, depth * (f.ActualWidth + 10));
                        Canvas.SetTop(f, level * (f.ActualHeight + 10));
                    }
                }
            }


            return;

            try
            {
                var sourcefilter = new SourceFilter("Source");
                AddFilterUI(sourcefilter);
                var blur = new GaussianBlurFilter();

                AddFilterUI(blur);
                var binfilter = new ThresholdFilter();
                //binfilter.GetPin("IN_In").Link = blur.GetPin("OUT_Out");
                AddFilterUI(binfilter);
                var median = new MedianFilter();
                AddFilterUI(median);

                var transformecc = new TransformECCFilter();
                var substract = new SubStractFilter();
                AddFilterUI(transformecc);
                AddFilterUI(substract);

                blur.GetPin("IN_In").SetLink(sourcefilter.GetPin("OUT_Out"));

                //순환참조 테스트
                var ret1 = binfilter.GetPin("IN_In").SetLink(median.GetPin("OUT_Out"));
                var ret2 = median.GetPin("IN_In").SetLink(binfilter.GetPin("OUT_Out"));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void AddFilterUI(ImageFilterBase filter)
        {  // create new
            var newFilter = new ImageFilterItem();// new Rectangle();
            newFilter.CloseRequest += (s, e) =>
            {
                if (s is ImageFilterItem f)
                {
                    //foreach (var link in f.GetLinks())
                    //{
                    //    var drawItem = f.SetLink(link, null, null);
                    //}

                    if (f.DataContext is ImageFilterBase fb)
                    {
                        if (ItemsSource is ObservableCollection<ImageFilterBase> collection)
                        {
                            var idx = collection.IndexOf(fb);
                            if (idx >= 0)
                            {
                                collection.RemoveAt(idx);//collection에서 제거 처리
                                return;
                            }
                        }
                        fb.Dispose();
                    }
                    //collection에서 제거 처리 되지 않은 경우 정리
                    f.DataContext = null;

                    if (canvas.Children.Contains(f))
                        canvas.Children.Remove(f);
                }
            };

            newFilter.LinkUpdated += (s, e) =>
            {
                if (s is UIElement uI)
                {
                    if (e.Value)
                    {
                        if (!canvas.Children.Contains(uI))
                        {
                            Canvas.SetZIndex(uI, 1);
                            canvas.Children.Add(uI);
                        }
                    }
                    else
                    {
                        if (canvas.Children.Contains(uI))
                        {
                            canvas.Children.Remove(uI);
                        }
                    }
                }
            };

            newFilter.RequestPinElement = FindUIFilterPin;



            newFilter.DataContext = filter;
            // assign properties
            Color randomColor = Color.FromRgb(
                (byte)rnd.Next(256),
                (byte)rnd.Next(256),
                (byte)rnd.Next(256));

            newFilter.Background = new SolidColorBrush(randomColor);

            Canvas.SetLeft(newFilter, 0);
            Canvas.SetTop(newFilter, 0);
            // add it to canvas

            Canvas.SetZIndex(newFilter, 0);
            System.Diagnostics.Debug.WriteLine(Canvas.GetZIndex(newFilter));
            canvas.Children.Add(newFilter);
        }

        private UIElement FindUIFilterPin(string filterKey, string pinKey)
        {
            foreach (var element in canvas.Children)
            {
                if (element is ImageFilterItem f && f.DataContext is ImageFilterBase fb)
                {
                    if (string.Compare(fb.GetKey(), filterKey) == 0)
                    {
                        return f.GetLink(pinKey);
                    }
                }
            }
            return null;
        }

        private void canvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (sender is Canvas c)
            {
                var pt = e.GetPosition(c);
                mouseDownSender = sender;
                VisualTreeHelper.HitTest(c, OnFilter, OnMouseDownHitTestResult, new PointHitTestParameters(pt));
            }
        }

        private HitTestFilterBehavior OnFilter(DependencyObject potentialHitTestTarget)
        {
            if (potentialHitTestTarget is UIElement uI && !uI.IsHitTestVisible)
            {
                return HitTestFilterBehavior.ContinueSkipSelf;
            }
            return HitTestFilterBehavior.Continue;
        }

        private HitTestResultBehavior OnMouseDownHitTestResult(HitTestResult result)
        {

            if (mouseDownSender is Canvas c)
            {

                //필터 항목이면 드래그.
                //핀 이면 라인 드래그

                DependencyObject element = result.VisualHit;
                ImageFilterItem filterElement = null;
                ImageFilterPin pinElement = null;

                if (element is FrameworkElement hit)
                {
                    while (element != null)
                    {
                        if (element is UIElement uElement)
                        {
                            if (element is ImageFilterItem f && c.Children.Contains(uElement))
                            {
                                filterElement = f;
                                break;
                            }
                            if (element is ImageFilterPin p)
                            {
                                pinElement = p;
                                //break;
                            }
                        }
                        element = VisualTreeHelper.GetParent(element);
                    }

                    if (filterElement != null)
                    {
                        if (pinElement == null)
                        {
                            dragObject = filterElement;
                            SelectedItem = filterElement.DataContext;
                        }
                        else if (
                            (IsInputPin(pinElement) || IsOutputPin(pinElement))
                            ) // 핀항목
                        {
                            LinkLine nLink = new LinkLine();

                            if (IsInputPin(pinElement))
                            {
                                nLink.End = new Tuple<ImageFilterItem, ImageFilterPin>(filterElement, pinElement);
                            }
                            else if (IsOutputPin(pinElement))
                            {
                                nLink.Begin = new Tuple<ImageFilterItem, ImageFilterPin>(filterElement, pinElement);
                            }
                            else
                            {
                                return HitTestResultBehavior.Stop;
                            }

                            //핀 드래그 시작지점 조정
                            FrameworkElement frameworkElement = pinElement as FrameworkElement; //childElement should be replaced with the Child Element Name
                            GeneralTransform generalTransform = frameworkElement.TransformToVisual(c); //parentElement should be replaced with the Parent Element Name
                            Rect rectangle = generalTransform.TransformBounds(new Rect(new Point(frameworkElement.Margin.Left, frameworkElement.Margin.Top), frameworkElement.RenderSize));
                            Point pt = new Point();
                            pt.X = rectangle.Left + rectangle.Width / 2;
                            pt.Y = rectangle.Top + rectangle.Height / 2;

                            nLink.Link.X1 = nLink.Link.X2 = pt.X;
                            nLink.Link.Y1 = nLink.Link.Y2 = pt.Y;

                            dragObject = nLink;

                            Canvas.SetZIndex(nLink.Link, 1);

                            c.Children.Add(nLink.Link);
                        }
                        else// 필터 항목
                        {
                            dragObject = filterElement;
                            SelectedItem = filterElement.DataContext;
                        }

                        BringToFront(c, filterElement);

                        Point newPoint = Mouse.GetPosition(canvas);
                        startPoint = Mouse.GetPosition(canvas);
                        startPoint = newPoint;
                        drag = true;
                    }
                }

            }


            return HitTestResultBehavior.Stop;

        }


        object mouseDownSender = null;

        private void canvas_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (drag && dragObject != null && sender is Canvas c)
            {

                if (dragObject is LinkLine nLink)
                {
                    if (c.Children.Contains(nLink.Link))
                    {
                        c.Children.Remove(nLink.Link);
                    }

                    if (dragTargetObject is Tuple<ImageFilterItem, ImageFilterPin> A)
                    {
                        if (nLink.Begin is Tuple<ImageFilterItem, ImageFilterPin> B && A.Item1 != B.Item1)
                        {
                            var drawItem = A.Item1.SetLink(A.Item2, B.Item1, B.Item2);// .PinIn.Source = B.Out;
                        }
                        else if (nLink.End is Tuple<ImageFilterItem, ImageFilterPin> C && A.Item1 != C.Item1)
                        {
                            var drawItem = C.Item1.SetLink(C.Item2, A.Item1, A.Item2);// .PinIn.Source = B.Out;
                        }
                    }
                    else
                    {
                        if (nLink.End is Tuple<ImageFilterItem, ImageFilterPin> D)
                        {
                            var drawItem = D.Item1.SetLink(D.Item2, null, null);// .PinIn.Source = B.Out;

                        }
                    }
                }
            }

            dragObject = null;
            dragTargetObject = null;
            drag = false;

        }

        private void canvas_MouseMove(object sender, MouseEventArgs e)
        {

            if (sender is Canvas c)
            {
                var pt = e.GetPosition(c);
                mouseMoveSender = sender;
                mouseMoveEventArg = e;
                VisualTreeHelper.HitTest(c, OnFilter, OnMouseMoveHitTestResult, new PointHitTestParameters(pt));
            }
        }

        private bool IsInputPin(FrameworkElement element)
        {
            if (element == null)
                return false;
            return element.Name.Contains("IN");
        }

        private bool IsOutputPin(FrameworkElement element)
        {
            if (element == null)
                return false;
            return element.Name.Contains("OUT");
        }

        object mouseMoveSender;
        MouseEventArgs mouseMoveEventArg;
        object dragTargetObject;
        private HitTestResultBehavior OnMouseMoveHitTestResult(HitTestResult result)
        {
            if (mouseMoveSender is Canvas c &&
                mouseMoveEventArg != null &&
                dragObject is LinkLine nLine)
            {
                DependencyObject element = result.VisualHit;
                ImageFilterItem filterElement = null;
                ImageFilterPin pinElement = null;
                if (element is FrameworkElement hit)
                {
                    while (element != null)
                    {
                        if (element is UIElement uElement)
                        {
                            if (element is ImageFilterItem f && c.Children.Contains(uElement))
                            {
                                filterElement = f;
                                break;
                            }
                            if (element is ImageFilterPin p)
                            {
                                pinElement = p;
                                //break;
                            }
                        }
                        element = VisualTreeHelper.GetParent(element);
                    }

                    if (filterElement != null)
                    {
                        if (pinElement != null &&

                            (IsInputPin(pinElement) || IsOutputPin(pinElement))) // 핀항목
                        {
                            if ((nLine.End == null && IsInputPin(pinElement)) ||
                                  (nLine.Begin == null && IsOutputPin(pinElement)))
                            {

                                //라인 포커스 위치 조정
                                FrameworkElement frameworkElement = pinElement as FrameworkElement; //childElement should be replaced with the Child Element Name
                                GeneralTransform generalTransform = frameworkElement.TransformToVisual(c); //parentElement should be replaced with the Parent Element Name
                                Rect rectangle = generalTransform.TransformBounds(new Rect(new Point(frameworkElement.Margin.Left, frameworkElement.Margin.Top), frameworkElement.RenderSize));
                                Point pt = new Point();
                                pt.X = rectangle.Left + rectangle.Width / 2;
                                pt.Y = rectangle.Top + rectangle.Height / 2;
                                if (nLine.End == null)
                                {
                                    nLine.Link.X2 = pt.X;
                                    nLine.Link.Y2 = pt.Y;
                                }
                                else if (nLine.Begin == null)
                                {
                                    nLine.Link.X1 = pt.X;
                                    nLine.Link.Y1 = pt.Y;
                                }
                                dragTargetObject = new Tuple<ImageFilterItem, ImageFilterPin>(filterElement, pinElement);
                                return HitTestResultBehavior.Stop;
                            }
                        }
                    }
                }
                {
                    var pt = mouseMoveEventArg.GetPosition(c);

                    if (nLine.End == null)
                    {
                        nLine.Link.X2 = pt.X;
                        nLine.Link.Y2 = pt.Y;
                    }
                    else if (nLine.Begin == null)
                    {
                        nLine.Link.X1 = pt.X;
                        nLine.Link.Y1 = pt.Y;
                    }

                }
                return HitTestResultBehavior.Stop;

            }

            if (dragObject is UIElement uI)
            {
                Point newPoint = Mouse.GetPosition(canvas);
                double left = Canvas.GetLeft(uI);
                double top = Canvas.GetTop(uI);
                Canvas.SetLeft(uI, left + (newPoint.X - startPoint.X));
                Canvas.SetTop(uI, top + (newPoint.Y - startPoint.Y));

                startPoint = newPoint;
            }
            return HitTestResultBehavior.Stop;
        }


        static public void BringToFront(Canvas pParent, UIElement pToMove)
        {
            try
            {
                pParent.Children.Remove(pToMove);
                pParent.Children.Add(pToMove);
            }
            catch 
            {
            }
        }
    }
}
