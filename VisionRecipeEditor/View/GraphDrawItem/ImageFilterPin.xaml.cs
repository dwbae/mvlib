﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VisionRecipeEditor.View
{
    /// <summary>
    /// ImageFilterPin.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ImageFilterPin : UserControl
    {
        public object PinName
        {
            get => GetValue(PinNameProperty);
            set => SetValue(PinNameProperty, value);
        }

        public static readonly DependencyProperty PinNameProperty =
            DependencyProperty.Register(
                "PinName",
                typeof(string),
                typeof(ImageFilterPin), null);// new PropertyMetadata(null, null));


        public object PinType
        {
            get => GetValue(PinNameProperty);
            set => SetValue(PinNameProperty, value);
        }

        public static readonly DependencyProperty PinTypeProperty =
            DependencyProperty.Register(
                "PinType",
                typeof(ImageFilter.FilterPin.IOType),
                typeof(ImageFilterPin), null);// new PropertyMetadata(null, null));

        public ImageFilterPin()
        {
            InitializeComponent();
        }
    }
}
