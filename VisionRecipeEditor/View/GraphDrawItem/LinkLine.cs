﻿using System.Windows.Media;
using System.Windows.Shapes;

namespace VisionRecipeEditor.View
{
    public class LinkLine
    {
        public object Begin { get; set; }
        public object End { get; set; }
        public Line Link { get; set; } = new Line()
        {
            StrokeThickness = 1,
            Stroke = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0)),
            IsHitTestVisible = false
        };
    }
}
