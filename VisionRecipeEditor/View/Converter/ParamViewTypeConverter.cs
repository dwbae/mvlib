﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Markup;
using System.Windows.Media;
using VisionRecipeEditor.ImageFilter;

namespace VisionRecipeEditor.View.Converter
{
    public class ParamViewTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is ParamRect) { return "Rect"; }
            else if (value is ParamInt) { return "Int"; }
            else if (value is ParamString) { return "String"; }
            else if (value is ParamFloat) { return "Float"; }
            else if (value is ParamBool) { return "Bool"; }
            else if (value is ParamArray) { return "Array"; }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ValueOffsetConverter : MarkupExtension, IValueConverter
    {
        private static ValueOffsetConverter _instance;

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return System.Convert.ToDouble(value) + System.Convert.ToDouble(parameter);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return _instance ?? (_instance = new ValueOffsetConverter());
        }
    }

    public class PinTypeNameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is ImageFilter.FilterPin.IOType t)
            {
                switch (t)
                {
                    case FilterPin.IOType.IN: return "IN";
                    case FilterPin.IOType.OUT: return "OUT";
                    default: return "NONE";
                }
            }
            return "ERR";
        }


        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class PinColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is ImageFilter.FilterPin.IOType t)
            {
                switch (t)
                {
                    case FilterPin.IOType.IN: return new SolidColorBrush( Color.FromArgb(255, 255, 0, 0));
                    case FilterPin.IOType.OUT: return new SolidColorBrush(Color.FromArgb(255, 0, 255, 0));
                    default: return new SolidColorBrush(Color.FromArgb(255, 50, 50, 50));
                }
            }
            return new SolidColorBrush(Color.FromArgb(255, 50, 50, 50));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
