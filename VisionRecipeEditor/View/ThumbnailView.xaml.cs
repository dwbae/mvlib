﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using VisionRecipeEditor.ImageFilter;
using VisionRecipeEditor.ViewModel;

namespace VisionRecipeEditor.View
{
    /// <summary>
    /// ThumbnailView.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ThumbnailView : UserControl
    {
        public object DisplayImage
        {
            get => GetValue(ImageProperty);
            set => SetValue(ImageProperty, value);
        }

        public static readonly DependencyProperty ImageProperty =
            DependencyProperty.Register(
                "DisplayImage",
                typeof(object),
                typeof(ThumbnailView), new PropertyMetadata(null, OnImagePropertyChangedCallback));

        private static void OnImagePropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            //UpdateFilter
            if (d != null && d is ThumbnailView view)
            {
                view.ResultViewUpdate(e.NewValue);
            }
        }

        /// <summary>
        /// FilterItem 정보 갱신 (화면에 표시)
        /// </summary>
        /// <param name="o"></param>
        private void ResultViewUpdate(object o)
        {

            if (o == null)
            {
                System.Diagnostics.Debug.WriteLine("get [NULL] filtered image data");

                vm.CombinedImage = null;
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("get [new] filtered image data");
                if (o is Bitmap[] bmps)
                {
                    vm.CombinedImage = ConvertBitmapToBitmapImage(bmps);
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        public ThumbnailViewModel vm;//= new EditViewViewModel();

        private BitmapImage ConvertBitmapToBitmapImage(Bitmap[] bitmaps)
        {
            try
            {
                if (bitmaps == null)
                    return null;

                Bitmap bmp = bitmaps.FirstOrDefault();
                if (bmp == null)
                    return null;

                using (var memory = new MemoryStream())
                {
                    bmp.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                    memory.Position = 0;

                    var bitmapImage = new BitmapImage();
                    bitmapImage.BeginInit();
                    bitmapImage.StreamSource = memory;
                    bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                    bitmapImage.EndInit();
                    bitmapImage.Freeze();

                    return bitmapImage;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public ThumbnailView()
        {
            InitializeComponent();
            vm = new ThumbnailViewModel(this.Dispatcher);
            this.Root.DataContext = vm;
        }
    }
}
