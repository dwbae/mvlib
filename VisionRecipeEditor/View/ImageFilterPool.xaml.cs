﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using VisionRecipeEditor.BaseModel;
using VisionRecipeEditor.ImageFilter;

namespace VisionRecipeEditor.View
{
    /// <summary>
    /// ImageFilterList.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ImageFilterPool : UserControl
    {

        public object ItemsSource
        {
            get => GetValue(ItemsSourceProperty);
            set => SetValue(ItemsSourceProperty, value);
        }

        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register(
                "ItemsSource",
                typeof(object),
                typeof(ImageFilterPool));//,                new PropertyMetadata(null, OnItemListPropertyChangedCallback));


        public object SelectedItem
        {
            get => GetValue(SelectedItemProperty);
            set => SetValue(SelectedItemProperty, value);
        }

        public static readonly DependencyProperty SelectedItemProperty =
           DependencyProperty.Register(
               "SelectedItem",
               typeof(object),
               typeof(ImageFilterPool));


        public string Title
        {
            get => (string)GetValue(TitleProperty);
            set => SetValue(TitleProperty, value);
        }

        public static readonly DependencyProperty TitleProperty =
            DependencyProperty.Register(
                "Title",
                typeof(string),
                typeof(ImageFilterPool));


        private static void OnItemListPropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {

            if (d != null && d is ImageFilterPool elist)
            {
                elist.ItemListPropertyUpdate(d, e);
            }
        }


        private void ItemListPropertyUpdate(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d != null && d is ImageFilterPool)
            {
                var old = (ObservableCollection<string>)e.OldValue;
                if (old != null)
                {
                    old.CollectionChanged -= ItemList_CollectionChanged;
                }
            }
        }



        private void ItemList_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {

        }

        public ICommand MoveUp { get; set; }
        public ICommand MoveDown { get; set; }

        public ICommand Remove { get; set; }



        public ObservableCollection<string> TestBindingItem = new ObservableCollection<string>() { "Tset1", "Test2" };



        public ImageFilterPool()
        {
            InitializeComponent();
            MoveUp = new RelayCommand((param) => {
                System.Diagnostics.Debug.WriteLine("MoveUp :" + param);
                if (param is ImageFilterBase f)
                {
                    System.Diagnostics.Debug.WriteLine("MoveUp :" + f.Name);
                }
            }, (param) => { return true; });

            MoveDown = new RelayCommand((param) => {
                System.Diagnostics.Debug.WriteLine("MoveDown :" + param);
                if (param is ImageFilterBase f)
                {
                    System.Diagnostics.Debug.WriteLine("MoveUp :" + f.Name);
                }
            }, (param) => { return true; });
        }

        private void ListView_PreviewMouseMove(object sender, MouseEventArgs e)
        {

        }

        private void ListView_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void ListView_Drop(object sender, DragEventArgs e)
        {

        }
    }
}
