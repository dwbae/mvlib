﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using VisionRecipeEditor.BaseModel;

namespace VisionRecipeEditor.ViewModel
{
    public class ThumbnailViewModel : ViewModelBase
    {
        Dispatcher _dispatcher;

        public ThumbnailViewModel(Dispatcher dispatcher)
        {
            this._dispatcher = dispatcher;

        }
        private int ImageID { get; set; }
        private int ImageWidth { get; set; }
        private BitmapImage _combinedImage;
        public BitmapImage CombinedImage
        {
            get { return _combinedImage; }
            set
            {
                _combinedImage = value;
                OnPropertyChanged();
            }
        }
    }
}
