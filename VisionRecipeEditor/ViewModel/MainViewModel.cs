﻿using System.Collections;
using System.Collections.ObjectModel;
using System.Windows.Input;
using VisionRecipeEditor.BaseModel;
using VisionRecipeEditor.Control.DragAndDrop;
using VisionRecipeEditor.ImageFilter;

namespace VisionRecipeEditor.ViewModel
{
    public class MainViewModel : ViewModelBase, IDropTarget
    {

        private string _filePath = @"Empty";
        public string FilePath
        {
            get => _filePath;
            set => SetProperty(ref _filePath, value);
        }

        #region filter list view

        private ObservableCollection<ImageFilterBase> _filterItems;


        public ObservableCollection<ImageFilterBase> FilterItems
        {
            get => _filterItems;
            set
            {
                SetProperty(ref _filterItems, value);
                //_filterItems.CollectionChanged += chain.CollectionChanged;
            }
        }

        private ImageFilterBase _selectedFilterItem;

        public ImageFilterBase SelectedFilterItem
        {
            get => _selectedFilterItem;
            set
            {
                SetProperty(ref _selectedFilterItem, value);
                OnPropertyChanged();
            }
        }

        private ObservableCollection<ImageFilterBase> _filterpool;
        public ObservableCollection<ImageFilterBase> FilterPool
        {
            get => _filterpool;
            set
            {
                SetProperty(ref _filterpool, value);
            }
        }



        private string _filterTitle;
        public string FilterTitle
        {
            get => _filterTitle;
            set => SetProperty(ref _filterTitle, value);
        }

        #endregion


        public ICommand CmdFileOpen { get; set; }

        public ICommand CmdFileSave { get; set; }
        

        ImageFilterChainControl chain = new ImageFilterChainControl();

        public MainViewModel()
        {
            CommandInit();
        }


        private void CommandInit()
        {

            CmdFileOpen = new RelayCommand(
                (param) =>
                {

                    Microsoft.Win32.OpenFileDialog openFileDialog = new Microsoft.Win32.OpenFileDialog();
                    if (openFileDialog.ShowDialog() == true)
                    {
                        string jsonstring = System.IO.File.ReadAllText(openFileDialog.FileName);
                        FilterItems = ImageFilterChainControl.LoadFromJson(jsonstring);
                    }
                },
                (param) => { return true; }
                );

            CmdFileSave = new RelayCommand(
               (param) =>
               {

                   Microsoft.Win32.SaveFileDialog saveFileDialog = new Microsoft.Win32.SaveFileDialog();
                   saveFileDialog.DefaultExt = ".json";
                   if (saveFileDialog.ShowDialog() == true)
                   {
                       var jsonstring = ImageFilterChainControl.CovertToJson(FilterItems);
                       var filePath = saveFileDialog.FileName;
                       if (string.IsNullOrWhiteSpace(System.IO.Path.GetExtension(filePath)))
                           filePath = $"{filePath.Trim()}.json";

                       System.IO.File.WriteAllText(filePath, jsonstring);

                       var path = System.IO.Path.GetDirectoryName(saveFileDialog.FileName);
                       foreach(var EndpointRecipe  in ImageFilterChainControl.CovertToJsonEachEndPoint(FilterItems))
                       {
                           System.IO.File.WriteAllText(
                               System.IO.Path.Combine(path, $"{EndpointRecipe.FilterName }.json"),
                               EndpointRecipe.Recipe
                               );
                       }


                   }
               },
               (param) => { return true; }
               );


            FilterItems = new ObservableCollection<ImageFilterBase>();
            //var root = System.AppDomain.CurrentDomain.BaseDirectory;
            //var imagepath = "";
            //{
            //    var path = System.IO.Path.Combine(root, @"..\..\..\..\Sample\target.jpg");

            //    if (System.IO.File.Exists(path))
            //    {
            //        imagepath = path;
            //    }
            //}
            //{
            //    var path = System.IO.Path.Combine(root, @"..\..\..\Sample\target.jpg");

            //    if (System.IO.File.Exists(path))
            //    {
            //        imagepath = path;
            //    }
            //}

            //var source = new SourceFilter("Source")
            //{
            //    Path = imagepath
            //};
            //filterItems.Add(source);
            //var crop = new CropFilter("Corp");
            //filterItems.Add(crop);
            //filterItems.Add(new GaussianBlurFilter("Gaussian"));
            //filterItems.Add(new ThresholdFilter("Binary"));


            //crop.GetPin("IN_In")?.SetLink(source.GetPin("OUT_Out"));
            


            FilterPool = new ObservableCollection<ImageFilterBase>();

            FilterPool.Add(new SourceFilter());
            FilterPool.Add(new CropFilter());
            FilterPool.Add(new BlurFilter());
            FilterPool.Add(new MedianFilter());
            FilterPool.Add(new GaussianBlurFilter());
            FilterPool.Add(new ThresholdFilter());
            FilterPool.Add(new TransformECCFilter());
            FilterPool.Add(new SubStractFilter());
            FilterPool.Add(new NormalizeFilter());
            FilterPool.Add(new PHOTFilter());
            FilterPool.Add(new EnhancedImageFilter());
            FilterPool.Add(new EnhancedOtsuThresholdFilter());


            //var jsonstring= ImageFilterChainControl.CovertToJson(filterItems);
            //System.IO.File.WriteAllText(System.IO.Path.Combine(root, "Recipe.json"), jsonstring);
            //jsonstring = System.IO.File.ReadAllText(System.IO.Path.Combine(root, "Recipe.json"));
            //FilterItems = ImageFilterChainControl.LoadFromJson(jsonstring);
        }


        void IDropTarget.DragOver(DropInfo dropInfo)
        {
            DefaultDropHandler d = new DefaultDropHandler();

            if (dropInfo.TargetCollection == FilterPool)
            {
                return;
            }

            d.DragOver(dropInfo);
        }


        void IDropTarget.Drop(DropInfo dropInfo)
        {
            int insertIndex = dropInfo.InsertIndex;
            IList destinationList = DefaultDropHandler.GetList(dropInfo.TargetCollection);

            IEnumerable data = DefaultDropHandler.ExtractData(dropInfo.Data);

            //동일그룹 내 이동시
            if (dropInfo.DragInfo?.VisualSource == dropInfo.VisualTarget)
            {
               


                IList sourceList = DefaultDropHandler.GetList(dropInfo.DragInfo.SourceCollection);

                foreach (object o in data)
                {

                    if (dropInfo.DragInfo.SourceCollection is System.Collections.ObjectModel.ObservableCollection<VisionRecipeEditor.ImageFilter.ImageFilterBase> c)
                    {
                        int index = sourceList.IndexOf(o);
                        c.Move(index,insertIndex-1);
                        //c.MoveCurrentToPosition(--insertIndex);
                    }   

                    //int index = sourceList.IndexOf(o);

                    //if (index != -1)
                    //{   
                        
                    //    sourceList.RemoveAt(index);
                    //    if (sourceList == destinationList && index < insertIndex)
                    //    {
                    //        --insertIndex;
                    //    }
                    //}
                }

                //foreach (object o in data)
                //{
                //    destinationList.Insert(insertIndex++, o);
                //}
            }
            else
            {
                foreach (object o in data)
                {
                    if (o is ImageFilterBase filter)
                    {
                        destinationList.Insert(insertIndex++, filter.Clone());
                    }
                    else
                    {
                        destinationList.Insert(insertIndex++, o);
                    }
                }
            }
        }


        private void Save()
        {
            
            
        }

        private void Load()
        {

        }
    }
}
