﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VisionRecipeEditor.BaseModel;

/// <summary>
/// 
/// </summary>
namespace VisionRecipeEditor.ImageFilter
{
    public enum ParamType
    {
        Rect,
        Int,
        String
    }

    public abstract class ParamBase : ViewModelBase, ICloneable
    {
        private string _name;
        public string Name { get => _name; set => SetProperty(ref _name, value); }
        private string ParamID { get; set; }
        public object Clone()
        {
            return DeepCopy();
        }

        protected abstract ParamBase DeepCopy();
    }

    public interface IParamBase
    {

    }

    public class ParamInt : ParamBase
    {
        private int _value;
        private int _max;
        private int _min;
        private int _step;

        public int Value { get => _value; set => SetProperty(ref _value, value); }
        public int Max { get => _max; set => SetProperty(ref _max, value); }
        public int Min { get => _min; set => SetProperty(ref _min, value); }
        public int Step { get => _step; set => SetProperty(ref _step, value); }


        protected override ParamBase DeepCopy()
        {
            return new ParamInt
            {
                Name = string.Copy(this.Name),
                Value = this.Value,
                Max = this.Max,
                Min = this.Min,
                Step = this.Step
            };
        }
    }

    public class ParamFloat : ParamBase
    {
        private float _value;
        private float _max;
        private float _min;
        private float _step;

        public float Value { get => _value; set => SetProperty(ref _value, value); }
        public float Max { get => _max; set => SetProperty(ref _max, value); }
        public float Min { get => _min; set => SetProperty(ref _min, value); }
        public float Step { get => _step; set => SetProperty(ref _step, value); }


        protected override ParamBase DeepCopy()
        {
            return new ParamFloat
            {
                Name = string.Copy(this.Name),
                Value = this.Value,
                Max = this.Max,
                Min = this.Min,
                Step = this.Step
            };
        }
    }


    public class ParamRect : ParamBase
    {
        private int _x;
        private int _y;
        private int _width;
        private int _height;

        public int X { get => _x; set => SetProperty(ref _x, value); }
        public int Y { get => _y; set => SetProperty(ref _y, value); }
        public int Width { get => _width; set => SetProperty(ref _width, value); }
        public int Height { get => _height; set => SetProperty(ref _height, value); }
        
        public System.Drawing.Rectangle Value
        {
            set
            {
                X = value.X;
                Y = value.Y;
                Width = value.Width;
                Height = value.Height;
            }
            get
            {
                return new System.Drawing.Rectangle(X, Y, Width, Height);
            }
        }

        protected override ParamBase DeepCopy()
        {
            return new ParamRect
            {
                Name = string.Copy(this.Name),
                Value = this.Value
            };
        }
    }

    public class ParamString : ParamBase
    {
        private string _value;
        public string Value { get => _value; set => SetProperty(ref _value, value); }

        protected override ParamBase DeepCopy()
        {
            return new ParamString
            {
                Name = string.Copy(this.Name),
                Value = string.Copy(this.Value)
            };
        }
    }

    public class ParamBool : ParamBase
    {
        private bool _value;
        public bool Value { get => _value; set => SetProperty(ref _value, value); }

        protected override ParamBase DeepCopy()
        {
            return new ParamBool
            {
                Name = string.Copy(this.Name),
                Value = this.Value
            };
        }
    }

    public class ParamArray : ParamBase
    {
        private System.Collections.ObjectModel.ObservableCollection<object> _value;
        public System.Collections.ObjectModel.ObservableCollection<object> Value 
        {
            get => _value; set => SetProperty(ref _value, value); 
        }

        public IEnumerable<Object> ValueList
        {
            set
            {

                if (value != null)
                    Value = new System.Collections.ObjectModel.ObservableCollection<object>(value);
                else
                    Value = null;
            }
        }

        private object _selectedValue;
        public object SelectedValue { 
            get => _selectedValue;
            set
            {
                if(value!=null)
                {
                    if(_value.Contains(value))
                    {
                        SetProperty(ref _selectedValue, value);
                    }
                }
                else
                {
                    SetProperty(ref _selectedValue, value);
                }
            }
        }

        protected override ParamBase DeepCopy()
        {
            var ret =  new ParamArray
            {
                Name = string.Copy(this.Name),
                Value = new System.Collections.ObjectModel.ObservableCollection<object>((object[])this.Value.ToArray().Clone())
            };

            ret.SelectedValue = ret.Value[(this.Value.IndexOf(this.SelectedValue))];
            return ret;
        }
    }

}
