﻿using ImageProcessNET;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using VisionRecipeEditor.BaseModel;
using VisionRecipeEditor.Extension;

namespace VisionRecipeEditor.ImageFilter
{
    public interface IFilterBase : ICloneable
    {
        FilterPin[] GetPins();
        string GetKey();

        int GetDepth();
        float GetLevel();
        void SetLevel(object sender, float level);
        object GetLayer();
    }

    public abstract class ImageFilterBase : ViewModelBase, IFilterBase, ICloneable, IDisposable
    {

        private string _key;
        public string Key
        {
            get => _key;
            set => SetProperty(ref _key, value);
        }

        private string _name = "Unknown Filter";
        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        private double _processTime;
        public double ProcessTime
        {
            get => _processTime;
            set => SetProperty(ref _processTime, value);
        }

        private bool _enable = true;
        public bool Enable
        {
            get => _enable;
            set
            {

                if (_enable != value)
                {
                    _enable = value;
                    DoImageProcess();
                }

            }
        }

        protected Bitmap[] _imageSource;
        public Bitmap[] ImageSource
        {
            get => _imageSource;
            set
            {
                foreach (var img in ImageDest.OrEmptyIfNull())
                {
                    img.Dispose();
                }
                SetProperty(ref _imageSource, value);
                DoImageProcessWhenInputChanged();
            }
        }

        protected Bitmap[] _imageDest;
        public Bitmap[] ImageDest
        {
            get => _imageDest;
            set
            {
                foreach (var img in ImageDest.OrEmptyIfNull())
                {
                    img?.Dispose();
                }
                SetProperty(ref _imageDest, value);
            }
        }

        private bool _isEndPoint;
        public bool IsEndPoint
        {
            get => _isEndPoint;
            set => SetProperty(ref _isEndPoint, value);
        }

        private string _resultMessage;
        public string ResultMessage
        {
            get => _resultMessage;
            set => SetProperty(ref _resultMessage, value);
        }



        private ObservableCollection<object> _params = new ObservableCollection<object>();
        public ObservableCollection<object> Params
        {
            get => _params;
            set => SetProperty(ref _params, value);

        }

        protected void ParamsAdd(ParamBase param)
        {
            param.PropertyChanged += (s, e) => { DoImageProcess(); };// Param_PropertyChanged;
            Params.Add(param);
        }

        private ObservableCollection<FilterPin> _pins = new ObservableCollection<FilterPin>();
        public ObservableCollection<FilterPin> Pins
        {
            get => _pins;
            set => SetProperty(ref _pins, value);

        }

        public event PropertyChangedEventHandler PinPropertyChanged;

        protected void PinAdd(FilterPin pin)
        {
            if (pin.PinType == FilterPin.IOType.OUT)
            {
                pin.DataRequest += (s, e) => { DoImageProcessWhenInputChanged(); };
            }
            else if (pin.PinType == FilterPin.IOType.IN)
            {
                pin.EventPropagation += (s, e) => { DoImageProcessWhenInputChanged(); };

                pin.PropertyChanged += (s, e) =>
                {

                    PinPropertyChanged?.Invoke(s, e);
                    DoImageProcessWhenInputChanged();
                };
            }
            //pin.PropertyChanged += (s, e) => { Filter(); };// Param_PropertyChanged;
            Pins.Add(pin);
        }

        public ImageFilterBase(string title)
        {
            this.Name = title;

            var randKey = DateTime.Now.ToString("yyyyMMddHHmmssSSS" + Guid.NewGuid().ToString());

            byte[] data = System.Security.Cryptography.MD5.Create().ComputeHash(
                Encoding.UTF8.GetBytes(randKey));

            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length && i < 8; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            _key = sBuilder.ToString();
        }

        Dictionary<string, int> pinTokenDic = new Dictionary<string, int>();
        private bool IsInputImageChanged()
        {
            bool bUpdated = false;

            //모든 Input 링크 토큰 갱신, 변경 사항 확인
            foreach (var pin in GetPins())
            {
                if (pin.PinType == FilterPin.IOType.IN)
                {
                    int token = (pin.Link != null) ? pin.Link.UpdateToken : -1;

                    if (pinTokenDic.ContainsKey(pin.Name))
                    {
                        if (pinTokenDic[pin.Name] != token)
                        {
                            bUpdated = true;
                        }
                        pinTokenDic[pin.Name] = token;
                    }
                    else
                    {
                        pinTokenDic[pin.Name] = token;
                        bUpdated = true;
                    }
                }
            }
            return bUpdated;
        }

        public void DoImageProcess()
        {
            Stopwatch s = new Stopwatch();
            s.Start();
            if (!Enable)
            {
                DoFilterByPass();
            }
            else
            {
                Proc(GetPins(), GetParams());
            }

            s.Stop();
            ProcessTime = s.ElapsedMilliseconds;
        }


        public void DoImageProcessWhenInputChanged()
        {
            if (IsInputImageChanged())
            {
                DoImageProcess();
            }
        }

        public void DoFilterByPass()
        {
            List<Bitmap> retList = new List<Bitmap>();

            foreach (var pin in GetPins())
            {
                if (pin is FilterPin io && io.PinType == FilterPin.IOType.IN)
                {
                    foreach (var img in io.Image.OrEmptyIfNull())
                    {
                        if (img != null)
                            retList.Add((Bitmap)img.Clone());
                    }
                    break;
                }
            }

            foreach (var pin in GetPins())
            {
                if (pin is FilterPin io && io.PinType == FilterPin.IOType.OUT)
                {
                    this.ImageDest = retList.ToArray();
                    io.Image = this.ImageDest;
                }
            }
        }


        public object[] GetParams()
        {
            return Params.ToArray();
        }

        public object GetParam(string name)
        {
            foreach (var pin in Params.OrEmptyIfNull())
            {
                if (pin is ParamBase p && string.Compare(p.Name, name) == 0)
                {
                    return pin;
                }
            }
            return null;
        }

        public FilterPin[] GetPins()
        {
            return Pins.ToArray();
        }

        public FilterPin GetPin(string name)
        {
            foreach (var pin in Pins.OrEmptyIfNull())
            {
                if (pin is FilterPin io && string.Compare(io.KeyName, name) == 0)
                {
                    return pin;
                }
            }
            throw new KeyNotFoundException($"Pin NotFound : {Name}");
            //return null;
        }

        public string GetName()
        {
            return Name;
        }


        public abstract void Proc(object[] pins, object[] args);

        #region ICloneable
        public object Clone()
        {
            return DeepCopy();
        }

        protected abstract ImageFilterBase DeepCopy();

        public void Dispose()
        {
            foreach (var bitmap in ImageDest.OrEmptyIfNull())
            {
                bitmap?.Dispose();
            }
            ImageDest = null;


            foreach (var bitmap in ImageSource.OrEmptyIfNull())
            {
                bitmap?.Dispose();
            }
            ImageSource = null;


            foreach (var pin in Pins)
            {
                pin.Dispose();
            }
            Pins.Clear();
        }

        public string GetKey()
        {
            return _key;
        }



        public int GetDepth()
        {
            int max = 0;
            foreach (var pin in GetPins())
            {
                max = Math.Max(max, pin.LinkDepth);
            }
            return max;

        }

        public Dictionary<object, float> LinkLevel { get; set; } = new Dictionary<object, float>();
        public float GetLevel()
        {
            var pins = GetPins();
            for (int i = 0; i < LinkLevel.Count;)
            {
                bool bValid = false;
                if (LinkLevel.ElementAt(i).Key is ImageFilterBase f)
                {
                    foreach (var pin in f.GetPins())
                    {
                        if (pin?.Link?.Parent == this)
                        {
                            bValid = true;
                            break;
                        }
                    }
                }
                if (bValid)
                    i++;
                else
                    _ = LinkLevel.Remove(LinkLevel.ElementAt(i).Key);
            }

            if (LinkLevel.Count == 0)
                return 0;

            return LinkLevel.Values.Average();
            //return LinkLevel.Values.Max();
            //return LinkLevel;
        }

        public void SetLevel(object sender, float level)
        {
            if (sender != null)
                LinkLevel[sender] = level;

            var pins = GetPins().Where(x => x.PinType == FilterPin.IOType.IN);
            if (pins.Count() == 1)
            {
                pins.ElementAt(0)?.Link?.Parent?.SetLevel(this, GetLevel());
            }
            else if (pins.Count() >= 2)
            {
                float pinStart = -pins.Count() / 2.0F;
                pinStart += GetLevel();
                pinStart += 0.5F;
                foreach (var pin in pins)
                {
                    pin?.Link?.Parent?.SetLevel(this, pinStart++);
                }
            }
        }

        public object GetLayer()
        {
            if (LinkLevel.Count() == 0)
                return this;

            foreach (var pair in LinkLevel)
            {
                if (pair.Key is IFilterBase f)
                {
                    return f.GetLayer();
                }
            }

            return this;
        }

        #endregion
    }

    /// <summary>
    /// 필터의 In/out 연결 핀 정의
    /// </summary>
    public class FilterPin : ViewModelBase, IDisposable
    {
        /// <summary>
        /// 핀 이름
        /// </summary>        
        private string _name = "####";
        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        public string KeyName
        {
            get { return $"{PinType}_{Name}"; }
        }

        public enum IOType { IN, OUT, NONE };

        private IOType _pinType = IOType.NONE;
        public IOType PinType
        {
            get => _pinType;
            set => SetProperty(ref _pinType, value);
        }

        public int LinkDepth { get; set; } = 0; // 상위 링크 참조 갯수
        /// <summary>
        /// 이미지 데이터를 요청할때 발생
        /// </summary>
        public event EventHandler DataRequest;

        /// <summary>
        /// 내부 버퍼 이미지
        /// </summary>
        protected Bitmap[] _image;
        public Bitmap[] Image
        {
            get
            {
                if (PinType == IOType.IN)
                {
                    return Link?.Image;
                }
                DataRequest?.Invoke(this, null);
                return _image;
            }
            set
            {
                if (PinType == IOType.OUT)
                {
                    UpdateToken++;

                    foreach (var img in Image.OrEmptyIfNull())
                    {
                        img?.Dispose();
                    }

                    SetProperty(ref _image, value);
                }
            }
        }

        /// <summary>
        /// 이미지 변경 상태를 확인하기 위한 토큰 변경시마다 1증가. 최대 값에서 0으로 돌아감 (overflow)
        /// </summary>
        readonly int TOKEN_MAX = 1024;
        private int _updateToken;
        public int UpdateToken
        {
            get => _updateToken;
            set
            {
                int setVal = value > TOKEN_MAX ? 0 : value;
                SetProperty(ref _updateToken, setVal);
            }
        }

        /// <summary>
        /// 핀과 연결된 항목
        /// </summary>
        private FilterPin _link;
        public FilterPin Link
        {
            get => _link;
            private set
            {
                if (_link != null)
                {
                    _link.DisconnectReqeust -= _link_DisconnectReqeust;
                    _link.PropertyChanged -= _link_PropertyChanged;
                }

                SetProperty(ref _link, value);

                if (_link != null)
                {
                    _link.PropertyChanged += _link_PropertyChanged;
                    _link.DisconnectReqeust += _link_DisconnectReqeust;
                }
            }
        }


        /// <summary>
        /// 연결 해지 요청
        /// </summary>
        public event EventHandler DisconnectReqeust;

        /// <summary>
        /// 핀에 연결된 데이터 변경 알림
        /// </summary>
        public event EventHandler EventPropagation;



        public readonly IFilterBase Parent;

        public FilterPin(IFilterBase filter, string name, IOType type)
        {
            Parent = filter;
            Name = name;
            PinType = type;
        }

        public void Dispose()
        {
            Link = null;
            DisconnectReqeust?.Invoke(this, null);

            foreach (var bitmap in Image.OrEmptyIfNull())
            {
                bitmap.Dispose();
            }

            Image = null;
        }
        private void _link_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(FilterPin.Image))
            {
                EventPropagation?.Invoke(this, null);
            }
        }

        private void _link_DisconnectReqeust(object sender, EventArgs e)
        {
            Link = null;
        }

        /// <summary>
        /// 핀에 다른 필터의 을 연결
        /// 핀 연결시 순환 참조를 검사를 포함.
        /// </summary>
        /// <param name="linkpin"></param>
        /// <returns>설정 성공여부 (Input 핀에만 설정 가능,  순환참조 확인시 False를 반환) </returns>
        public bool SetLink(FilterPin linkpin)
        {
            if (this.PinType != IOType.IN)
            {
                return false;
            }

            if (linkpin != null && IsLinkedPin(linkpin.Parent, this.Parent))
            {
                return false;
            }

            Link = linkpin;
            GetLinkDepth();
            return true;
        }



        public int GetLinkDepth()
        {
            Parent.SetLevel(null, Parent.GetLevel());

            if (Link != null)
            {
                int maxDepth = 0;
                foreach (var pin in Link.Parent.GetPins().OrEmptyIfNull())
                {
                    maxDepth = Math.Max(maxDepth, pin.GetLinkDepth());
                }
                LinkDepth = maxDepth;
            }
            else
                LinkDepth = 0;
            return LinkDepth + 1;
        }


        /// <summary>
        /// 순환 참조 여부 확인
        /// </summary>
        /// <param name="f"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        private static bool IsLinkedPin(IFilterBase f, IFilterBase p)
        {
            foreach (var pin in f.GetPins().OrEmptyIfNull())
            {
                if (pin.Link == null)
                {
                    continue;
                }

                if (pin.Link.Parent == null)
                {
                    continue;
                }

                if (pin.Link.Parent == p)
                {
                    return true;
                }

                if (IsLinkedPin(pin.Link.Parent, p))
                {
                    return true;
                }
            }
            return false;
        }

        internal void RaiseLinkUpdate()
        {
            this.OnPropertyChanged(nameof(this.Link));
            GetLinkDepth();
        }
    }


    public class SourceFilter : ImageFilterBase
    {
        public string Path
        {
            get
            {
                if (base.Params != null && base.Params.Count > 0 && base.Params[0] is ParamString s)
                {
                    return s.Value;
                }
                return null;
            }
            set
            {
                if (base.Params != null && base.Params.Count > 0 && base.Params[0] is ParamString s)
                {
                    s.Value = value;
                }
            }
        }

        public SourceFilter(string title = "Source") : base(title)
        {
            base.ParamsAdd(new ParamString() { Name = "Path", Value = "Path" });
            base.PinAdd(new FilterPin(this, "Out", FilterPin.IOType.OUT));
        }




        public override void Proc(object[] pins, object[] args)
        {
            List<Bitmap> retList = new List<Bitmap>();
            foreach (var arg in args.OrEmptyIfNull())
            {
                if (arg is ParamString s)
                {
                    if (System.IO.File.Exists(s.Value))
                    {
                        retList.Add(new Bitmap(s.Value));
                    }
                }
            }

            foreach (var pin in pins.OrEmptyIfNull())
            {
                if (pin is FilterPin io && io.PinType == FilterPin.IOType.OUT)
                {
                    this.ImageDest = retList.ToArray();
                    io.Image = this.ImageDest;
                }
            }
        }

        protected override ImageFilterBase DeepCopy()
        {
            SourceFilter newFilter = new SourceFilter(string.Copy(this.Name));

            newFilter.Path = this.Path;
            return newFilter;
        }
    }

    /// <summary>
    /// 지정된 값으로 이미지 이진화
    /// </summary>
    public class ThresholdFilter : ImageFilterBase
    {
        readonly string THRESHOLD = "Threshold";
        public ThresholdFilter(string title = "Binary") : base(title)
        {
            base.ParamsAdd(new ParamInt() { Name = THRESHOLD, Value = (255 / 2), Min = 0, Max = 255, Step = 1 });
            base.PinAdd(new FilterPin(this, "In", FilterPin.IOType.IN));
            base.PinAdd(new FilterPin(this, "Out", FilterPin.IOType.OUT));
        }



        public override void Proc(object[] pins, object[] args)
        {
            List<Bitmap> retList = new List<Bitmap>();

            int threshold = 255 / 2;

            foreach (var arg in args.OrEmptyIfNull())
            {
                if (arg is ParamInt p && string.CompareOrdinal(p.Name, THRESHOLD) == 0)
                {
                    threshold = p.Value;
                    break;
                }
            }

            foreach (var pin in pins.OrEmptyIfNull())
            {
                if (pin is FilterPin io && io.PinType == FilterPin.IOType.IN)
                {
                    foreach (var img in io.Image.OrEmptyIfNull())
                    {
                        retList.Add(ImageProcess.Threshold(
                            img,
                            threshold,
                            255,
                            ImageProcess.ThresholdTypes.Binary
                            ));

                    }

                }
            }

            foreach (var pin in pins.OrEmptyIfNull())
            {
                if (pin is FilterPin io && io.PinType == FilterPin.IOType.OUT)
                {
                    this.ImageDest = retList.ToArray();
                    io.Image = this.ImageDest;
                }
            }
        }

        protected override ImageFilterBase DeepCopy()
        {
            ThresholdFilter newFilter = new ThresholdFilter(string.Copy(this.Name));

            newFilter.Params.Clear();
            foreach (ParamBase param in this.Params)
            {
                newFilter.ParamsAdd((ParamBase)param.Clone());
            }

            return newFilter;
        }

    }

    /// <summary>
    /// 설정된 주변 영역 범위로 이미지 흐림
    /// </summary>
    public class BlurFilter : ImageFilterBase
    {
        readonly string KERNEL = "Kernel";
        public BlurFilter(string title = "Blur") : base(title)
        {
            base.ParamsAdd(new ParamInt() { Name = KERNEL, Value = 3, Min = 0, Max = 21, Step = 2 });
            base.PinAdd(new FilterPin(this, "In", FilterPin.IOType.IN));
            base.PinAdd(new FilterPin(this, "Out", FilterPin.IOType.OUT));
        }


        public override void Proc(object[] pins, object[] args)
        {
            List<Bitmap> retList = new List<Bitmap>();

            int kernel = 3;

            foreach (var arg in args.OrEmptyIfNull())
            {
                if (arg is ParamInt p && string.CompareOrdinal(p.Name, KERNEL) == 0)
                {
                    kernel = p.Value;
                    break;
                }
            }

            foreach (var pin in pins.OrEmptyIfNull())
            {
                if (pin is FilterPin io && io.PinType == FilterPin.IOType.IN)
                {
                    foreach (var img in io.Image.OrEmptyIfNull())
                    {
                        retList.Add(ImageProcess.Blur(
                   img,
                   kernel,
                   0,
                   ImageProcess.BlurTypes.Normal
                   ));


                    }

                }
            }

            foreach (var pin in pins.OrEmptyIfNull())
            {
                if (pin is FilterPin io && io.PinType == FilterPin.IOType.OUT)
                {
                    this.ImageDest = retList.ToArray();
                    io.Image = this.ImageDest;
                }
            }
        }
        protected override ImageFilterBase DeepCopy()
        {
            BlurFilter newFilter = new BlurFilter(string.Copy(this.Name));

            newFilter.Params.Clear();
            foreach (ParamBase param in this.Params)
            {
                newFilter.ParamsAdd((ParamBase)param.Clone());
            }
            return newFilter;
        }
    }

    public class MedianFilter : ImageFilterBase
    {
        readonly string KERNEL = "Kernel";
        public MedianFilter(string title = "Median") : base(title)
        {
            base.ParamsAdd(new ParamInt() { Name = KERNEL, Value = 3, Min = 0, Max = 21, Step = 2 });
            base.PinAdd(new FilterPin(this, "In", FilterPin.IOType.IN));
            base.PinAdd(new FilterPin(this, "Out", FilterPin.IOType.OUT));
        }


        public override void Proc(object[] pins, object[] args)
        {
            List<Bitmap> retList = new List<Bitmap>();

            int kernel = 3;

            foreach (var arg in args.OrEmptyIfNull())
            {
                if (arg is ParamInt p && string.CompareOrdinal(p.Name, KERNEL) == 0)
                {
                    kernel = p.Value;
                    break;
                }
            }

            foreach (var pin in pins.OrEmptyIfNull())
            {
                if (pin is FilterPin io && io.PinType == FilterPin.IOType.IN)
                {
                    foreach (var img in io.Image.OrEmptyIfNull())
                    {
                        retList.Add(ImageProcess.Blur(
                   img,
                   kernel,
                   0,
                   ImageProcess.BlurTypes.Median
                   ));

                    }

                }
            }

            foreach (var pin in pins.OrEmptyIfNull())
            {
                if (pin is FilterPin io && io.PinType == FilterPin.IOType.OUT)
                {
                    this.ImageDest = retList.ToArray();
                    io.Image = this.ImageDest;
                }
            }
        }
        protected override ImageFilterBase DeepCopy()
        {
            MedianFilter newFilter = new MedianFilter(string.Copy(this.Name));
            newFilter.Params.Clear();
            foreach (ParamBase param in this.Params)
            {
                newFilter.ParamsAdd((ParamBase)param.Clone());
            }
            //if (newFilter.Params[0] is ParamInt i && this.Params[0] is ParamInt j)
            //{
            //    i.Value = j.Value;
            //    i.Min = j.Min;
            //    i.Max = j.Max;                
            //}
            return newFilter;
        }
    }

    public class GaussianBlurFilter : ImageFilterBase
    {
        private readonly string KERNEL = "Kernel";
        private readonly string SIGMA = "Sigma";
        public GaussianBlurFilter(string title = "Gaussian") : base(title)
        {
            base.ParamsAdd(new ParamInt() { Name = KERNEL, Value = 3, Min = 0, Max = 21, Step = 2 });
            base.ParamsAdd(new ParamInt() { Name = SIGMA, Value = 3, Min = 0, Max = 21, Step = 2 });
            base.PinAdd(new FilterPin(this, "In", FilterPin.IOType.IN));
            base.PinAdd(new FilterPin(this, "Out", FilterPin.IOType.OUT));
        }


        public override void Proc(object[] pins, object[] args)
        {
            List<Bitmap> retList = new List<Bitmap>();

            int kernel = 3;
            int sigma = 3;

            foreach (var arg in args.OrEmptyIfNull())
            {
                if (arg is ParamInt p)
                {
                    if (string.CompareOrdinal(p.Name, KERNEL) == 0)
                    {
                        kernel = p.Value;
                    }
                    else if (string.CompareOrdinal(p.Name, SIGMA) == 0)
                    {
                        sigma = p.Value;
                    }

                }
            }

            foreach (var pin in pins.OrEmptyIfNull())
            {
                if (pin is FilterPin io && io.PinType == FilterPin.IOType.IN)
                {
                    foreach (var img in io.Image.OrEmptyIfNull())
                    {
                        retList.Add(ImageProcess.Blur(
                    img,
                    kernel,
                    sigma,
                    ImageProcess.BlurTypes.Gaussian
                    ));
                    }

                }
            }

            foreach (var pin in pins.OrEmptyIfNull())
            {
                if (pin is FilterPin io && io.PinType == FilterPin.IOType.OUT)
                {
                    this.ImageDest = retList.ToArray();
                    io.Image = this.ImageDest;
                }
            }
        }
        protected override ImageFilterBase DeepCopy()
        {
            GaussianBlurFilter newFilter = new GaussianBlurFilter(string.Copy(this.Name));
            newFilter.Params.Clear();
            foreach (ParamBase param in this.Params)
            {
                newFilter.ParamsAdd((ParamBase)param.Clone());
            }
            return newFilter;
        }
    }



    public class CropFilter : ImageFilterBase
    {

        private readonly string AREA = @"Area";
        public CropFilter(string title = "Crop") : base(title)
        {
            base.ParamsAdd(new ParamRect() { Name = AREA });
            base.PinAdd(new FilterPin(this, "In", FilterPin.IOType.IN));
            base.PinAdd(new FilterPin(this, "Out", FilterPin.IOType.OUT));
        }



        public override void Proc(object[] pins, object[] args)
        {
            List<Bitmap> retList = new List<Bitmap>();

            Rectangle area = new Rectangle();


            foreach (var arg in args.OrEmptyIfNull())
            {
                if (arg is ParamRect p && string.CompareOrdinal(p.Name, AREA) == 0)
                {
                    area = new Rectangle(p.X, p.Y, p.Width, p.Height);
                    break;
                }
            }

            bool bSetSource = false; // 첫번째 핀 이미지 미리보기

            foreach (var pin in pins.OrEmptyIfNull())
            {
                if (pin is FilterPin io && io.PinType == FilterPin.IOType.IN)
                {
                    if (!bSetSource && io.Image.OrEmptyIfNull().Count() > 0)
                    {
                        this.ImageSource = io.Image;
                        bSetSource = true;
                    }
                    foreach (var img in io.Image.OrEmptyIfNull())
                    {
                        retList.Add(ImageProcess.Crop(img, area));
                    }
                }
            }

            foreach (var pin in pins.OrEmptyIfNull())
            {
                if (pin is FilterPin io && io.PinType == FilterPin.IOType.OUT)
                {
                    this.ImageDest = retList.ToArray();
                    io.Image = this.ImageDest;
                }
            }
        }
        protected override ImageFilterBase DeepCopy()
        {
            CropFilter newFilter = new CropFilter(string.Copy(this.Name));
            newFilter.Params.Clear();
            foreach (ParamBase param in this.Params)
            {
                newFilter.ParamsAdd((ParamBase)param.Clone());
            }

            return newFilter;
        }

    }


    public class NormalizeFilter : ImageFilterBase
    {
        public NormalizeFilter(string title = "Normalize") : base(title)
        {
            base.PinAdd(new FilterPin(this, "In", FilterPin.IOType.IN));
            base.PinAdd(new FilterPin(this, "Out", FilterPin.IOType.OUT));
        }


        public override void Proc(object[] pins, object[] args)
        {
            List<Bitmap> retList = new List<Bitmap>();


            foreach (var pin in pins.OrEmptyIfNull())
            {
                if (pin is FilterPin io && io.PinType == FilterPin.IOType.IN)
                {
                    foreach (var img in io.Image.OrEmptyIfNull())
                    {
                        retList.Add(ImageProcess.Normalize(img));
                    }
                }
            }

            foreach (var pin in pins.OrEmptyIfNull())
            {
                if (pin is FilterPin io && io.PinType == FilterPin.IOType.OUT)
                {
                    this.ImageDest = retList.ToArray();
                    io.Image = this.ImageDest;
                }
            }
        }
        protected override ImageFilterBase DeepCopy()
        {
            NormalizeFilter newFilter = new NormalizeFilter(string.Copy(this.Name));
            newFilter.Params.Clear();
            foreach (ParamBase param in this.Params)
            {
                newFilter.ParamsAdd((ParamBase)param.Clone());
            }
            return newFilter;
        }
    }

    public class TransformECCFilter : ImageFilterBase
    {

        public TransformECCFilter(string title = "TransformECC") : base(title)
        {
            base.PinAdd(new FilterPin(this, "Src", FilterPin.IOType.IN));
            base.PinAdd(new FilterPin(this, "Ref", FilterPin.IOType.IN));
            base.PinAdd(new FilterPin(this, "Out", FilterPin.IOType.OUT));
        }


        public override void Proc(object[] pins, object[] args)
        {
            List<Bitmap> retList = new List<Bitmap>();

            FilterPin srcImgs = null;
            FilterPin refImgs = null;
            foreach (var pin in pins.OrEmptyIfNull())
            {
                if (pin is FilterPin io && io.PinType == FilterPin.IOType.IN)
                {
                    if (io.Name == "Src")
                        srcImgs = io;
                    else if (io.Name == "Ref")
                        refImgs = io;
                }
            }

            if (srcImgs.Image != null && refImgs.Image != null)
            {
                if (srcImgs.Image.Length == refImgs.Image.Length)
                {

                    for (int i = 0; i < srcImgs.Image.Length; ++i)
                    {
                        if (i < srcImgs.Image.Length && i < refImgs.Image.Length)
                        {
                            retList.Add(ImageProcess.FindTransformECC(srcImgs.Image[i], refImgs.Image[i]));
                        }
                    }
                }
                else if (refImgs.Image.Length > 0)
                {
                    foreach (var img in srcImgs.Image.OrEmptyIfNull())
                    {
                        retList.Add(ImageProcess.FindTransformECC(img, refImgs.Image[0]));
                    }
                }
            }

            foreach (var pin in pins.OrEmptyIfNull())
            {
                if (pin is FilterPin io && io.PinType == FilterPin.IOType.OUT)
                {
                    this.ImageDest = retList.ToArray();
                    io.Image = this.ImageDest;
                }
            }
        }
        protected override ImageFilterBase DeepCopy()
        {
            TransformECCFilter newFilter = new TransformECCFilter(string.Copy(this.Name));
            newFilter.Params.Clear();
            foreach (ParamBase param in this.Params)
            {
                newFilter.ParamsAdd((ParamBase)param.Clone());
            }
            return newFilter;
        }
    }

    public class SubStractFilter : ImageFilterBase
    {

        public SubStractFilter(string title = "Substract") : base(title)
        {
            base.PinAdd(new FilterPin(this, "Src", FilterPin.IOType.IN));
            base.PinAdd(new FilterPin(this, "Ref", FilterPin.IOType.IN));
            base.PinAdd(new FilterPin(this, "Out", FilterPin.IOType.OUT));
        }


        public override void Proc(object[] pins, object[] args)
        {
            List<Bitmap> retList = new List<Bitmap>();

            FilterPin srcImgs = null;
            FilterPin refImgs = null;
            foreach (var pin in pins.OrEmptyIfNull())
            {
                if (pin is FilterPin io && io.PinType == FilterPin.IOType.IN)
                {
                    if (io.Name == "Src")
                        srcImgs = io;
                    else if (io.Name == "Ref")
                        refImgs = io;
                }
            }

            if (srcImgs.Image != null && refImgs.Image != null)
            {
                if (srcImgs.Image.Length == refImgs.Image.Length)
                {

                    for (int i = 0; i < srcImgs.Image.Length; ++i)
                    {
                        if (i < srcImgs.Image.Length && i < refImgs.Image.Length)
                        {
                            retList.Add(ImageProcess.SubStract(srcImgs.Image[i], refImgs.Image[i]));
                        }
                    }
                }
                else if (refImgs.Image.Length > 0)
                {
                    foreach (var img in srcImgs.Image.OrEmptyIfNull())
                    {
                        retList.Add(ImageProcess.SubStract(img, refImgs.Image[0]));
                    }
                }
            }

            foreach (var pin in pins.OrEmptyIfNull())
            {
                if (pin is FilterPin io && io.PinType == FilterPin.IOType.OUT)
                {
                    this.ImageDest = retList.ToArray();
                    io.Image = this.ImageDest;
                }
            }
        }
        protected override ImageFilterBase DeepCopy()
        {
            SubStractFilter newFilter = new SubStractFilter(string.Copy(this.Name));
            newFilter.Params.Clear();
            foreach (ParamBase param in this.Params)
            {
                newFilter.ParamsAdd((ParamBase)param.Clone());
            }
            return newFilter;
        }
    }
    public class PHOTFilter : ImageFilterBase
    {
        public PHOTFilter(string title = "Phase Only Trasnform") : base(title)
        {

            base.PinAdd(new FilterPin(this, "In", FilterPin.IOType.IN));
            base.PinAdd(new FilterPin(this, "Out", FilterPin.IOType.OUT));
        }


        public override void Proc(object[] pins, object[] args)
        {
            List<Bitmap> retList = new List<Bitmap>();


            foreach (var pin in pins.OrEmptyIfNull())
            {
                if (pin is FilterPin io && io.PinType == FilterPin.IOType.IN)
                {
                    foreach (var img in io.Image.OrEmptyIfNull())
                    {
                        retList.Add(ImageProcess.PHOT(img));
                    }
                }
            }

            foreach (var pin in pins.OrEmptyIfNull())
            {
                if (pin is FilterPin io && io.PinType == FilterPin.IOType.OUT)
                {
                    this.ImageDest = retList.ToArray();
                    io.Image = this.ImageDest;
                }
            }
        }
        protected override ImageFilterBase DeepCopy()
        {
            PHOTFilter newFilter = new PHOTFilter(string.Copy(this.Name));

            newFilter.Params.Clear();
            foreach (ParamBase param in this.Params)
            {
                newFilter.ParamsAdd((ParamBase)param.Clone());
            }
            return newFilter;
        }
    }


    public class EnhancedOtsuThresholdFilter : ImageFilterBase
    {

        readonly string TORELENCE = "Torelence";
        readonly string INVERT = "Invert";
        public EnhancedOtsuThresholdFilter(string title = "Enhanced Otsu") : base(title)
        {

            base.ParamsAdd(new ParamInt()
            {
                Name = TORELENCE,
                Value = (byte.MaxValue / 2),
                Min = byte.MinValue,
                Max = byte.MaxValue,
                Step = 1
            });
            base.ParamsAdd(new ParamBool() { Name = INVERT, Value = false });
            base.PinAdd(new FilterPin(this, "In", FilterPin.IOType.IN));
            base.PinAdd(new FilterPin(this, "Out", FilterPin.IOType.OUT));
        }



        public override void Proc(object[] pins, object[] args)
        {
            List<Bitmap> retList = new List<Bitmap>();

            int torelence = byte.MaxValue / 2;
            bool bWhite = true;

            foreach (var arg in args.OrEmptyIfNull())
            {
                if (arg is ParamInt p && string.CompareOrdinal(p.Name, TORELENCE) == 0)
                {
                    torelence = p.Value;

                }
                if (arg is ParamBool b && string.CompareOrdinal(b.Name, INVERT) == 0)
                {
                    bWhite = !b.Value;

                }
            }

            List<int> resultThresHold = new List<int>();
            foreach (var pin in pins.OrEmptyIfNull())
            {
                if (pin is FilterPin io && io.PinType == FilterPin.IOType.IN)
                {
                    foreach (var img in io.Image.OrEmptyIfNull())
                    {
                        retList.Add(MVisionWrapperNET.MVisionWrapper.Enhance_Threshold_Otsu(
                            img,
                            bWhite,
                            torelence,
                            out int threshold
                            ));
                        resultThresHold.Add(threshold);
                    }
                }
            }

            foreach (var pin in pins.OrEmptyIfNull())
            {
                if (pin is FilterPin io && io.PinType == FilterPin.IOType.OUT)
                {
                    this.ImageDest = retList.ToArray();
                    io.Image = this.ImageDest;
                }
            }

            this.ResultMessage = $"Threshold: {resultThresHold.FirstOrDefault()}";

        }

        protected override ImageFilterBase DeepCopy()
        {
            EnhancedOtsuThresholdFilter newFilter = new EnhancedOtsuThresholdFilter(string.Copy(this.Name));

            newFilter.Params.Clear();
            foreach (ParamBase param in this.Params)
            {
                newFilter.ParamsAdd((ParamBase)param.Clone());
            }

            return newFilter;
        }

    }

    public class EnhancedImageFilter : ImageFilterBase
    {
        readonly string DIRECTION = "Direction";
        readonly string SIGMA = "Sigma";
        readonly string SCALE = "Scale";
        public EnhancedImageFilter(string title = "Enhanced Image") : base(title)
        {
            var comboDirection = new ParamArray()
            {
                Name = DIRECTION,
                ValueList = Enum.GetNames(typeof(MVisionWrapperNET.MVisionWrapper.EN_FILTEREDIRECTION)).ToArray(),

            };

            comboDirection.SelectedValue = comboDirection.Value[1];
            base.ParamsAdd(comboDirection);
            base.ParamsAdd(new ParamInt() { Name = SIGMA, Value = 4, Min = 1, Max = 10, Step = 1 });
            base.ParamsAdd(new ParamFloat() { Name = SCALE, Value = 1, Min = 0.1F, Max = 20, Step = 0.1F });

            base.PinAdd(new FilterPin(this, "In", FilterPin.IOType.IN));
            base.PinAdd(new FilterPin(this, "Out", FilterPin.IOType.OUT));
        }


        public override void Proc(object[] pins, object[] args)
        {
            List<Bitmap> retList = new List<Bitmap>();

            float sigma = 1;
            float scale = 0.1F;
            MVisionWrapperNET.MVisionWrapper.EN_FILTEREDIRECTION direction = MVisionWrapperNET.MVisionWrapper.EN_FILTEREDIRECTION.ENHANCE_XY;

            foreach (var arg in args.OrEmptyIfNull())
            {
                if (arg is ParamArray s && string.CompareOrdinal(s.Name, DIRECTION) == 0 && s.SelectedValue != null)
                {

                    if (string.CompareOrdinal(s.SelectedValue.ToString(),
                        MVisionWrapperNET.MVisionWrapper.EN_FILTEREDIRECTION.ENHANCE_X.ToString()
                        ) == 0)
                    {
                        direction = MVisionWrapperNET.MVisionWrapper.EN_FILTEREDIRECTION.ENHANCE_X;
                    }
                    else if (string.CompareOrdinal(s.SelectedValue.ToString(),
                        MVisionWrapperNET.MVisionWrapper.EN_FILTEREDIRECTION.ENHANCE_Y.ToString()
                        ) == 0)
                    {
                        direction = MVisionWrapperNET.MVisionWrapper.EN_FILTEREDIRECTION.ENHANCE_Y;
                    }
                }

                if (arg is ParamFloat f)
                {
                    if (string.CompareOrdinal(f.Name, SCALE) == 0)
                    {
                        scale = f.Value;

                    }
                }
                if (arg is ParamInt n)
                    if (string.CompareOrdinal(n.Name, SIGMA) == 0)
                    {
                        sigma = n.Value;

                    }
            }

            foreach (var pin in pins.OrEmptyIfNull())
            {
                if (pin is FilterPin io && io.PinType == FilterPin.IOType.IN)
                {
                    foreach (var img in io.Image.OrEmptyIfNull())
                    {
                        retList.Add(
                            MVisionWrapperNET.MVisionWrapper.EnhanceImage(
                                img, sigma, scale, direction)
                            );
                        break;

                    }

                }
            }

            foreach (var pin in pins.OrEmptyIfNull())
            {
                if (pin is FilterPin io && io.PinType == FilterPin.IOType.OUT)
                {
                    this.ImageDest = retList.ToArray();
                    io.Image = this.ImageDest;
                }
            }
        }

        protected override ImageFilterBase DeepCopy()
        {
            EnhancedImageFilter newFilter = new EnhancedImageFilter(string.Copy(this.Name));

            newFilter.Params.Clear();
            foreach (ParamBase param in this.Params)
            {
                newFilter.ParamsAdd((ParamBase)param.Clone());
            }

            return newFilter;
        }

    }


    public class FilterRecipe
    {
        public string Name { get; set; }
        public string Comment { get; set; }

        public List<FilterAttribute> Filters { get; set; }
    }

    public class FilterAttribute
    {
        public string Name { get; set; }
        public string Key { get; set; }
        public string Type { get; set; }
        public bool Enable { get; set; }
        public bool IsEndPoint { get; set; }
        public List<FilterParamAttribute> Params { get; set; }
        public List<FilterPinAttribute> Pins { get; set; }
    }

    public class FilterParamAttribute
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }

    public class FilterPinAttribute
    {
        public string Type { get; set; }
        public string Name { get; set; }
        public FilterLinkAttribute Link { get; set; }
        public string Key { get; set; }
    }

    public class FilterLinkAttribute
    {
        public string FilterKey { get; set; }
        public string PinKey { get; set; }
    }



}
