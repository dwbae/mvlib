﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace VisionRecipeEditor.ImageFilter
{
    public class ImageFilterChainControl
    {

        /// <summary>
        /// 자동 갱신 여부
        /// </summary>
        public bool ChainUpdate { get; set; } = true;

        /// <summary>
        /// Filter 목록이 변경되면 이전에 처리되었던 이미지 결과를 전달.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        object MainChain;
        object Propagation;
        internal void CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            MainChain = sender;
            Propagation = null;
            List<object> a = new List<object>();

            if (e.OldItems != null)
            {
                foreach (var item in e.OldItems)
                {
                    if (item is ImageFilterBase f)
                    {
                        f.PropertyChanged -= FilterChainControl_PropertyChanged;
                    }
                }
            }

            if (e.NewItems != null)
            {
                foreach (var item in e.NewItems)
                {
                    if (item is ImageFilterBase f)
                    {
                        f.PropertyChanged += FilterChainControl_PropertyChanged;
                    }
                }
            }

            if (sender is ObservableCollection<ImageFilterBase> o)
            {

                int startIdxOld = Math.Min(e.OldStartingIndex, o.Count);
                int startIdxNew = Math.Min(e.NewStartingIndex, o.Count);
                int startIdx = Math.Max(startIdxNew, startIdxOld) - 1;

                if (startIdx >= 0 && startIdx < o.Count - 1)
                {
                    System.Diagnostics.Debug.WriteLine($"Add:{o[startIdx].Name}{o[startIdx].ImageDest}=>{o[startIdx + 1].Name}{o[startIdx + 1].ImageSource}");
                    Propagation = o[startIdx + 1];
                    o[startIdx + 1].ImageSource = o[startIdx].ImageDest;
                }
            }

        }

        /// <summary>
        /// Filter 수정이 발생하면 뒤쪽 이미지까지 변경 내용이 전파 되어야함.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FilterChainControl_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (Propagation == null)
                return;

            if (MainChain != null && MainChain is ObservableCollection<ImageFilterBase> o && sender is ImageFilterBase f)
            {
                if (e.PropertyName == nameof(f.ImageDest))
                {
                    for (int i = 0; i < o.Count - 1; ++i)
                    {
                        if (o[i] == f)
                        {
                            System.Diagnostics.Debug.WriteLine($"Updated:{o[i].Name}{o[i].ImageDest}=>{o[i + 1].Name}{o[i + 1].ImageSource}");
                            o[i + 1].ImageSource = o[i].ImageDest;


                            break;
                        }
                    }
                }
            }
        }

        CancellationTokenSource updateCancellationTokenSource;
        /// <summary>
        /// 필터 적용 사항 전부 갱신
        /// </summary>
        public async void Refresh()
        {
            updateCancellationTokenSource?.Cancel();
            updateCancellationTokenSource = new CancellationTokenSource();
            CancellationToken ct = updateCancellationTokenSource.Token;
            var task = Task.Run(() =>
            {
                ct.ThrowIfCancellationRequested();
                if (MainChain != null && MainChain is ObservableCollection<ImageFilterBase> o)
                {
                    for (int i = 0; i < o.Count - 1; ++i)
                    {
                        if (ct.IsCancellationRequested)
                        {
                            ct.ThrowIfCancellationRequested();
                        }
                        System.Diagnostics.Debug.WriteLine($"Updated:{o[i].Name}{o[i].ImageDest}=>{o[i + 1].Name}{o[i + 1].ImageSource}");
                        o[i + 1].ImageSource = o[i].ImageDest;
                    }
                }
            }, updateCancellationTokenSource.Token);

            try
            {
                await task;
            }
            catch
            {
            }
            finally
            {
                updateCancellationTokenSource.Dispose();
            }
        }
    


        public static ObservableCollection<ImageFilterBase> LoadFromJson(string jsonString)
        {
            FilterRecipe recipe = Newtonsoft.Json.JsonConvert.DeserializeObject<FilterRecipe>(jsonString);

            ObservableCollection<ImageFilterBase> ret = new ObservableCollection<ImageFilterBase>();


            foreach (var filter in recipe.Filters)
            {
                ImageFilterBase f = CreateInstance(filter.Type);
                if (f == null) continue;
                
                ret.Add(f);
                f.Name = filter.Name;
                f.Key = filter.Key;
                f.Enable = filter.Enable;
                f.IsEndPoint = filter.IsEndPoint;

                foreach (var param in filter.Params)
                {
                    var p = f.GetParam(param.Key);
                    if(p !=null)
                    {
                        if (p is ParamInt i)
                        {
                            i.Value = JsonConvert.DeserializeObject<int>(param.Value);
                        }
                        else if (p is ParamString s)
                        {
                            s.Value = param.Value;
                        }
                        else if (p is ParamRect r)
                        {
                            r.Value = JsonConvert.DeserializeObject<System.Drawing.Rectangle>(param.Value);
                        }

                        else if (p is ParamFloat fl)
                        {
                            fl.Value = JsonConvert.DeserializeObject<float>(param.Value);
                        }

                        else if (p is ParamBool b)
                        {
                            b.Value = JsonConvert.DeserializeObject<bool>(param.Value);
                        }

                        else if (p is ParamArray arr)
                        {
                            var selected = JsonConvert.DeserializeObject<object>(param.Value);

                            for (int ix = 0; ix < arr.Value.Count(); ++ix)
                            {
                                if(string.Compare(arr.Value[ix].ToString(), selected.ToString())==0)
                                {
                                    arr.SelectedValue = arr.Value[ix];
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            //모든 핀이 추가된후 링크 설정
            foreach (var filter in recipe.Filters)
            {
                foreach (var pin in filter.Pins)
                {
                    if(pin.Link!= null)
                    {
                        ImageFilterBase src = null;
                        ImageFilterBase target = null;

                        foreach(var addedfilter in ret)
                        {
                            if(addedfilter.Key == filter.Key)
                            {
                                src = addedfilter;
                            }
                            if(addedfilter.Key == pin.Link.FilterKey)
                            {
                                target = addedfilter;
                            }
                            if (src != null && target != null)
                            {
                                src.GetPin(pin.Key).SetLink(target.GetPin(pin.Link.PinKey));
                                break;
                            }
                        }
                    }
                }

            }



            return ret;
        }

        public static ImageFilterBase CreateInstance(string type)
        {
            if (type == typeof(SourceFilter).ToString())
            {
                return new SourceFilter();
            }
            if (type == typeof(BlurFilter).ToString())
            {
                return new BlurFilter();
            }
            if (type == typeof(MedianFilter).ToString())
            {
                return new MedianFilter();
            }
            if (type == typeof(GaussianBlurFilter).ToString())
            {
                return new GaussianBlurFilter();
            }
            if (type == typeof(CropFilter).ToString())
            {
                return new CropFilter();
            }
            if (type == typeof(NormalizeFilter).ToString())
            {
                return new NormalizeFilter();
            }
            if (type == typeof(TransformECCFilter).ToString())
            {
                return new TransformECCFilter();
            }
            if (type == typeof(SubStractFilter).ToString())
            {
                return new SubStractFilter();
            }
            if (type == typeof(ThresholdFilter).ToString())
            {
                return new ThresholdFilter();
            }
            if (type == typeof(PHOTFilter).ToString())
            {
                return new PHOTFilter();
            }
            if (type == typeof(EnhancedImageFilter).ToString())
            {
                return new EnhancedImageFilter();
            }
            if (type == typeof(EnhancedOtsuThresholdFilter).ToString())
            {
                return new EnhancedOtsuThresholdFilter();
            }
            return null;
        }

        public static string CovertToJson(IEnumerable<ImageFilterBase> filterChain)
        {

            FilterRecipe recipe = new FilterRecipe()
            {
                Name = "recipe XX",
                Comment = DateTime.Now.ToString(),
                Filters = new List<FilterAttribute>()
            };

            foreach(var filter in filterChain)
            {
                var type = filter.GetType();

                var filterattri = new FilterAttribute()
                {
                    Name = filter.Name,
                    Key = filter.GetKey(),
                    Type = type.ToString(),
                    Enable = filter.Enable,
                    IsEndPoint = filter.IsEndPoint,
                    Params = new List<FilterParamAttribute>(),
                    Pins = new List<FilterPinAttribute>()
                    
                };

                
                recipe.Filters.Add(filterattri);

                foreach(var p in filter.GetParams())
                {
                    if (p is ParamInt i)
                    {
                        filterattri.Params.Add(new FilterParamAttribute()
                        {
                            Key = i.Name,
                            Value = JsonConvert.SerializeObject(i.Value)
                        }); 
                    }
                    else if (p is ParamString s)
                    {
                        filterattri.Params.Add(new FilterParamAttribute()
                        {
                            Key = s.Name,
                            Value = s.Value
                        });
                    }
                    else if (p is ParamRect r)
                    {
                        filterattri.Params.Add(new FilterParamAttribute()
                        {
                            Key = r.Name,
                            Value = JsonConvert.SerializeObject(r.Value)
                        });
                    }
                    else if (p is ParamBool b)
                    {
                        filterattri.Params.Add(new FilterParamAttribute()
                        {
                            Key = b.Name,
                            Value = JsonConvert.SerializeObject(b.Value)
                        });
                    }
                    else if (p is ParamFloat f)
                    {
                        filterattri.Params.Add(new FilterParamAttribute()
                        {
                            Key = f.Name,
                            Value = JsonConvert.SerializeObject(f.Value)
                        });
                    }
                    else if (p is ParamArray arr)
                    {
                        filterattri.Params.Add(new FilterParamAttribute()
                        {
                            Key = arr.Name,
                            Value = JsonConvert.SerializeObject(arr.SelectedValue)
                        });
                    }
                }

                foreach (var io in filter.GetPins())
                {
                    filterattri.Pins.Add(new FilterPinAttribute()
                    {
                        Name = io.Name,
                        Key = io.KeyName,
                        Type = io.PinType.ToString(),

                        Link = io.Link != null ? new FilterLinkAttribute()
                        {
                            FilterKey = io.Link.Parent.GetKey(),
                            PinKey = io.Link.KeyName
                        } : null
                    });
                }

            }

            string sRet = JsonConvert.SerializeObject(recipe,
                       new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            return sRet;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterChain"></param>
        /// <returns>(EndPointFilterName, Recipe:JSON)</returns>
        public static IEnumerable<EndPointRecipe> CovertToJsonEachEndPoint(IEnumerable<ImageFilterBase> filterChain)
        {
            foreach (var filter in filterChain)
            {
                var jsonString = GetJsonByEndPoint(filter);
                if(string.IsNullOrEmpty(jsonString))
                {
                    continue;
                }
                else
                {
                    yield return new EndPointRecipe() { FilterName = $"{filter.Key}_{filter.Name}", Recipe = jsonString };
                }
            }
        }

        public struct EndPointRecipe
        {
            public string FilterName;
            public string Recipe;
        }


        public static void CollectLinkFilters(List<IFilterBase> collector,  IFilterBase filter)
        {
            if (!collector.Contains(filter))
                collector.Add(filter);

            foreach(var f in filter.GetPins())
            {
                if (f?.Link?.Parent != null)
                    CollectLinkFilters(collector, f.Link.Parent);
            }
        }

        public static string GetJsonByEndPoint( ImageFilterBase endPoint)
        {

            if (!endPoint.IsEndPoint)
                return null;

            List<IFilterBase> chainedFilter = new List<IFilterBase>();

            CollectLinkFilters(chainedFilter, endPoint);

            return CovertToJson(chainedFilter.Select(x => x as ImageFilterBase));
        }
    }


    
}
