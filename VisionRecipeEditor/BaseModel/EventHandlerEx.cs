﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisionRecipeEditor.BaseModel
{

    /// <summary>
    /// Object 배열을 포함하는 이벤트 정의
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void ObjectArrayEventHandler<T>(object sender, ObjectArrayEventArgs<T> e);
    /// <summary>
    /// 이전값과 변경된 값을 포함하는 이벤트 정의
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void ObjectChangeEventHandler<T>(object sender, ObjectChangedEventArg<T> e);

    /// <summary>
    /// 변수 하나를 포함하는 제네릭 이벤트 정의
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void ObjectEventHandler<T>(object sender, ObjectEventArgs<T> e);


    /// <summary>
    /// Object Array를 포함하는 이벤트 전달 변수
    /// </summary>
    public class ObjectArrayEventArgs<T> : EventArgs
    {
        private readonly T[] value;
        public T[] Value { get => value; }
        public ObjectArrayEventArgs(T[] value)
        {
            this.value = value;
        }
    }
    /// <summary>
    /// 이전값과 변경된값을 전달 하는 변수
    /// </summary>
    public class ObjectChangedEventArg<T> : EventArgs
    {
        private readonly T oldValue;
        public T OldValue { get => oldValue; }

        private readonly T newValue;
        public T NewValue { get => newValue; }

        public ObjectChangedEventArg(T oldvalue, T newvalue)
        {
            this.oldValue = oldvalue;
            this.newValue = newvalue;
        }
    }
    /// <summary>
    /// Object를 포함하는 이벤트 전달 변수
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ObjectEventArgs<T> : EventArgs
    {
        private readonly T value;
        public T Value { get => value; }
        public ObjectEventArgs(T value)
        {
            this.value = value;
        }
    }
}
