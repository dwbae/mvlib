﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using VisionRecipeEditor.ViewModel;

namespace VisionRecipeEditor
{
    /// <summary>
    /// App.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// 응용프로그램 시작점
        /// </summary>
        /// <param name="e"></param>
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);


            MainWindow window = new MainWindow();
            MainViewModel datacontext = new MainViewModel();
            window.DataContext = datacontext;
            window.Show();
        }
    }
}
