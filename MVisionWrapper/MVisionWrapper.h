﻿#pragma once

using namespace System;


#define PI 3.1415926535
#define _MV_NUM_FILTER 7


public ref struct MV_FILTER {
	float* s_pKernel;
	int       s_nSize;
	int       s_nDivisor;
} ;

namespace MVWrapper {


	public enum class EN_FILTEREDIRECTION {
		ENHANCE_XY = 0, 
		ENHANCE_X = 1, 
		ENHANCE_Y = 2, 
	};

	
	public ref class MVisoinWrapper
	{
	private:
		MV_FILTER m_filterGUS;
		MV_FILTER m_filterLOG;

		float m_fScaleFactor;
		float m_lfSigma;

		double Gaussian(double x, double sigma);
		double LOG(double x, double sigma);
		void FilterCreate(double s_lfSigma, double fScaleFactor);

	public:
		MVisoinWrapper();
		virtual ~MVisoinWrapper();

		void MVisoinWrapperClose();
		
		int EnhanceImage(array<Byte>^ arrSrcBuffer, array<Byte>^ arrDstBuffer, int nImgWidth, int nImgHeight, EN_FILTEREDIRECTION eFilterDirection, float fSigma, float fScaleFactor);
		
		int Enhance_Threshold_Otsu(array<Byte>^ arrSrcBuffer, array<Byte>^ arrDstBuffer, int nImgWidth, int nImgHeight, Boolean bWhite, int nAbsoluteThresh);


	};
}
