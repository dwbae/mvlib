#include "stdafx.h"
#include "MVisionWrapper.h"
#include "stdio.h"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/core/core.hpp"


//using namespace std;
using namespace cv;


namespace MVWrapper
{
	double MVisoinWrapper::Gaussian(double x, double sigma)
	{
		return (1.0 / (sqrt(2 * PI) * sigma) * exp(-x * x / (2.0 * sigma * sigma)));
	}

	double MVisoinWrapper::LOG(double x, double sigma)
	{
		double t;

		t = x * x / (sigma * sigma);
		return (1.0 / (sqrt(2 * PI) * sigma * sigma * sigma) * (t - 1.0) * exp(t / -2.0));
	}


	void MVisoinWrapper::FilterCreate(double s_lfSigma, double fScaleFactor)
	{
		fScaleFactor = fScaleFactor * 0.2;
		s_lfSigma = max(s_lfSigma, 1.0);

		if (m_filterGUS.s_pKernel)
		{
			delete[] m_filterGUS.s_pKernel;
			m_filterGUS.s_pKernel = NULL;
		}


		if (m_filterLOG.s_pKernel)
		{
			delete[] m_filterLOG.s_pKernel;
			m_filterLOG.s_pKernel = NULL;
		}

		double s_lfGusFltPeak = 71;
		double s_lfLOGFltPeak = 50;

		int nGusWidth = s_lfSigma * 4;
		int n_LogWidth = s_lfSigma * 8;

		int size = nGusWidth;
		int anc = size / 2;

		short* pGUS = (short*)new short[size];

		m_filterGUS.s_pKernel = new float[size];

		double scale = fabs(s_lfGusFltPeak / Gaussian(0.0, s_lfSigma));
		double sum = 0;
		char str[80];

		for (int i = 0; i < size; i++)
		{
			int x = i - anc;
			double val = Gaussian(x, s_lfSigma) * scale;
			pGUS[i] = (short)(val + (val >= 0 ? 0.5 : -0.5));
			sum += pGUS[i];
		}

		// flip
		for (int i = 0; i < size; i++)
		{
			m_filterGUS.s_pKernel[i] = pGUS[size - i - 1];

#ifdef _SHOW_DEBUG_INFO
			sprintf_s(str, "%d ", pFilterGUS->s_pKernel[i]);
			printf(str);
#endif
		}

#ifdef _SHOW_DEBUG_INFO
		printf("\n");
#endif

		m_filterGUS.s_nSize = size;
		m_filterGUS.s_nDivisor = sum;



		/////////////////////////////////////////////////////////////////////////////////////////////////////
		/// LOG 

		scale = fabs(s_lfLOGFltPeak / LOG(0.0, s_lfSigma));
		size = n_LogWidth;



		anc = size / 2;

		short* pLOG = (short*) new short[size];
		m_filterLOG.s_pKernel = new float[size];
		sum = 0;
		for (int i = 0; i < size; i++)
		{
			int x = i - anc;
			double val = -1.0 * LOG(x, s_lfSigma) * scale * fScaleFactor;
			pLOG[i] = (short)(val + (val >= 0 ? 0.5 : -0.5));


			sum += pLOG[i];
		}
		// flip
		for (int i = 0; i < size; i++)
		{

			m_filterLOG.s_pKernel[i] = pLOG[size - i - 1];
#ifdef _SHOW_DEBUG_INFO
			sprintf_s(str, "%d ", pFilterLOG->s_pKernel[i]);
			printf(str);
#endif
		}
#ifdef _SHOW_DEBUG_INFO
		printf("\n");
#endif
		m_filterLOG.s_nSize = size;
		m_filterLOG.s_nDivisor = 32;


		if (sum)
		{
			if ((size % 2) == 0)
				anc--;

			double ancValue = m_filterLOG.s_pKernel[anc];
			m_filterLOG.s_pKernel[n_LogWidth - 1] = m_filterLOG.s_pKernel[n_LogWidth - 1] - sum;

#ifdef _SHOW_DEBUG_INFO
			printf("make zero sum  .... anc%d  %f --> %d\n", anc, ancValue, pFilterLOG->s_pKernel[anc]);
#endif
		}

		delete[] pGUS; pGUS = 0;
		delete[] pLOG; pLOG = 0;
	}


	double otsu_8u_with_mask(const Mat1b src, const Mat1b& mask)
	{
		const int N = 256;
		int M = 0;
		int i, j, h[N] = { 0 };

		for (i = 0; i < src.rows; i++)
		{
			const uchar* psrc = src.ptr(i);
			const uchar* pmask = mask.ptr(i);
			for (j = 0; j < src.cols; j++)
			{
				if (pmask[j])
				{
					h[psrc[j]]++;
					++M;
				}
			}
		}

		double mu = 0, scale = 1. / (M);
		for (i = 0; i < N; i++)
			mu += i * (double)h[i];

		mu *= scale;
		double mu1 = 0, q1 = 0;
		double max_sigma = 0, max_val = 0;

		for (i = 0; i < N; i++)
		{
			double p_i, q2, mu2, sigma;

			p_i = h[i] * scale;
			mu1 *= q1;
			q1 += p_i;
			q2 = 1. - q1;

			if (std::min(q1, q2) < FLT_EPSILON || std::max(q1, q2) > 1. - FLT_EPSILON)
				continue;

			mu1 = (mu1 + i * p_i) / q1;
			mu2 = (mu - q1 * mu1) / q2;
			sigma = q1 * q2 * (mu1 - mu2) * (mu1 - mu2);

	
			if (sigma > max_sigma)
			{
				max_sigma = sigma;
				max_val = i;
			
			}
		}

		return max_val;
	}


	MVisoinWrapper::MVisoinWrapper()
	{
		m_fScaleFactor = 0;
		m_lfSigma = 0;
	}

	MVisoinWrapper::~MVisoinWrapper()
	{
		MVisoinWrapperClose();
	}

	void MVisoinWrapper::MVisoinWrapperClose()
	{
		if (m_filterGUS.s_pKernel)
		{
			delete[] m_filterGUS.s_pKernel;
			m_filterGUS.s_pKernel = NULL;
		}


		if (m_filterLOG.s_pKernel)
		{
			delete[] m_filterLOG.s_pKernel;
			m_filterLOG.s_pKernel = NULL;
		}
	}

	int MVisoinWrapper::Enhance_Threshold_Otsu(array<Byte>^ arrSrcBuffer, array<Byte>^ arrDstBuffer, int nImgWidth, int nImgHeight, Boolean bWhite, int nAbsoluteThresh)
	{
		int nReturn = 0;
		if (nImgWidth < 0 || nImgHeight < 0)
		{
			nReturn = -1;
			return nReturn;
		}

		pin_ptr<Byte> pSrcBuffer = &arrSrcBuffer[arrSrcBuffer->GetLowerBound(0)];
		pin_ptr<Byte> pDstBuffer = &arrDstBuffer[arrDstBuffer->GetLowerBound(0)];

		Mat matRaw;
		Mat matThresh;

		matRaw.create(cv::Size(nImgWidth, nImgHeight), CV_8U);

		
		memcpy(matRaw.data, pSrcBuffer, nImgWidth * nImgHeight);
		

		double dThreshValue;

		if (bWhite)
		{
			cv::threshold(matRaw, matThresh, 127, 255, THRESH_BINARY);
			dThreshValue = otsu_8u_with_mask(matRaw, matThresh);

			dThreshValue = MAX(dThreshValue, nAbsoluteThresh);
			threshold(matRaw, matThresh, dThreshValue, 255, THRESH_BINARY);
		}
		else
		{
			cv::threshold(matRaw, matThresh, 128, 255, THRESH_BINARY_INV);
			dThreshValue = otsu_8u_with_mask(matRaw, matThresh);

			dThreshValue = MIN(dThreshValue, nAbsoluteThresh);
			threshold(matRaw, matThresh, dThreshValue, 255, THRESH_BINARY_INV);
		}

		nReturn = (int)dThreshValue;		
		memcpy(pDstBuffer, matThresh.data, matThresh.cols * matThresh.rows);


		if (!matRaw.empty())
			matRaw.release();

		if (!matThresh.empty())
			matThresh.release();

		return nReturn;
	}

	int MVisoinWrapper::EnhanceImage(array<Byte>^ arrSrcBuffer, array<Byte>^ arrDstBuffer, int nImgWidth, int nImgHeight, EN_FILTEREDIRECTION eFilterDirection, float fSigma, float fScaleFactor)
	{
		int nReturn = 0;
		if (nImgWidth < 0 || nImgHeight < 0)
		{
			return -1;
		}

		pin_ptr<Byte> pSrcBuffer = &arrSrcBuffer[arrSrcBuffer->GetLowerBound(0)];
		pin_ptr<Byte> pDstBuffer = &arrDstBuffer[arrDstBuffer->GetLowerBound(0)];

		fSigma = MAX(1, fSigma);
		fScaleFactor = MAX(0.1, fScaleFactor);

		Mat matRaw;
		Mat matFilterSrc;
		Mat matFilterX;
		Mat matFilterY;
		Mat matDst;
		matRaw.create(cv::Size(nImgWidth, nImgHeight), CV_8U);
		memcpy(matRaw.data, pSrcBuffer, nImgWidth * nImgHeight);

		if (m_fScaleFactor != fScaleFactor || m_lfSigma != fSigma)
		{
			m_fScaleFactor = fScaleFactor;
			m_lfSigma = fSigma;

			FilterCreate(fSigma, fScaleFactor);

			for (int i = 0; i < m_filterGUS.s_nSize; i++)
			{
				m_filterGUS.s_pKernel[i] = m_filterGUS.s_pKernel[i] / m_filterGUS.s_nDivisor;
			}
			for (int i = 0; i < m_filterLOG.s_nSize; i++)
			{
				m_filterLOG.s_pKernel[i] = m_filterLOG.s_pKernel[i] / m_filterLOG.s_nDivisor;
			}
		}
	
		Mat m_KrnlLOG = cv::Mat(m_filterLOG.s_nSize, 1, CV_32FC1, m_filterLOG.s_pKernel);
		Mat m_KrnlGUS = cv::Mat(m_filterGUS.s_nSize, 1, CV_32FC1, m_filterGUS.s_pKernel);

		int xn = m_filterLOG.s_nSize;
		int yn = m_filterGUS.s_nSize;
		int xAnchor = (xn >> 1); //+ 1;
		int yAnchor = (yn >> 1); //+ 1;

		matRaw.convertTo(matFilterSrc, CV_32F, 1., -128.);

		switch (eFilterDirection)
		{
		case MVWrapper::EN_FILTEREDIRECTION::ENHANCE_X:
			cv::sepFilter2D(matFilterSrc, matFilterX, CV_32F, m_KrnlLOG, m_KrnlGUS, cv::Point(xAnchor - 1, yAnchor - 1), 0, BORDER_REPLICATE);
			break;
		case MVWrapper::EN_FILTEREDIRECTION::ENHANCE_Y:
			cv::sepFilter2D(matFilterSrc, matFilterX, CV_32F, m_KrnlGUS, m_KrnlLOG, cv::Point(yAnchor - 1, xAnchor - 1), 0, BORDER_REPLICATE);
			break;
		default:
			cv::sepFilter2D(matFilterSrc, matFilterX, CV_32F, m_KrnlLOG, m_KrnlGUS, cv::Point(xAnchor - 1, yAnchor - 1), 0, BORDER_REPLICATE);
			cv::sepFilter2D(matFilterSrc, matFilterY, CV_32F, m_KrnlGUS, m_KrnlLOG, cv::Point(yAnchor - 1, xAnchor - 1), 0, BORDER_REPLICATE);
			cv::add(matFilterX, matFilterY, matFilterX);	
			break;
		}

		matFilterX = matFilterX + 128;
		matFilterX.convertTo(matDst, CV_8U);

		memcpy(pDstBuffer, matDst.data, matDst.cols * matDst.rows);
	

		if (!matRaw.empty())
			matRaw.release();
		if (!matFilterSrc.empty())
			matFilterSrc.release();
		if (!matFilterX.empty())
			matFilterX.release();
		if (!matFilterY.empty())
			matFilterY.release();
		if (!matDst.empty())
			matDst.release();

		return nReturn;
	}


}



