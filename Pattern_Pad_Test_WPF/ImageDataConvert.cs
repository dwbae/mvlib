﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing.Imaging;
using System.Drawing;

namespace Pattern_Pad_Test_WPF
{
    public class ImageDataConvert
    {

        public static bool ImageToGrayBytes(Image data, int roiLeft, int roiTop, int roiRight, int roiBottom, ref byte[] graydata)
        {
            int pixelByte = 0;
            int ImageStride = 0;
            byte[] bytedata = ImageToRgbByteStream(data, roiLeft, roiTop, roiRight, roiBottom, GrayscaleAttribute(), out ImageStride, out pixelByte);
            if (bytedata == null)
                return false;
            int numbytes = bytedata.Length;
            int Imageheight = numbytes / ImageStride;

            ///AND Upside down
            graydata = new byte[(numbytes / pixelByte) + 1];
            int j = 0;
            for (int height = Imageheight - 1; height > 0; height--)
            {
                for (int stride = 0; stride < ImageStride; stride += pixelByte)
                {
                    graydata[j] = bytedata[stride + ((height - 1) * ImageStride)];
                    j++;
                }
            }

            return true;
        }
        private static byte[] ImageToRgbByteStream(Image image, int roiLeft, int roiTop, int roiRight, int roiBottom, ImageAttributes imageAttributes, out int stride, out int pixelByte)
        {
            stride = 0;
            pixelByte = 0;

            int TextureWidth = roiRight - roiLeft;
            int TextureHeight = roiBottom - roiTop;

            if (TextureWidth < 1 || TextureHeight < 1)
                return null;

            Bitmap bmpRoi = new Bitmap(TextureWidth, TextureHeight);

            using (Graphics g = Graphics.FromImage(bmpRoi))
            {
                if (imageAttributes == null)
                {
                    g.DrawImage(image, new Rectangle(0, 0, TextureWidth, TextureHeight), roiLeft, roiTop, TextureWidth, TextureHeight, GraphicsUnit.Pixel);
                }
                else
                {
                    g.DrawImage(image, new Rectangle(0, 0, TextureWidth, TextureHeight), roiLeft, roiTop, TextureWidth, TextureHeight, GraphicsUnit.Pixel, imageAttributes);
                }
                g.Flush();
            }
            return BitmapToByte(bmpRoi, out stride, out pixelByte);
        }

        public static byte[] BitmapToByte(Bitmap bmpImage, out int stride, out int pixelByte)
        {
            BitmapData bmpdata = bmpImage.LockBits(new Rectangle(0, 0, bmpImage.Width, bmpImage.Height), ImageLockMode.ReadOnly, bmpImage.PixelFormat);

            int numbytes = bmpdata.Stride * bmpImage.Height;
            byte[] bytedata = new byte[numbytes];
            IntPtr ptr = bmpdata.Scan0;
            System.Runtime.InteropServices.Marshal.Copy(ptr, bytedata, 0, numbytes);
            bmpImage.UnlockBits(bmpdata);

            pixelByte = Image.GetPixelFormatSize(bmpImage.PixelFormat) / 8;
            stride = bmpdata.Stride;
            return bytedata;
        }
        public static ImageAttributes GrayscaleAttribute()
        {
            ColorMatrix colorMatrix = new ColorMatrix(
                new float[][]
                {
                    new float[] {.3f, .3f, .3f, 0, 0},
                    new float[] {.59f, .59f, .59f, 0, 0},
                    new float[] {.11f, .11f, .11f, 0, 0},
                    new float[] {0, 0, 0, 1, 0},
                    new float[] {0, 0, 0, 0, 1}
                });
            ImageAttributes attributes = new ImageAttributes();
            attributes.SetColorMatrix(colorMatrix);
            return attributes;
        }

        public static Bitmap RGBByteStreamtoImage(byte[] rdata, byte[] gdata, byte[] bdata, int width, int height)
        {
            Bitmap bmp = new Bitmap(width, height);
            BitmapData bmpdata = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.WriteOnly, bmp.PixelFormat);

            int bytes = Math.Abs(bmpdata.Stride) * bmp.Height;
            byte[] rgbValues = new byte[bytes];

            System.Runtime.InteropServices.Marshal.Copy(bmpdata.Scan0, rgbValues, 0, bytes);

            int pixelByte = Image.GetPixelFormatSize(bmp.PixelFormat) / 8;

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    rgbValues[y * bmpdata.Stride + (x * pixelByte)] = bdata[y * width + x];
                    rgbValues[y * bmpdata.Stride + (x * pixelByte) + 1] = gdata[y * width + x];
                    rgbValues[y * bmpdata.Stride + (x * pixelByte) + 2] = rdata[y * width + x];
                    rgbValues[y * bmpdata.Stride + (x * pixelByte) + 3] = 255;
                }
            }

            System.Runtime.InteropServices.Marshal.Copy(rgbValues, 0, bmpdata.Scan0, bytes);
            bmp.UnlockBits(bmpdata);
            return bmp;
        }
    }
}
