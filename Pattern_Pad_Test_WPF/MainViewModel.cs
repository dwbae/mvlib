﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ModelBase;



namespace Pattern_Pad_Test_WPF
{
    public class MainViewModel : ViewModelBase
    {
        public ICommand CmdPatternPad { get; set; }

        public MainViewModel()
        {
            CommandInit();
        }

        private void CommandInit()
        {
            CmdPatternPad = new RelayCommand(
                (param) => { Pattern_Pad.Pattern("AAAA","BBBB", "CCCC"); },
                (param) => { return true; }
                );
        }
    }
}
