﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using ImageProcesCLR;
namespace Pattern_Pad_Test_WPF
{
    
    public class Pattern_Pad
    {
        //[DllImport("Pattern_Pad.dll")] public static extern int FunctionTest(int a);
        //public static void TestFunc()
        //{
        //    var ret = FunctionTest(1);
        //}

        public static void TestFunc()
        {
            ImageProcess imageProcess = new ImageProcess();
            var ret = imageProcess.FuncTest(1, 2);
        }

        public static void Pattern(string fileM, string fileR, string fileN)
        {
            ImageProcess.Pattern_Matching(fileM, fileR, fileN);
        }

        public static Bitmap EnhancedOTSU(Bitmap sourceBitmap)
        {
            MVWrapper.MVisoinWrapper wrapper = new MVWrapper.MVisoinWrapper();

            byte[] bytedata = new byte[0];
            ImageDataConvert.ImageToGrayBytes(sourceBitmap, 0, 0,  sourceBitmap.Width, sourceBitmap.Height, ref bytedata);

            wrapper.Enhance_Threshold_Otsu(bytedata, bytedata, sourceBitmap.Width, sourceBitmap.Height, true, 0);

            return ImageDataConvert.RGBByteStreamtoImage(bytedata, bytedata, bytedata, sourceBitmap.Width, sourceBitmap.Height);
        }


        public static Bitmap EnhancedOTSU_CLR(Bitmap sourceBitmap)
        {
            byte[] bytedata = new byte[0];
            ImageDataConvert.ImageToGrayBytes(sourceBitmap, 0, 0, sourceBitmap.Width, sourceBitmap.Height, ref bytedata);

            bytedata= ImageProcess.Enhance_Threshold_OtsuFilter(bytedata, sourceBitmap.Width, sourceBitmap.Height, true, 0);

            return ImageDataConvert.RGBByteStreamtoImage(bytedata, bytedata, bytedata, sourceBitmap.Width, sourceBitmap.Height);
        }




    }
}
