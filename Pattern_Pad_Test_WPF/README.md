﻿##Pattern_Pad_Test_WPF : ImageProcess64.dll - ImageProcessCLR.dll 의 동작을 확인하기 위한 테스트 프로그램

### Resource

* HomePage : <http://www.semics.com>
* Docs : <https://semicsrnd.atlassian.net/wiki/spaces/PVIP/pages/94635598/OPERA+Pattern+Pad>
* Build : Visual Studio 2019 (vc142) x64 

#### Summary

* 이 프로젝트는 OpenCV 3.2.0을 활용한 이미지 프로세스 기능의 동작을 확인하기 위한 실행프로그램 입니다. 
* 이 프로젝트의 ImageProcessCLR에 종속 되어 있습니다. 
	* Pattern_Pad_Test_WPF-> ImageProcessCLR -> ImageProcess64 -> OpenCV


#### History
* 2021-09-01 - Pattern_Pad.cs 내에 테스트 함수 작성, Pattern_Pad_Test_WPFTests 유닛 테스트 작성 및 검증
 