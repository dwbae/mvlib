﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ModelBase
{
    public abstract class ViewModelBase : INotifyPropertyChanged
    {   
        /// <summary>
        /// 속성 알림 이벤트
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// 변경 발생시 호출
        /// </summary>
        /// <param name="propertyName">리스너에게 알릴 속성이름(입력하지 않을경우 컴파일러에서 호출될때 자동으로 입력됨)</param>
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        /// <summary>
        /// 조건부 이벤트 발생
        /// </summary>
        /// <typeparam name="T">유형</typeparam>
        /// <param name="storage">기존값</param>
        /// <param name="value">변경할 값</param>
        /// <param name="propertyName">리스너에게 알릴 속성의 이름 (입력하지 않을경우 컴파일러에서 호출될때 자동으로 입력됨)</param>
        /// <returns></returns>
        protected virtual bool SetProperty<T>(ref T storage, T value, [CallerMemberName] string propertyName = "")
        {
            if (EqualityComparer<T>.Default.Equals(storage, value))
                return false;
            storage = value;
            this.OnPropertyChanged(propertyName);
            return true;
        }
    }
}
