#pragma once

#ifdef MVISIONLIBRARY_EXPORTS
#define MVISIONLIBRARY_API __declspec(dllexport) 
#else
#define MVISIONLIBRARY_API __declspec(dllimport) 
#endif


enum class EN_FILTEREDIRECTION {
	ENHANCE_XY = 0,
	ENHANCE_X = 1,
	ENHANCE_Y = 2,
};

extern "C" 		MVISIONLIBRARY_API int FuncTest(unsigned char* pSrcBuffer, unsigned char* pDstBuffer, int nImgWidth, int nImgHeight);
extern "C" 		MVISIONLIBRARY_API int EnhanceImage(unsigned char* pSrcBuffer, unsigned char* pDstBuffer, int nImgWidth, int nImgHeight, EN_FILTEREDIRECTION eFilterDirection, float fSigma, float fScaleFactor);
extern "C" 		MVISIONLIBRARY_API int Enhance_Threshold_Otsu(unsigned char* pSrcBuffer, unsigned char* pDstBuffer, int nImgWidth, int nImgHeight, bool bWhite, int nAbsoluteThresh);


