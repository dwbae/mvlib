#pragma once
#include "BaseDefine.h"
class IParam
{
public:
	virtual LPCTSTR GetName() = 0;
	virtual ~IParam() {}
};
