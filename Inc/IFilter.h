#pragma once
#include "BaseDefine.h"
#include "IPin.h"
class IFilter
{
public:
	virtual int Init(rapidjson::GenericValue<ENCODE>& value) = 0;
	virtual LPCTSTR GetKey() = 0;
	virtual void Process() = 0;
	virtual bool IsEndPoint() = 0;
	virtual std::vector<IPin*> GetOutPin() = 0;
	virtual IPin* GetPin(LPCTSTR pinKey) = 0;
	virtual ~IFilter() {};
};
