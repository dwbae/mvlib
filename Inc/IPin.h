#pragma once


#include "BaseDefine.h"


class IPin
{
public : 
	virtual bool IsOutput() = 0;
	virtual LPCTSTR GetKey() = 0;
	virtual cv::Mat* GetImage() = 0;
	virtual void SetImage(cv::Mat*) = 0;
	virtual void SetLink(IPin*) = 0;
	virtual IPin* GetLink() = 0;
	virtual void Dispose() = 0;
	virtual ~IPin() {};
};