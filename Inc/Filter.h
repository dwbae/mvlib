//----------------------------------------------------------------------------
/** @file Filter.h
	@brief 여기에는 필터와 Recipe의 원형이 되는 클래스를 정의합니다.
*/
//----------------------------------------------------------------------------
#pragma once

#include "BaseDefine.h"

#include "Factory.h"

#include "IFilter.h"
#include "IPin.h"
#include "IParam.h"

using namespace std;
using namespace rapidjson;

/// <summary>
/// 필터의 기본형 클래스 
/// </summary>
class Filter : public IFilter , public IBaseObject
{
protected:
	/// <summary>
	/// 핀 목록
	/// </summary>
	vector<IPin*> Pins;
	/// <summary>
	/// 필터의 변경 가능한 속성 정보
	/// </summary>
	vector<IParam*> Params;
	/// <summary>
	/// 필터 이름
	/// </summary>
	tstring strName;
	/// <summary>
	/// 필터의 고유 구분자
	/// </summary>
	tstring strKey;
	/// <summary>
	/// 필터의 동작 여부 true= 동작함 ,false=입력을 바로 출력으로 전달 (by-pass)
	/// </summary>
	bool bEnable;
	/// <summary>
	/// 필터의 출력을 레시피의 출력으로 사용할지 여부 true= 레시피의 출력 , false= 레시피 출력 항목 아님
	/// </summary>
	bool bIsEndPoint;
public:

	Filter();

	virtual ~Filter();
	/// <summary>
	/// 디버그 용도, 필터가 생성되었는지 확인을 위한 메시지 출력
	/// </summary>
	virtual void Print();// override;

	virtual void Process();// final;

	virtual bool IsEndPoint()// final
	{
		return bIsEndPoint;
	}

	virtual void Dispose();

	/// <summary>
	/// 필터의 동작, 이미지 변환 작업이 수행 되어야함
	/// </summary>
	/// <param name="value"></param>
	/// <returns></returns>
	virtual void Proc(vector<IPin*> Pins, vector<IParam*> params) ;// { return value; }
	virtual vector<IPin*> GetOutPin();// override;
	virtual IPin* GetPin(LPCTSTR pinKey);// override;
	/// <summary>
	/// 필터의 초기화 , Recipe.json에 기록된 내용 (필터 이름, 키워드, 설정값) 을 읽어 반영
	/// </summary>
	/// <param name="value"></param>
	/// <returns></returns>
	virtual int Init(GenericValue<ENCODE>& value);// override;

	virtual LPCTSTR GetKey() { return strKey.c_str(); }
protected:
	bool SetParam(LPCTSTR key, int value);
	bool SetParam(LPCTSTR key, LPCTSTR value);
	bool SetParam(LPCTSTR key, bool value);
	bool SetParam(LPCTSTR key, float value);
};


#define MAKEFILTER(NAME) class C##NAME : public Filter {public:	C##NAME();		virtual void Proc(vector<IPin*> Pins, vector<IParam*> params) override;	virtual int Init(GenericValue<ENCODE>& value) override;	virtual void Print() override;};
																	   


