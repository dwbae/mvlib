//----------------------------------------------------------------------------
/** @file Factory.h
	@brief 여기에는 팩토리 클래스의 등록과 관리를 위한 내용을 기술합니다.
*/
//----------------------------------------------------------------------------
#pragma once
#include "BaseDefine.h"


/// <summary>
/// 팩토리 클래스에서 관리하는 기본 인터페이스 정의
/// </summary>
class IBaseObject
{
	virtual void Print() = 0;
};

/// <summary>
/// 싱글턴/  팩토리 클래스의 등록과 생성을 관리
/// </summary>
class CFactory
{
public:
	static CFactory* Instance();
	/// <summary>
	/// 등록된 이름으로 해당하는 클래스를 생성하여 반환
	/// </summary>
	/// <param name="name">등록된 이름</param>
	/// <returns>생성된 클래스 포인터 (등록되지 않은 이름의 경우 nullptr)</returns>
	IBaseObject* CreateInstance(tstring name);
	/// <summary>
	/// 클래스 등록
	/// </summary>
	/// <param name="name">검색 키워드</param>
	/// <param name="classFactoryFunction">클래스</param>
	void RegisterFactoryFuncton(tstring name, std::function<IBaseObject* (void)> classFactoryFunction);

private:
	//using FactoryRegistry = std::unordered_map<tstring, std::function<IBaseObject* (void)>>;
	//FactoryRegistry mFactoryRegistry;

	std::unordered_map<tstring, std::function<IBaseObject* (void)>> mFactoryRegistry;
};



/// <summary>
/// 클래스 등록
/// </summary>
/// <typeparam name="T"></typeparam>
template<class T>
class CRegistrar {
public:
	CRegistrar(tstring className)
	{
		CFactory::Instance()->RegisterFactoryFuncton(
			className,
			[](void)-> IBaseObject* {return new T(); });
	}
};

#define REGISTER_CLASSNAME(NAME, TYPE)  CRegistrar<TYPE> registrar_##TYPE(NAME);