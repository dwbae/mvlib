#pragma once


#ifdef IMAGEPROCESS_EXPORTS
#define IMAGEPROCESSLIBRARY_API __declspec(dllexport)
#else
#define IMAGEPROCESSLIBRARY_API __declspec(dllimport)
#endif

typedef struct sImageData {
	int width; //image width pixel
	int height; //image height pixel
	char channel; //image color channel  gray=1, color=3
	char bits;//bit count of single pixel.  8 ,16, 24, 32 

	size_t szSize;//ImageDataSize    width*height*bits/8
	unsigned char* pData;//ImageData pointer
	sImageData() :
		width(0),
		height(0),
		channel(1),
		bits(0),
		szSize(0),
		pData(nullptr)
	{
	}
	~sImageData()
	{
		if (pData != nullptr)
		{
			delete[] pData;
		}
		pData = nullptr;
	}
}ImageData;

#pragma warning(disable : 4200)
typedef struct sImageParam {
	int argc;
	ImageData** argv;
	sImageParam():
		argc(0),
		argv(nullptr)
	{
	}
	~sImageParam()
	{
		for (int i = 0; i < argc; ++i)
		{
			delete argv[i];
			argv[i] = nullptr;
		}
		delete[] argv;

	}
}ImageParam;


extern "C" IMAGEPROCESSLIBRARY_API int Add(int a, int b);

extern "C" IMAGEPROCESSLIBRARY_API double  VBcv1032Pattern_Matching(const char* DirectionM, const char* DirectionR, const char* DirectionF);
extern "C" IMAGEPROCESSLIBRARY_API double  VBcv1032FindPatternReview(const char* DirectionR);
extern "C" IMAGEPROCESSLIBRARY_API double  VBcv1032CheckRefPadArea(const char* DirectionR, int PadSizeX, int PadSizeY);

extern "C" IMAGEPROCESSLIBRARY_API double  VBcv1032Pattern_MatchingECC(const char* DirectionM, const char* DirectionR);


extern "C" IMAGEPROCESSLIBRARY_API int RecipeExecute(const char* recipe, const char* param, ImageParam* src, ImageParam* ref, ImageParam* dst);
extern "C" IMAGEPROCESSLIBRARY_API int ImageParamRelease(ImageParam * dst);

