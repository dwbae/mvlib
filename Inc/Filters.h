//----------------------------------------------------------------------------
/** @file Filters.h
	@brief 여기에는 추가되는 필터 클래스를 정의합니다.
*/
//----------------------------------------------------------------------------
#pragma once
#ifndef FILTER_H_7ED72FDE-97BE-46A1-AE55-850B28C7C119
#define FILTER_H_7ED72FDE_97BE_46A1_AE55_850B28C7C119
#include "FilterBase.h"

class BlurFilter : public Filter
{
public:
	BlurFilter()
	{
		Pins.push_back(new Pin(_T("In")));
		Pins.push_back(new Pin(_T("Out")));
	}
	~BlurFilter() {	}

	virtual Mat Proc(Mat value) override;
	virtual int Init(GenericValue<ENCODE>& value) override;

	virtual void Print() override
	{
		cout << "BlurFilter" << endl;
	}
};
REGISTER_CLASSNAME(_T("VisionRecipeEditor.ImageFilter.BlurFilter"), BlurFilter);


class SourceFilter : public Filter
{
public:
	SourceFilter()
	{
		Pins.push_back(new Pin(_T("Out")));
	}
	~SourceFilter() {	}

	virtual Mat Proc(Mat value) override;
	virtual int Init(GenericValue<ENCODE>& value) override;

	virtual void Print() override
	{
		cout << "Source Filter" << endl;
	}
};
REGISTER_CLASSNAME(_T("VisionRecipeEditor.ImageFilter.SourceFilter"), SourceFilter);

#endif