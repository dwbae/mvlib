#pragma once
#include "IPin.h"
#include "IFilter.h"


/// <summary>
/// 필터간 데이터 연결점 클래스
/// </summary>
class Pin : public IPin
{
public:
	/// <summary>
	/// 핀 이름
	/// </summary>
	tstring strName;
	tstring strKey;
	/// <summary>
	/// 연결된 핀 정보
	/// </summary>
	IPin* Link;
	int LinkToken;

	/// <summary>
	/// 핀이 속해 있는 부모 클래스(필터)
	/// </summary>
	IFilter* Parent;

	cv::Mat* Image;
	int Token;

	bool isOutput;
public:
	Pin(IFilter* filter, const TCHAR* name, bool bOutput=false);
	virtual ~Pin();
	virtual bool IsOutput() override;
	virtual LPCTSTR GetKey() override;
	virtual cv::Mat* GetImage() override;
	virtual void SetImage(cv::Mat*) override;
	virtual void SetLink(IPin*) override;
	virtual IPin* GetLink() override;
	virtual void Dispose();
};
