//----------------------------------------------------------------------------
/** @file BaseDefine.h
	@brief 여기에는 전역으로 사용되는 정의를 기록합니다. 
*/
//----------------------------------------------------------------------------
#pragma once

#include <Windows.h>
#include <string>
#include <vector>
#include <unordered_map>
#include <functional>

#include "opencv2/opencv.hpp"
#include "RecipeJsonDefine.h"

#ifndef tstring
#ifdef _UNICODE
typedef std::wstring tstring;
#else
typedef std::string tstring;
#endif
#endif





