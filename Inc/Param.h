#pragma once
#include "IParam.h"

namespace imgproc
{
	class CParam : public IParam
	{
	protected:
		tstring _name;
	public:
		CParam(LPCTSTR name);
		virtual ~CParam();

		virtual LPCTSTR GetName() override;
		virtual void SetName(LPCTSTR name);
	};

	template<typename T>
	class Param : public CParam
	{
		T value;
	public:
		Param<T>(LPCTSTR name) :CParam(name) {};
		T GetValue() { return value; }
		void SetValue(T val) { value = val; }
	};
}