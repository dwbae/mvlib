#pragma once

#include "../rapidjson/include/rapidjson/document.h"
#include "../rapidjson/include/rapidjson/rapidjson.h"
#include "../rapidjson/include/rapidjson/encodedstream.h"

#ifdef _UNICODE
	#define ENCODE rapidjson::UTF16LE<>
#else
	#define ENCODE rapidjson::ASCII<>
#endif // 



#define RECIPE_JSON_NAME _T("Name")
#define RECIPE_JSON_COMMENT _T("Comment")
#define RECIPE_JSON_FILTERS _T("Filters")

	#define RECIPE_JSON_FILTER_NAME _T("Name")
	#define RECIPE_JSON_FILTER_KEY _T("Key")
	#define RECIPE_JSON_FILTER_TYPE _T("Type")

	#define RECIPE_JSON_FILTER_ENABLE _T("Enable")
	#define RECIPE_JSON_FILTER_IS_END_POINT _T("IsEndPoint")

	#define RECIPE_JSON_FILTER_PARAMS _T("Params")
		#define RECIPE_JSON_FILTER_PARAM_KEY _T("Key")
		#define RECIPE_JSON_FILTER_PARAM_VALUE _T("Value")

	#define RECIPE_JSON_FILTER_PINS _T("Pins")
		#define RECIPE_JSON_FILTER_PIN_TYPE _T("Type")
		#define RECIPE_JSON_FILTER_PIN_NAME _T("Name")
		#define RECIPE_JSON_FILTER_PIN_KEY _T("Key")
		#define RECIPE_JSON_FILTER_PIN_LINK _T("Link")
			#define RECIPE_JSON_FILTER_PIN_LINK_FILTER _T("FilterKey")
			#define RECIPE_JSON_FILTER_PIN_LINK_PIN _T("PinKey")


#define GETJSONSTRING(JSON, KEY, VALUE) \
{                                       \
	if (JSON.HasMember(KEY))			\
	{									\
		VALUE = JSON[KEY].GetString();	\
	}									\
}                                       \


#define GETJSONBOOL(JSON, KEY, VALUE) \
{                                       \
	if (JSON.HasMember(KEY))			\
	{									\
		VALUE = JSON[KEY].GetBool();	\
	}									\
}	

#define GETJSONINT(JSON, KEY, VALUE) \
{                                       \
	if (JSON.HasMember(KEY))			\
	{									\
		VALUE = JSON[KEY].GetInt();	\
	}									\
}	