//----------------------------------------------------------------------------
/** @file Filter.h
	@brief 여기에는 필터와 Recipe의 원형이 되는 클래스를 정의합니다.
*/
//----------------------------------------------------------------------------
#pragma once

#include "BaseDefine.h"
#include "IFilter.h"

/// <summary>
/// Recipe 파일 처리를 위한 최상위 클래스
/// </summary>
class Recipe
{
	std::vector<IFilter*> Filters;

	tstring strName;
	tstring strComment;

public:
	Recipe() {}
	~Recipe();
public:
	void Parse(const char* recipe);
	int Init(rapidjson::GenericValue<ENCODE> &value);

	void PinkLinking(rapidjson::GenericValue<rapidjson::UTF16LE<>>& fp, const rapidjson::SizeType& i, IFilter*& filter, tstring& strFilterKey, tstring& strPinKey);

public :
	void Process(std::vector<cv::Mat*> &src, std::vector<cv::Mat*> &ref, std::vector<cv::Mat*> &ret);

private:
	IFilter* GetFilterByKey(LPCTSTR key);
};



